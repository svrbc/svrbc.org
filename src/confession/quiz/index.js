module.exports = {
  use: '@gridsome/source-filesystem',
  options: {
    baseDir: './src',
    path: 'confession/quiz/*.yml',
    typeName: 'ConfessionQuiz',
    refs: {
      lbcf: 'ConfessionParagraph'
    }
  }
}
