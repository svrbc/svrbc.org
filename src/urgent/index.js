module.exports = {
  use: '@gridsome/source-filesystem',
  options: {
    baseDir: './src',
    path: [
      'urgent/*.md',
      '!urgent/README.md'
    ],
    typeName: 'UrgentAnnouncement'
  }
}
