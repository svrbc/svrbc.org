---
id: dummy
---

This file exists so that the schema remains even when there are no urgent
announcements stored in this directory.
