/* eslint-env jest */
import srcImages from '../lib/src-images'

// BEGIN tests ////////////////////////////////////////////////////////////////

describe('banners', () => {
  let images

  beforeAll(async () => {
    images = await srcImages.scan(__dirname, '**', '*.jpg')
  })

  it('under 500k', () => {
    for (const i of images) {
      const msg = `${i.basename} should be <500kb`
      expect(i.bytes, msg).toBeLessThan(500000)
    }
  })

  it('1920x1080', () => {
    for (const i of images) {
      const msg = `${i.basename} should be 1920x1080`
      expect(i.dims.width, msg).toEqual(1920)
      expect(i.dims.height, msg).toEqual(1080)
    }
  })
})
