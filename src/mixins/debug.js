const FEATURES = {
  featLive: 'live',
  v1: 'v1'
}

export default {
  data () {
    return Object.assign({
      // TODO: remove debug.  For some reason, it's not showing up as defined
      // until it gets set.
      debug: false,
      $debug: false
    }, Object.fromEntries(Object.keys(FEATURES).map(k => [k, false])))
  },
  mounted () {
    this.$debug = 'debug' in this.$route.query
    this.debug = this.$debug
    if (!('f' in this.$route.query)) return
    const f = this.$route.query.f
    const fs = f ? f.split(',') : []
    Object.entries(FEATURES).forEach(([k, v]) => {
      this[k] = !f || fs.includes(v)
    })
  }
}
