import delay from 'delay'

export default {
  data () {
    return {
      loaded: false,
      now: null
    }
  },
  async mounted () {
    // In development mode, we have to give the system a cycle to set the old
    // class first, otherwise it will process mounted() before applying our
    // reactive properties.
    if (process.env.NODE_ENV === 'development') await delay(0)
    this.loaded = true

    while (true) {
      this.now = new Date()
      await delay(500)
    }
  }
}
