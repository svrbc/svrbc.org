import tips from '../lib/tips'

export default {
  methods: {
    async copyNotify (e) {
      const message = 'Copied!'
      tips.flash(e.trigger, message, {
        placement: 'top'
      })
    }
  }
}
