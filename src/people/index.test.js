/* eslint-env jest */
import srcImages from '../lib/src-images'

// BEGIN tests ////////////////////////////////////////////////////////////////

describe('portraits', () => {
  let images

  beforeAll(async () => {
    images = await srcImages.scan(__dirname, '**', '*.jpg')
  })

  it('under 100k', () => {
    for (const i of images) {
      const msg = `${i.basename} should be <100kb`
      expect(i.bytes, msg).toBeLessThan(100000)
    }
  })

  it('512x640', () => {
    for (const i of images) {
      const msg = `${i.basename} should be 512x640`
      expect(i.dims.width, msg).toEqual(512)
      expect(i.dims.height, msg).toEqual(640)
    }
  })
})
