# Announcements

New announcements should go in this directory.

**image**: The path to the image.  Guidelines:

*   Should be located in this directory
*   Should be named generically for reuse. (e.g., “flowers” rather than
    “mother’s day”)
*   Should be a .jpg
*   Should be 720x480
*   Should be <100kb

**style**: light/dark.  This should match whether the chosen image is light or
dark.

**script**: The script that should be run to produce an event or null.  This is
an alternative to `schedule`.

**schedule.start**: The date to begin showing this announcement.

**schedule.end**: The date to stop showing this announcement.  If no date is
provided, the start of the next will be used.

**schedule.urgency**: The date of the event, for determining the order of the
announcements.  This will be interpreted with a natural language processor, so
you can use dates like “thursday”.  The date will be assumed to be in the
future.  This will be assumed to be the same as end if none is specified.

**content**: Comment to be displayed in the placard.

Example:

    # Memorial day
    - image: grill.jpg
      style: dark
      start: 2020-04-25
      urgency: 2020-05-25
      content: |
        Memorial Day picnic!

        In several weeks!
    - start: 2020-05-18
      end: 2020-05-25
      content: |
        Memorial Day picnic!

        Bring a friend!
