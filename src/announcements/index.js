module.exports = {
  use: '@gridsome/source-filesystem',
  options: {
    baseDir: './src',
    path: [
      'announcements/*.yml'
    ],
    typeName: 'Announcement'
  }
}
