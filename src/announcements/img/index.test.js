/* eslint-env jest */
import srcImages from '../../lib/src-images'

// BEGIN tests ////////////////////////////////////////////////////////////////

describe('announcements', () => {
  let images

  beforeAll(async () => {
    images = await srcImages.scan(__dirname, '**', '*.jpg')
  })

  it('under 100k', () => {
    for (const i of images) {
      expect(i.bytes).toBeLessThan(100000)
    }
  })

  it('720x480', () => {
    for (const i of images) {
      if ([
        'blue-chairs.jpg',
        'dirt-trowel.jpg',
        'man-stars.jpg'
      ].includes(i.basename)) continue
      expect(i.dims.width, `${i.basename} width is off`).toEqual(720)
      expect(i.dims.height, `${i.basename} height is off`).toEqual(480)
    }
  })
})
