---
id: "2021-07-23"
date: 2021-07-23
title: Advice For Those Entertaining Paedobaptist Notions
author: Conley Owens
image: ../banners/ocean-wave.jpg
bannerstyle: light
---

**You**: A credobaptist[^1] (likely of the Baptist variety) considering
embracing paedobaptism[^2] (likely of the Presbyterian variety).  At this
point, you’ve seen something in paedobaptist arguments; perhaps it’s the New
Testament emphasis on households or perhaps an apparent continuity between the
New Covenant and the covenant of circumcision made with Abraham.  You are at a
point where you are willing to tell others that you are considering these
things; perhaps you even ask them to convince you otherwise, but deep down you
suspect you may have already reached your conclusions and passed the point of
no return.

**Me**: A Baptist pastor who wants to help!  I have some advice for you, and
it is probably not the advice you are expecting.

## My advice for you

**Put everything on hold until you have studied congregationalism.**[^3]

Or, to give you something more directly actionable, read [Jonathan Leeman’s
*Don’t Fire Your Church
Members*](https://9marks.myshopify.com/products/dont-fire-your-church-members-the-case-for-congregationalism).

Now why on earth would I recommend this?

1.  As stated in the introduction, I’m assuming you’ve already done a bit of
    study on baptism, so it’s not clear I have anything to offer you haven’t
    already considered.  Perhaps if we were face-to-face, I could discuss the
    matter with you for long hours, but even then, there’s something
    psychological about this point in your journey that would resist new
    arguments.  I don’t say that as an insult or to accuse you of being
    closed-minded; it’s just a reality of human weakness and the deliberative
    process.

2.  Most people don't learn about Baptist polity (i.e., congregationalism)
    and so don't think much of it.  They don’t realize it’s a great treasure
    that's typically lost in a switch to another kind of practice.

3.  Congregationalism is more fundamental than credobaptism and therefore
    should precede any conclusions on baptism. Congregational suffrage
    necessitates regenerate membership but not the other way around.

4.  In many respects, in pitting them against their opposing positions, the
    case for congregationalism is more straightforward than the case for
    credobaptism.  In other words, in working out related doctrinal issues, it
    represents low-hanging fruit.

A right understanding of church polity[^4]—which I would argue
congregationalism is—answers the question “who holds the keys of the kingdom,
and how should they be exercised?” ([Matthew 16:19]{.ref}). These keys are used
to remove from the church (i.e., excommunicate) in Matthew 18 and used to add
to the church (i.e., baptize) in Matthew 28.  Your understanding of the keys
must inform your understanding of their use in baptism.

If you are inclined to dismiss this advice as irrelevant, please consider that
connection just described.  I’ll say it again.

**Put everything on hold until you have studied congregationalism.**

## My advice for others

Maybe I assumed to much about you and you aren’t nearly as far along in your
considerations of paedobaptism as I described in the introduction.  Maybe no
one has ever taught you Baptist covenant theology and you didn’t realize there
was a serious counter argument to the one-covenant-multiple-administrations
model, common in Presbyterianism.

If that’s the case, I would simply point out that the New Covenant is not like
the Old ([Jeremiah 31:31-32]{.ref}), and the unity between them is found in
typological reference (i.e., foreshadowing), not in essential identity.  Here’s
a reading list to help you contemplate this point.

*   [*The Mystery of Christ, His Covenant, and His Kingdom* by Samuel Renihan](https://press.founders.org/shop/the-mystery-of-christ-his-covenant-and-his-kingdom/)
*   [*The Distinctiveness of 17th Century Particular Baptist Covenant Theology*, by Pascal Denault](http://www.rbap.net/our-books/the-distinctiveness-of-baptist-covenant-theology/)
*   [*Covenant Theology From Adam to Christ*, by Nehemiah Coxe and John Owen](http://www.rbap.net/our-books/covenant-theology-from-adam-to-christ-by-nehemiah-coxe-and-john-owen/)

[A longer reading list may be found here.](https://www.1689federalism.com/recommended-reading-list/)

## Some qualifications

Of course, this is a short article primarily designed to call to attention the
relevance of church polity.  It’s jumping past the basics, assuming you already have those covered.  But since I’d feel negligent if I didn’t at least mention
them, here are some steps you simply cannot afford to skip.

1.  Read God’s word to look for guidance.

2.  Pray that the Holy Spirit would lead you into all truth.

3.  Ask your pastors for help as they are the ones God has ordained to watch
    over your soul.

Wrestling with this question of doctrine is a difficult matter, and the
implications for church life and even family life are weighty.  May God bless
you and keep you!

[^1]: Credobaptism is the practice of only baptizing those who have made a
    credible profession of faith.

[^2]: Paedobaptism is the practice of baptizing infants (of believers).

[^3]: Congregationalism is the practice of a congregation exercising the
    highest authority in church life.  For example, in congregationalism,
    the congregation’s vote on a matter cannot be overruled.

[^4]: I.e., church government.
