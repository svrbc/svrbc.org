module.exports = {
  use: '@gridsome/source-filesystem',
  options: {
    baseDir: './src',
    path: 'articles/*.md',
    typeName: 'Article'
  }
}
