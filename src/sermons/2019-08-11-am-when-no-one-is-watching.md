---
id: 2019-08-11-am
title: When No One Is Watching
date: 2019-08-11
text: Nehemiah 13:1–9
preacher: josh-sheldon
series: ezra-nehemiah
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2019/190811-AM-WhenNoOneIsWatching.mp3
audioBytes: 44443865
---
