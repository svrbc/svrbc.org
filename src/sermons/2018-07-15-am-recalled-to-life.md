---
id: 2018-07-15-am
title: Recalled To Life
date: 2018-07-15
text: Ezra 1
preacher: josh-sheldon
series: ezra-nehemiah
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2018/180715-AM-RecalledToLife.mp3
audioBytes: 45271397
youtube: RTwRUAcJxuI
---
