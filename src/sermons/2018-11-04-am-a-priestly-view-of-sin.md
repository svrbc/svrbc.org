---
id: 2018-11-04-am
title: A Priestly View Of Sin
date: 2018-11-04
text: Ezra 9:1–5
preacher: josh-sheldon
series: ezra-nehemiah
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2018/181104-AM-APriestlyViewOfSin.mp3
audioBytes: 52445252
---
