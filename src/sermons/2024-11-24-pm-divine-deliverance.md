---
id: 2024-11-24-pm
title: Divine Deliverance
date: 2024-11-24
text: Isaiah 38:1–6
preacher: conley-owens
series: isaiah
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2024/241124-DivineDeliverance.aac
audioBytes: 39274847
youtube: mtuwisD-QRk
---
