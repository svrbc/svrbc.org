---
id: 2018-05-13-pm
title: A Lament Of Faith
date: 2018-05-13
text: Habakkuk 2:6–20
preacher: josh-sheldon
series: minor-prophets
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2018/180513-PM-ALamentOfFaith.mp3
audioBytes: 34786064
youtube: FqArIDqGFz4
---
