---
id: 2018-05-27-am
title: Our Christian Posture Part 2
date: 2018-05-27
text: Ephesians 1:15–23
preacher: larry-trummel
series: our-christian-posture
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2018/180527-AM-OurChristianPosturePt2.mp3
audioBytes: 51745240
youtube: xsFY9xRkdKU
---
