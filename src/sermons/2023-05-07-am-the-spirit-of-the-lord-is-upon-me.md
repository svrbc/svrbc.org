---
id: 2023-05-07-am
title: The Spirit of the Lord is Upon Me
date: 2023-05-07
text: Luke 4:14–22
preacher: brian-garcia
series: luke
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2023/230507-TheSpiritOfTheLordIsUponMe.aac
youtube: J-H9rDOxdzc
---
