---
id: 2015-04-19-am
title: All God’s Children Must Depart From Iniquity
date: 2015-04-19
text: 2 Timothy 2:19
preacher: josh-sheldon
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2015/150419+All+Gods+Children+Must+Depart+From+Quality.mp3
audioBytes: 47069433
---
