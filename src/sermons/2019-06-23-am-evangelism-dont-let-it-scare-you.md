---
id: 2019-06-23-am
title: Evangelism – Don’t Let It Scare You
date: 2019-06-23
text: Acts 8:26–39
preacher: josh-sheldon
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2019/190623-AM-Evangelism-DontLetItScareYou.mp3
audioBytes: 47967254
---
