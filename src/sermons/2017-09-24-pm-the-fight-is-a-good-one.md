---
id: 2017-09-24-pm
title: The Fight is a Good One
date: 2017-09-24
text: 1 Timothy 6:12–16
preacher: josh-sheldon
series: 1-timothy
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2017/170924+PM+Service+-+The+Fight+is+a+Good+One.mp3
audioBytes: 23620724
youtube: KRaoA4iKrfU
---
