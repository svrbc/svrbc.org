---
id: 2024-11-10-am
title: Procreation is not Optional
date: 2024-11-10
text: Genesis 2:28
preacher: tim-mullet
series: basic-truths
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2024/241110-BasicTruths-ProcreationIsNotOptional.aac
audioBytes: 67718744
youtube: ks6uxivcZmg
---
