---
id: 2017-07-23-am
title: The Authority of the Son
date: 2017-07-23
text: Hebrews 1:2–3
preacher: conley-owens
series: hebrews
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2017/170723+The+Authority+of+the+Son.mp3
audioBytes: 29923519
youtube: VSbELMevrs8
---
