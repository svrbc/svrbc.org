---
id: 2025-03-02-pm
title: The Vanity of Princes
date: 2025-03-02
text: Isaiah 40:21–24
preacher: conley-owens
series: isaiah
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2025/250302-TheVanityofPrinces.aac
audioBytes: 41619068
youtube: mNke3Q08hrE
---
