---
id: 2024-03-10-pm
title: Sense for the Senseless
date: 2024-03-10
text: Isaiah 35:5–7
preacher: conley-owens
series: isaiah
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2024/240310-SenseFortheSenseless.aac
youtube: Pj3UNvXQ73o
---
