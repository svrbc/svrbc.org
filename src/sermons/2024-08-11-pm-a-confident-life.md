---
id: 2024-08-11-pm
title: A Confident Life
date: 2024-08-11
text: Colossians 2:6–7
preacher: josh-sheldon
series: the-sufficiency-of-christ
audio: https://storage.googleapis.com/pbc-ca-sermons/2024/240811-AConfidentLife.aac
youtube: pnrm1SI02lc
---
