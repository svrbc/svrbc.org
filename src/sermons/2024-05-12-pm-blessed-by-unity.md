---
id: 2024-05-12-pm
title: Blessed By Unity
date: 2024-05-12
text: Psalm 133
preacher: josh-sheldon
unregisteredSeries: Psalms of Ascent
audio: https://storage.googleapis.com/pbc-ca-sermons/2024/240512-BlessedByUnity.aac
youtube: gLUw4rmcaWw
---
