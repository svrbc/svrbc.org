---
id: 2019-11-24-am
title: Time Is Short
date: 2019-11-24
text: 1 Peter 4:7–11
preacher: mike-kelley
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2019/191124-AM-TheTimeIsShort.mp3
audioBytes: 38823062
---
