---
id: 2012-03-25-am
title: The Word of God
date: 2012-03-25
text: John 7:1–9
preacher: josh-sheldon
series: john
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2012/2012-03-25+The+Word+of+God.mp3
audioBytes: 33740775
---
