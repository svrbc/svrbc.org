---
id: 2023-08-27-pm
title: The Work Of The Spirit
date: 2023-08-27
text: Isaiah 32:14–20
preacher: conley-owens
series: isaiah
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2023/230827-TheWorkOfTheSpirit.aac
youtube: qq2RmIusSmY
---
