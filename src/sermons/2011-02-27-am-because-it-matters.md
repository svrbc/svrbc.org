---
id: 2011-02-27-am
title: Because It Matters
date: 2011-02-27
text: Exodus 20:12–17
preacher: josh-sheldon
series: the-ten-commandments
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2011/2011-02-27+Because+It+Matters.mp3
audioBytes: 49365888
---
