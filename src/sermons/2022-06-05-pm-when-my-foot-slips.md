---
id: 2022-06-05-pm
title: When My Foot Slips
date: 2022-06-05
text: Psalm 94:1–19
preacher: brian-garcia
audio: https://storage.googleapis.com/pbc-ca-sermons/2022/220605-WhenMyFootSlips.aac
audioBytes: 14856501
youtube: A9wnTuURZt8
---
