---
id: 2025-02-02-pm
title: The Authorized Message
date: 2025-02-02
text: Isaiah 40:6–8
preacher: conley-owens
series: isaiah
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2025/250202-TheAuthorizedMessage.aac
audioBytes: 40183096
youtube: TsE9dWdPlHw
---
