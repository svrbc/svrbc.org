---
id: 2012-02-05-am
title: The Person and Purpose of the Revelation of Jesus Christ
date: 2012-02-05
text: Revelation 1
preacher: jesse-lu
series: the-churches-of-revelation
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2012/2012-02-05+The+Person+and+Purposeof+the+Revelation+of+Jesus+Christ.mp3
audioBytes: 53465292
---
