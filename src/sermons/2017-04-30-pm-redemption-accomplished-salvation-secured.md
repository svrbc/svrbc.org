---
id: 2017-04-30-pm
title: 'Redemption Accomplished, Salvation Secured'
date: 2017-04-30
text: John 19:28–30
preacher: josh-sheldon
series: the-seven-last-sayings
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2017/170430+PM+Service+-+Redemption+Accomplished+Salvation+Secured.mp3
audioBytes: 22493075
youtube: oAU02kv58DI
---
