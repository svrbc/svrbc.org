---
id: 2024-04-14-am
title: True Riches
date: 2024-04-14
text: Luke 16:10–13
preacher: conley-owens
series: luke
audio: https://storage.googleapis.com/pbc-ca-sermons/2024/240414-TrueRiches.aac
youtube: CcmablC-Y9s
---
