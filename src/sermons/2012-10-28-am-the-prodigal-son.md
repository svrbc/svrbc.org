---
id: 2012-10-28-am
title: The Prodigal Son
date: 2012-10-28
text: Luke 15:11–32
preacher: josh-sheldon
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2012/2012-10-28+The+Prodigal+Son.mp3
audioBytes: 53173392
---
