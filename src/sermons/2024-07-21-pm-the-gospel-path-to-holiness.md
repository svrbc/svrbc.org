---
id: 2024-07-21-pm
title: The Gospel Path To Holiness
date: 2024-07-21
text: Galatians 2:19–3:3
preacher: chris-santiago
unregisteredSeries: Gospel Path To Holiness
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2024/240721-The%20Gospel%20Path%20To%20Holiness.aac
youtube: kqDOTcShgLA
---
