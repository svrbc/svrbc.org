---
id: 2021-08-29-pm
title: God Hears When We Pray
date: 2021-08-29
text: Malachi 3:16–18
preacher: josh-sheldon
unregisteredSeries: Prayer
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2021/210829-GodHearsWhenWePray.aac
youtube: N7Uc2hKweVQ
---
