---
id: 2023-10-29-pm
title: Of Prey and Spoil
date: 2023-10-29
text: Isaiah 33:23–24
preacher: conley-owens
series: isaiah
audio: https://storage.googleapis.com/pbc-ca-sermons/2023/231029-OfPreyAndSpoil.aac
youtube: Sth36EQjhFA
---
