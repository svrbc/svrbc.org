---
id: 2014-11-09-am
title: Prepared For Holiness
date: 2014-11-09
text: 1 Peter 1:13–21
preacher: josh-sheldon
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2014/141109+Prepared+For+Holiness.mp3
audioBytes: 44494393
---
