---
id: 2018-10-14-pm
title: The Sentence – Death!
date: 2018-10-14
text: Matthew 26:65–66
preacher: josh-sheldon
series: meditations-upon-the-cross
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2018/181014-PM-TheSentence-Death.mp3
audioBytes: 28386692
---
