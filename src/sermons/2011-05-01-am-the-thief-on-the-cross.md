---
id: 2011-05-01-am
title: The Thief on The Cross
date: 2011-05-01
text: Luke 23:35–43
preacher: jesse-lu
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2011/2011-05-01+The+Thief+on+The+Cross.mp3
audioBytes: 50040960
---
