---
id: 2024-02-18-pm
title: 'Not Peace, but a Sword'
date: 2024-02-18
text: Luke 12:49–59
preacher: brian-garcia
series: luke
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2024/240218-NotPeaceButASword.aac
youtube: xvKTsAna8EQ
---
