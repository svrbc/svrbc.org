---
id: 2015-11-29-am
title: A Word To Be Trusted
date: 2015-11-29
text: 1 Kings 22
preacher: josh-sheldon
series: elijah-elisha
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2015/151129+A+Word+to+be+Trusted.mp3
audioBytes: 59822180
youtube: UurB2hB7Zhk
---
