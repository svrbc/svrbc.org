---
id: 2017-05-28-pm
title: The Only Valid Goal
date: 2017-05-28
text: 1 Timothy 1:3–7
preacher: josh-sheldon
series: 1-timothy
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2017/20170528+PM+Service+-+The+Only+Valid+Goal.mp3
audioBytes: 35713522
youtube: yyDpGRIlaVg
---
