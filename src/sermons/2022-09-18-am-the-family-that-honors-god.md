---
id: 2022-09-18-am
title: The Family that Honors God
date: 2022-09-18
text: Ephesians 6:1–4
preacher: brian-garcia
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2022/220918-TheFamilyThatHonorsGod.aac
youtube: qTQ8etXc5WU
---
