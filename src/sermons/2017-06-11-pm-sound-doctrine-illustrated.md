---
id: 2017-06-11-pm
title: Sound Doctrine Illustrated
date: 2017-06-11
text: 1 Timothy 1:12–17
preacher: josh-sheldon
series: 1-timothy
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2017/170611+PM+Service+-+Sound+Doctrine+Illustrated.mp3
audioBytes: 30178089
youtube: fdgw2XRbVxQ
---
