---
id: 2017-02-26-pm
title: Pleading our Innocence
date: 2017-02-26
text: Psalm 7
preacher: josh-sheldon
series: the-psalms
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2017/170226+Afternoon+Service-Pleading+Our+Innocence.mp3
audioBytes: 27708292
youtube: fUaNLnH9qyU
---
