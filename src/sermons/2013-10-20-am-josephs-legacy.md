---
id: 2013-10-20-am
title: Joseph’s Legacy
date: 2013-10-20
text: Genesis 37:1–20
preacher: josh-sheldon
series: the-life-of-joseph
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2013/2013-10-20+Joseph%27s+Legacy.mp3
audioBytes: 41671698
---
