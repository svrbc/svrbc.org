---
id: 2018-08-19-pm
title: A Truth Worth Dying For
date: 2018-08-19
text: Matthew 26:63–64
preacher: josh-sheldon
series: meditations-upon-the-cross
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2018/180819-PM-ATruthWorthDyingFor.mp3
audioBytes: 21353695
---
