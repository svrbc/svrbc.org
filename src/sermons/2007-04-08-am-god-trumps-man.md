---
id: 2007-04-08-am
title: God Trumps Man
date: 2007-04-08
text: Acts 2:22–39
preacher: josh-sheldon
series: acts
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2007/2007-04-08+God+Trumps+Man.mp3
audioBytes: 43305087
---
