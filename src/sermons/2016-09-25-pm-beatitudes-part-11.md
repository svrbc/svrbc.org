---
id: 2016-09-25-pm
title: Beatitudes Part 11
date: 2016-09-25
text: Matthew 6:1–4
preacher: josh-sheldon
series: beatitudes
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2016/160925+Beatitudes+Part+11.mp3
audioBytes: 29244269
youtube: b0QnJF4p4IU
---
