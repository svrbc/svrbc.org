---
id: 2024-10-13-am
title: Marriage Is Not Optional
date: 2024-10-13
text: Genesis 2:18–25
preacher: tim-mullet
series: basic-truths
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2024/241013-BasicTruths-MarriageIsNotOptional.aac
audioBytes: 65467693
youtube: Sfhb2IwXtjo
---
