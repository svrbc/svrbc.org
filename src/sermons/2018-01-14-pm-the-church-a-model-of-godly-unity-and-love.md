---
id: 2018-01-14-pm
title: 'The Church: A Model of Godly Unity And Love'
date: 2018-01-14
text: John 17:20–26
preacher: josh-sheldon
series: the-road-to-the-cross
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2018/180114-PM-TheChurch-AModelOfGoldyUnityAndLove.mp3
audioBytes: 27390727
youtube: 8DfGzs9RyQs
---
