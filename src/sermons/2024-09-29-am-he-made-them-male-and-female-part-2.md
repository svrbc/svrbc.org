---
id: 2024-09-29-am
title: 'He Made them Male and Female, Part 2'
date: 2024-09-29
text: Genesis 1:27
preacher: tim-mullet
series: basic-truths
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2024/240929-BasicTruthsfromGenesisHemadethemMaleandFemalePart2.aac
audioBytes: 49843139
youtube: R-r0MboKFzQ
---
