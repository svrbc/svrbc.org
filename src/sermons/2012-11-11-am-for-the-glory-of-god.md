---
id: 2012-11-11-am
title: For the Glory of God
date: 2012-11-11
text: John 11:1–16
preacher: josh-sheldon
series: john
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2012/2012-11-11+For+the+Glory+of+God.mp3
audioBytes: 50932434
---
