---
id: 2017-01-22-am
title: Striving For Righteousness
date: 2017-01-22
text: Romans 2:17–29
preacher: josh-sheldon
series: romans
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2017/170122+Striving+For+Righteousness.mp3
audioBytes: 50450361
youtube: 5gnMnoklNP4
---
