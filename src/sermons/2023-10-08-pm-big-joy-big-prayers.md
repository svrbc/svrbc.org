---
id: 2023-10-08-pm
title: 'Big Joy, Big Prayers'
date: 2023-10-08
text: Psalm 126
preacher: josh-sheldon
unregisteredSeries: Psalms of Ascent
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2023/231008-BigJoyBigPrayers.aac
youtube: LLmONLCHHas
---
