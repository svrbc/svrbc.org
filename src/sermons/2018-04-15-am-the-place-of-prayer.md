---
id: 2018-04-15-am
title: The Place Of Prayer
date: 2018-04-15
text: Romans 15:30–33
preacher: josh-sheldon
series: romans
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2018/180415-AM-ThePlaceOfPrayer.mp3
audioBytes: 48861673
youtube: '-czFUliM9aE'
---
