---
id: 2024-04-21-am
title: Wait On The Lord
date: 2024-04-21
text: Psalm 130
preacher: josh-sheldon
unregisteredSeries: Psalms of Ascent
audio: https://storage.googleapis.com/pbc-ca-sermons/2024/240421-WaitOnTheLord.aac
youtube: V38_Fjtz8zU
---
