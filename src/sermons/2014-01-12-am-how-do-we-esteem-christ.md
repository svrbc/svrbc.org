---
id: 2014-01-12-am
title: How Do We Esteem Christ
date: 2014-01-12
text: John 11:55–12:11
preacher: josh-sheldon
series: john
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2014/2014-01-12+How+Do+We+Esteem+Christ.mp3
audioBytes: 42368922
---
