---
id: 2023-09-24-pm
title: The Christian’s Peaceful Trust
date: 2023-09-24
text: Psalm 125
preacher: josh-sheldon
unregisteredSeries: Psalms of Ascent
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2023/230924-TheChristiansPeacefulTrust.aac
youtube: DzaIqDJS1Iw
---
