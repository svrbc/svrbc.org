---
id: 2008-05-18-am
title: 'Seeking the Lord, A Lesson From A King'
date: 2008-05-18
text: 2 Chronicles 14–16
preacher: josh-sheldon
series: kings-of-judah
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2008/2008-05-18+Seeking+the+Lord,+A+Lesson+From+A+King+.mp3
audioBytes: 58822074
---
