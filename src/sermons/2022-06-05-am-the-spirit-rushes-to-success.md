---
id: 2022-06-05-am
title: The Spirit Rushes to Success
date: 2022-06-05
text: 1 Samuel 16:13
preacher: josh-sheldon
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2022/220605-TheSpiritRusheToSuccess.aac
youtube: xqgxgtYvwSk
---
