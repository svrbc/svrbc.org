---
id: 2014-08-10-am
title: The Work Of God’s Holy Spirit
date: 2014-08-10
text: John 16:5–15
preacher: josh-sheldon
series: john
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2014/2014-08-10+The+Work+Of+Gods+Holy+Spirit.mp3
audioBytes: 50749937
---
