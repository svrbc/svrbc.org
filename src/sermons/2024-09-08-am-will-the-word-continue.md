---
id: 2024-09-08-am
title: Will The Word Continue?
date: 2024-09-08
text: 2 Kings 2:9–15
preacher: josh-sheldon
unregisteredSeries: The Sufficiency of Christ
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2024/240908-WillTheWordContinue.aac
audioBytes: 38311078
youtube: GAWcytAs7s4
---
