---
id: 2016-10-23-pm
title: Beatitudes Part 15
date: 2016-10-23
text: Matthew 6:5–14
preacher: josh-sheldon
series: beatitudes
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2016/161023+Afternoon+Service+-+Beatitudes+Part+15.mp3
audioBytes: 34852527
youtube: FtJa-Qyw5Zw
---
