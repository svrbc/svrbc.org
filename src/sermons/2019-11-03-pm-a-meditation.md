---
id: 2019-11-03-pm
title: A Meditation
date: 2019-11-03
text: Psalm 6
preacher: josh-sheldon
series: the-psalms
audio: https://storage.googleapis.com/pbc-ca-sermons/2019/191103-PM-AMeditation.mp3
audioBytes: 38814761
---
