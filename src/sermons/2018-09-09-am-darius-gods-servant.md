---
id: 2018-09-09-am
title: 'Darius, God’s Servant'
date: 2018-09-09
text: Ezra 6:1–12
preacher: josh-sheldon
series: ezra-nehemiah
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2018/180909-AM-Darius-GodsServant.mp3
audioBytes: 44388254
---
