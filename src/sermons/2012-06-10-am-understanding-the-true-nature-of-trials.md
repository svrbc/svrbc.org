---
id: 2012-06-10-am
title: Understanding the True Nature of Trials
date: 2012-06-10
text: James 1:12–14
preacher: steve-mixsell
series: james
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2012/2012-06-10+Understanding+the+True+Nature+of+Trials.mp3
audioBytes: 56990610
---
