---
id: 2022-12-25-am
title: 'The Gospel of Luke: Hope for the World'
date: 2022-12-25
text: Luke 1:26–38
preacher: brian-garcia
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2022/221225-TheGospelOfLuke-HopeForTheWorld.aac
youtube: Q9XBlBsupbM
---
