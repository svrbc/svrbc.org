---
id: 2021-09-26-am
title: Unshakeable Confidence
date: 2021-09-26
text: 2 Thessalonians 2:1–4
preacher: josh-sheldon
series: awaiting-christ
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2021/210926-UnshakeableConfidence.aac
audioBytes: 53380933
youtube: kml0R8sPxPA
---
