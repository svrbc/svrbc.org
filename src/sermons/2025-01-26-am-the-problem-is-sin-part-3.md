---
id: 2025-01-26-am
title: The Problem Is Sin Part 3
date: 2025-01-26
text: Genesis 3:14–15
preacher: tim-mullet
series: basic-truths
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2025/250126-BasicTruths-TheProblemIsSin-Part3.aac
audioBytes: 71875834
youtube: VQxkQ4UKyXU
---
