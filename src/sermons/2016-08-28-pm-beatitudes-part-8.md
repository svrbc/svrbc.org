---
id: 2016-08-28-pm
title: Beatitudes Part 8
date: 2016-08-28
text: Matthew 5
preacher: josh-sheldon
series: beatitudes
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2016/160828+Beatitudes+Part+8.mp3
audioBytes: 30535363
youtube: XCLKYDFBTBc
---
