---
id: 2008-09-28-am
title: Jehoshaphat Part 1
date: 2008-09-28
text: 2 Chronicles 17
preacher: josh-sheldon
series: kings-of-judah
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2008/2008-09-28+Jehoshaphat+Part+1.mp3
audioBytes: 50655546
---
