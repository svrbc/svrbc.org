---
id: 2014-10-19-am
title: 'The Lord, He is Sovereign'
date: 2014-10-19
text: John 18:1–11
preacher: josh-sheldon
series: john
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2014/141019+The+Lord+-+He+is+Sovereign.mp3
audioBytes: 42631493
---
