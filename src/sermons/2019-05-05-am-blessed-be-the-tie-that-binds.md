---
id: 2019-05-05-am
title: Blessed Be The Tie That Binds
date: 2019-05-05
text: Nehemiah 9:1–6
preacher: josh-sheldon
series: ezra-nehemiah
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2019/190505-AM-BlestBeTheTieThatBinds.mp3
audioBytes: 50662251
---
