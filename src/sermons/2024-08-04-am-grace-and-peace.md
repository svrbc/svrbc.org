---
id: 2024-08-04-am
title: Grace and Peace
date: 2024-08-04
text: Colossians 1:1–2
preacher: josh-sheldon
series: the-sufficiency-of-christ
audio: https://storage.googleapis.com/pbc-ca-sermons/2024/240804-GraceAndPeace.aac
youtube: qnS-JMjDtvE
---
