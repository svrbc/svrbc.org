---
id: 2019-03-17-pm
title: He Dies As He Lived
date: 2019-03-17
text: Luke 23:33
preacher: josh-sheldon
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2019/190317-PM-HeDiesAsHeLived.mp3
audioBytes: 25540351
---
