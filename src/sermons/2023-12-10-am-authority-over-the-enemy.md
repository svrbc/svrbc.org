---
id: 2023-12-10-am
title: Authority Over the Enemy
date: 2023-12-10
text: Luke 10:13–20
preacher: brian-garcia
series: luke
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2023/231210-AuthorityOvertheEnemy.aac
youtube: rftziGPmcjQ
---
