---
id: 2019-06-23-pm
title: 'Who Do You Think You Are, God?'
date: 2019-06-23
text: Zechariah 6:1–8
preacher: josh-sheldon
series: zechariah
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2019/190623-PM-WhoDoYouThinkYouAre-God.mp3
audioBytes: 34762253
---
