---
id: 2021-09-12-pm
title: 'The Oracle Against Moab: God’s Mercy in Light of His Justice'
date: 2021-09-12
text: Isaiah 15:1–16:5
preacher: conley-owens
series: isaiah
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2021/210912-TheOracleConcerningMoab.aac
audioBytes: 26595947
youtube: 8aRVhUiP3qw
---
