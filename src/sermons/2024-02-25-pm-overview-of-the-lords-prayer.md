---
id: 2024-02-25-pm
title: Overview Of The Lord’s Prayer
date: 2024-02-25
text: Matthew 6:5–15
unregisteredPreacher: Jeffrey Homstad
unregisteredSeries: Matthew
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2024/240225-OverviewOfTheLordsPrayer.aac
youtube: bP8ikSEKlg4
---
