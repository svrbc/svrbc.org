---
id: 2022-11-27-pm
title: A Spirit of Sleep
date: 2022-11-27
text: Isaiah 29:9–12
preacher: conley-owens
audio: https://storage.googleapis.com/pbc-ca-sermons/2022/221127-ASpiritOfSleep.aac
youtube: 2qloG97AA-Q
---
