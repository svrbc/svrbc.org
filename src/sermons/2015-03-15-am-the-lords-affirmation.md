---
id: 2015-03-15-am
title: The Lord’s Affirmation
date: 2015-03-15
text: John 21:15–23
preacher: josh-sheldon
series: the-passion-of-christ
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2015/150315+The+Lords+Affirmation.mp3
audioBytes: 46036635
---
