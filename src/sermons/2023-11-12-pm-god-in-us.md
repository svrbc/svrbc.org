---
id: 2023-11-12-pm
title: God In Us
date: 2023-11-12
text: Philippians 2:12–13
preacher: josh-sheldon
series: philippians
audio: https://storage.googleapis.com/pbc-ca-sermons/2023/231112-GodInUs.aac
youtube: WHFDKKSUS2o
---
