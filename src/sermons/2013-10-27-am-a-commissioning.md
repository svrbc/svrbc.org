---
id: 2013-10-27-am
title: A Commissioning
date: 2013-10-27
text: Acts 13:1–12
preacher: josh-sheldon
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2013/2013-10-27+A+Commissioning.mp3
audioBytes: 40182591
---
