---
id: 2018-02-25-pm
title: Anger Over Mercy
date: 2018-02-25
text: Jonah 4
preacher: josh-sheldon
series: jonah
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2018/180225-PM-AngerOverMercy.mp3
audioBytes: 35607766
youtube: 26wFOExmUe8
---
