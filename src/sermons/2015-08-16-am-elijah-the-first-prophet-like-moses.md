---
id: 2015-08-16-am
title: 'Elijah, The First Prophet Like Moses'
date: 2015-08-16
text: 1 Kings 17
preacher: josh-sheldon
series: elijah-elisha
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2015/150816+Elijah+The+First+Prophet+Like+Moses.mp3
audioBytes: 49361095
youtube: ISfPL02k9FQ
---
