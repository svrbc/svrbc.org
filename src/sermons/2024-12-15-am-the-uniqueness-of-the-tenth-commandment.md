---
id: 2024-12-15-am
title: The Uniqueness Of The Tenth Commandment
date: 2024-12-15
text: Luke 18:18–22
preacher: conley-owens
series: luke
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2024/241215-TheUniquenessOfTheTenthCommandment.aac
audioBytes: 79282025
youtube: ulMD7dSNilQ
---
