---
id: 2019-09-15-pm
title: God’s Purpose In Christ
date: 2019-09-15
text: Psalm 2
preacher: josh-sheldon
series: the-psalms
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2019/190915-PM-GodsPurposeInChrist.mp3
audioBytes: 34027488
---
