---
id: 2023-12-03-pm
title: Palaces for Beasts
date: 2023-12-03
text: Isaiah 34:8–17
preacher: conley-owens
series: isaiah
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2023/231203-PalacesForBeasts.aac
youtube: oI7DfbCfiTY
---
