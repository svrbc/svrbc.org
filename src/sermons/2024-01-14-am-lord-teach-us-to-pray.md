---
id: 2024-01-14-am
title: 'Lord, Teach Us To Pray'
date: 2024-01-14
text: Luke 11:1–13
preacher: brian-garcia
series: luke
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2024/240114-LordTeachUsToPray.aac
youtube: Yb8RsrqGJx0
---
