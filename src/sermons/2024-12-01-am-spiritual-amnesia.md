---
id: 2024-12-01-am
title: Spiritual Amnesia
date: 2024-12-01
text: Psalm 103
preacher: josh-sheldon
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2024/241201-SpiritualAmnesia.aac
audioBytes: 51627695
youtube: Tw8FTMRzYZo
---
