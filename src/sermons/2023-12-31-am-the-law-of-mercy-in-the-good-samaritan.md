---
id: 2023-12-31-am
title: The Law Of Mercy In The Good Samaritan
date: 2023-12-31
text: Luke 10:25–37
preacher: brian-garcia
series: luke
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2023/231231-TheLawOfMercyInTheGoodSamaritan.aac
youtube: GRomtnfnCvs
---
