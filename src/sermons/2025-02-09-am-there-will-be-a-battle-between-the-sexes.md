---
id: 2025-02-09-am
title: There Will Be a Battle Between the Sexes
date: 2025-02-09
text: Genesis 3:16
preacher: tim-mullet
series: basic-truths
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2025/250209-BasicTruths-ThereWillBeABattleBetweentheSexes.aac
audioBytes: 76561760
youtube: dN24tNXUVj0
---
