---
id: 2024-12-23-am
title: The Salvation of the Rich
date: 2024-12-23
text: Luke 18:23–27
preacher: conley-owens
series: luke
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2025/250223-TheSalvationoftheRich.aac
audioBytes: 48273879
youtube: OZuQR8wqEK8
---
