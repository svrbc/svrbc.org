---
id: 2016-12-18-pm
title: Beatitudes Part 23
date: 2016-12-18
text: Matthew 7:12–15
preacher: josh-sheldon
series: beatitudes
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2016/161218+Afternoon+Service+-+Beatitudes+Part+23.mp3
audioBytes: 24251835
youtube: mOuJibJw6_g
---
