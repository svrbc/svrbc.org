---
id: 2012-06-03-am
title: The Danger of Judging
date: 2012-06-03
text: Matthew 7:1–6
preacher: josh-sheldon
series: lessons-from-the-sermon-on-the-mount
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2012/2012-06-03+The+Danger+of+Judging.mp3
audioBytes: 40414443
---
