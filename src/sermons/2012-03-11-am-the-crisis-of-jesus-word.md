---
id: 2012-03-11-am
title: The Crisis of Jesus Word
date: 2012-03-11
text: John 6:60–71
preacher: josh-sheldon
series: john
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2012/2012-03-11+The+Crisis+of+Jesus+Word.mp3
audioBytes: 42041994
---
