---
id: 2017-12-31-pm
title: The Love Of The Father
date: 2017-12-31
text: Luke 15:11–32
preacher: henry-wiley
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2017/171231-PM-TheLoveOfTheFather.mp3
audioBytes: 30424277
youtube: wWCbXVq6U6Q
---
