---
id: 2023-10-29-am
title: Take Up Your Cross
date: 2023-10-29
text: Luke 9:21–27
preacher: brian-garcia
series: luke
audio: https://storage.googleapis.com/pbc-ca-sermons/2023/231029-TakeUpYourCross.aac
youtube: C86Y_8RdFJE
---
