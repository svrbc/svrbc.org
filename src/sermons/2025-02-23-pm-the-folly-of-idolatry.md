---
id: 2025-02-23-pm
title: The Folly of Idolatry
date: 2025-02-23
text: Isaiah 40:16–20
preacher: conley-owens
series: isaiah
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2025/250223-TheFollyofIdolatry.aac
audioBytes: 39154818
youtube: At-zLDGj3-4
---
