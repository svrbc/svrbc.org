---
id: 2017-04-30-am
title: The Spirit of Life
date: 2017-04-30
text: Romans 8:1–4
preacher: josh-sheldon
series: romans
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2017/170430+The+Spirit+of+Life.mp3
audioBytes: 50165281
youtube: 8xwsP_A9arU
---
