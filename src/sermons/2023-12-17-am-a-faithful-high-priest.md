---
id: 2023-12-17-am
title: A Faithful High Priest
date: 2023-12-17
text: Hebrews 2:17–18
preacher: conley-owens
series: hebrews
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2023/231217-AFaithfulHighPriest.aac
youtube: YQv90ZStoko
---
