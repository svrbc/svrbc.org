---
id: 2009-02-01-am
title: Invitation To The Thirsty
date: 2009-02-01
text: John 4:1–14
preacher: mike-mccullough
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2009/2009-02-01+Invitation+To+The+Thirsty.mp3
audioBytes: 40224708
---
