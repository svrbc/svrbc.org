---
id: 2015-12-06-am
title: Is There No God?
date: 2015-12-06
text: 1 Kings 22:41–2 Kings 1:1, 18
preacher: josh-sheldon
series: elijah-elisha
audio: https://storage.googleapis.com/pbc-ca-sermons/2015/151206+Is+There+No+God.mp3
audioBytes: 55169454
youtube: R18z12Jb014
---
