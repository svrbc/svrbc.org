---
id: 2021-01-10-am
title: A Baptismal Message
date: 2021-01-10
text: Galatians 3:25–27
preacher: josh-sheldon
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2021/210110-ABaptismalMessage.mp3
audioBytes: 48076217
youtube: Mj56RO54wcM
---
