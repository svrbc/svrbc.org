---
id: 2025-01-19-pm
title: Lending to the Lord
date: 2025-01-19
text: Proverbs 19:17
preacher: conley-owens
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2025/250119-LendingtotheLord.aac
audioBytes: 48524508
youtube: aqAfl1eNp8c
---
