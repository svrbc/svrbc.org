---
id: 2024-10-20-am
title: Humble Prayer
date: 2024-10-20
text: Luke 18:9–14
preacher: conley-owens
series: luke
audio: https://storage.googleapis.com/pbc-ca-sermons/2024/241020-HumblePrayer.aac
audioBytes: 73537977
youtube: J-L9uJkaORQ
---
