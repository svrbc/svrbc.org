---
id: 2009-01-04-am
title: Seeing God
date: 2009-01-04
text: Matthew 5:8
preacher: james-torrefranca
audio: https://storage.googleapis.com/pbc-ca-sermons/2009/2009-01-04+Seeing+God.mp3
audioBytes: 62013375
---
