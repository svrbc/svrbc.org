---
id: 2020-02-16-am
title: To Live Worthy Of The Gospel
date: 2020-02-16
text: Philippians 1:27–30
preacher: josh-sheldon
series: philippians
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2020/200216-AM-ToLiveWorthyOfTheGospel.mp3
audioBytes: 54674670
youtube: _tu3nQs2qf4
---
