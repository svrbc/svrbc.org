---
id: 2021-08-15-am
title: The Gospel of the Kingdom
date: 2021-08-15
text: Luke 13:18–21
preacher: brian-garcia
series: the-gospel-of-the-kingdom
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2021/210815-TheGospelOfTheKingdom.aac
youtube: XFRYeYQ8ml0
---
