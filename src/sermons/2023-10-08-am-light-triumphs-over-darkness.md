---
id: 2023-10-08-am
title: Light Triumphs Over Darkness
date: 2023-10-08
text: Luke 8:26–39
preacher: brian-garcia
series: luke
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2023/231008-LightTriumphsOverDarkness.aac
youtube: OJ41EyvTiGM
---
