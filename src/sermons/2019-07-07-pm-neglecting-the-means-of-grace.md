---
id: 2019-07-07-pm
title: Neglecting The Means Of Grace
date: 2019-07-07
text: Isaiah 7:10–25
preacher: conley-owens
series: isaiah
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2019/190707-PM-NeglectingTheMeansOfGrace.mp3
audioBytes: 39059290
---
