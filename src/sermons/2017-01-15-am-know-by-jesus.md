---
id: 2017-01-15-am
title: Know By Jesus
date: 2017-01-15
text: Matthew 7:21–29
preacher: josh-sheldon
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2017/170115-AM-KnowByJesus-complete.mp3
audioBytes: 57743662
youtube: siyIwe2MGxs
---
