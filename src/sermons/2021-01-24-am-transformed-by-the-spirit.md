---
id: 2021-01-24-am
title: Transformed by the Spirit
date: 2021-01-24
text: 1 Thessalonians 1:5–10
preacher: josh-sheldon
series: awaiting-christ
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2021/210124-TransformedByTheSpirit.mp3
audioBytes: 72268530
youtube: VmHrhdeXhDI
---
