---
id: 2023-12-17-pm
title: Weak Hands
date: 2023-12-17
text: Isaiah 35:3–4
preacher: conley-owens
series: isaiah
audio: https://storage.googleapis.com/pbc-ca-sermons/2023/231217-WeakHands.aac
youtube: aFQkrKFVO3k
---
