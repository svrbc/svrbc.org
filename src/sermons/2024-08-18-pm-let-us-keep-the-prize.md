---
id: 2024-08-18-pm
title: Let Us Keep The Prize
date: 2024-08-18
text: Colossians 2:16–23
preacher: josh-sheldon
series: the-sufficiency-of-christ
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2024/240818-LetUsKeepThePrize.aac
youtube: EKAq0a0C7zA
---
