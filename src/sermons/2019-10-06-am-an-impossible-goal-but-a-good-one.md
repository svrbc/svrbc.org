---
id: 2019-10-06-am
title: 'An Impossible Goal, But A Good One'
date: 2019-10-06
text: Hebrews 12:1–17
preacher: josh-sheldon
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2019/191006-AM-AnImpossibleGoalButAGoodOne.mp3
audioBytes: 43216317
---
