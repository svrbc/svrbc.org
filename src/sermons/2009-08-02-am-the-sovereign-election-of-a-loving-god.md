---
id: 2009-08-02-am
title: The Sovereign Election of a Loving God
date: 2009-08-02
text: Deuteronomy 7:1–11
preacher: josh-sheldon
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2009/2009-08-02+The+Sovereign+Election+of+a+Loving+God.mp3
audioBytes: 48687306
---
