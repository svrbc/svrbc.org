---
id: 2017-01-15-pm
title: Though Troubles Assails
date: 2017-01-15
text: Psalm 3
preacher: josh-sheldon
series: the-psalms
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2017/170115+Afternoon+Service.mp3
audioBytes: 30439200
youtube: Hcqitc6yJXQ
---
