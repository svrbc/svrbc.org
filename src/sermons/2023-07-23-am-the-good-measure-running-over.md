---
id: 2023-07-23-am
title: The Good Measure Running Over
date: 2023-07-23
text: Luke 6:37–42
preacher: brian-garcia
series: luke
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2023/230723-TheGoodMeasureRunningOver.aac
youtube: K8jg9AHZqoY
---
