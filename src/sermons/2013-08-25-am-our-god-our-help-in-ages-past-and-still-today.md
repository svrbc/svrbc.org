---
id: 2013-08-25-am
title: 'Our God, Our Help in Ages Past and Still Today'
date: 2013-08-25
text: Psalm 121
preacher: josh-sheldon
series: psalms-of-ascents
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2013/2013-08-25+Our+God,+Our+Help_In+Ages+Past+and+Still+Today.mp3
audioBytes: 40363152
---
