---
id: 2007-04-29-am
title: The Welcomed Word
date: 2007-04-29
text: Acts 2:14–41
preacher: josh-sheldon
series: acts
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2007/2007-04-29+The+Welcomed+Word.mp3
audioBytes: 43560708
---
