---
id: 2009-04-12-am
title: The Answer is the Resurrection
date: 2009-04-12
text: Matthew 26:36–46
preacher: josh-sheldon
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2009/2009-04-12+The+Answer+is+the+Resurrection.mp3
audioBytes: 48615999
---
