---
id: 2023-06-04-pm
title: A Fire In Zion
date: 2023-06-04
text: Isaiah 31:6–9
preacher: conley-owens
series: isaiah
audio: https://storage.googleapis.com/pbc-ca-sermons/2023/230604-AFireinZion.aac
youtube: usWxqsqXsI0
---
