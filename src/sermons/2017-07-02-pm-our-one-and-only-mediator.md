---
id: 2017-07-02-pm
title: Our One And Only Mediator
date: 2017-07-02
text: 1 Timothy 2:5–7
preacher: josh-sheldon
series: 1-timothy
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2017/170702+PM+Service+-+Our+One+And+Only+Mediator.mp3
audioBytes: 26369221
youtube: '-AbhYdkZY6o'
---
