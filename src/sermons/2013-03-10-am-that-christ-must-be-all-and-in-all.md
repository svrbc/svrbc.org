---
id: 2013-03-10-am
title: 'That Christ Must Be All, and In All'
date: 2013-03-10
text: Colossians 3:11
preacher: josh-sheldon
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2013/2013-03-10+That+Christ+Must+Be+All%2C+and+In+All.mp3
audioBytes: 46357944
---
