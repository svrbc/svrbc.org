---
id: 2014-02-23-am
title: All for God’s Glory
date: 2014-02-23
text: John 12:27–36
preacher: josh-sheldon
series: john
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2014/2014-02-23+All+for+God%27s+Glory.mp3
audioBytes: 51882360
---
