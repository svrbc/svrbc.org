---
id: 2014-09-14-am
title: Jesus’ Prayer of Consecration Part 1
date: 2014-09-14
text: John 17:1–5
preacher: josh-sheldon
series: john
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2014/140914+Jesus+-+Prayer+of+Consecration+Part+1.mp3
audioBytes: 42042183
---
