---
id: 2012-08-12-am
title: Knowing and Knowing
date: 2012-08-12
text: John 8:12–30
preacher: josh-sheldon
series: john
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2012/2012-08-12+Knowing+and+Knowing.mp3
audioBytes: 55531944
---
