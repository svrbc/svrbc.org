---
id: 2017-07-02-am
title: God’s Unwavering Love
date: 2017-07-02
text: Romans 8:35–39
preacher: josh-sheldon
series: romans
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2017/170702+Gods+Unwavering+Love.mp3
audioBytes: 55379332
youtube: 69CBl9kiaXE
---
