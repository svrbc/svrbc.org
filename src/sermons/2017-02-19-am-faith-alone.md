---
id: 2017-02-19-am
title: Faith Alone
date: 2017-02-19
text: Romans 4:1–8
preacher: josh-sheldon
series: romans
audio: https://storage.googleapis.com/pbc-ca-sermons/2017/170219+Faith+Alone.mp3
audioBytes: 44268705
youtube: RbmhTfvDl-w
---
