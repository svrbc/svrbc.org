---
id: 2025-01-05-pm
title: The Works of the Flesh
date: 2025-01-05
text: Galatians 5:19–21
preacher: tim-mullet
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2025/250105-TheWorksoftheFlesh.aac
audioBytes: 52682366
youtube: 9w5oadFcXik
---
