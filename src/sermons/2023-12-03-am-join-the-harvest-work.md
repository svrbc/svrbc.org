---
id: 2023-12-03-am
title: Join the Harvest Work!
date: 2023-12-03
text: Luke 10:1–12
preacher: brian-garcia
series: luke
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2023/231203-JoinTheHarvestWork.aac
youtube: UQr5u8_RcA4
---
