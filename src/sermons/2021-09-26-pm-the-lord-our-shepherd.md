---
id: 2021-09-26-pm
title: The Lord Our Shepherd
date: 2021-09-26
text: Psalm 23
preacher: josh-sheldon
series: the-psalms
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2021/210926-TheLordOurShepherd.aac
youtube: ojguZ3HhkPM
---
