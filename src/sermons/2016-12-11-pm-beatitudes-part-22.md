---
id: 2016-12-11-pm
title: Beatitudes Part 22
date: 2016-12-11
text: Matthew 7:7–11
preacher: josh-sheldon
series: beatitudes
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2016/161211+Afternoon+Service+-+Beatitudes+Part+22.mp3
audioBytes: 28164297
youtube: 5UFD7MflMy0
---
