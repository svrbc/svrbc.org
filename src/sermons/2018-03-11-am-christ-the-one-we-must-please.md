---
id: 2018-03-11-am
title: 'Christ, The One We Must Please'
date: 2018-03-11
text: Romans 15:1–13
preacher: josh-sheldon
series: romans
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2018/180311-AM-ChristTheOneWeMustPlease.mp3
audioBytes: 44994724
youtube: is2rgz_4-p8
---
