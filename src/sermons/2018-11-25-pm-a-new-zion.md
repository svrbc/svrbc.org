---
id: 2018-11-25-pm
title: A New Zion
date: 2018-11-25
text: Isaiah 2:1–4
preacher: conley-owens
series: isaiah
audio: https://storage.googleapis.com/pbc-ca-sermons/2018/181125-PM-ANewZion.mp3
audioBytes: 22359702
youtube: _sObE56dTPc
---
