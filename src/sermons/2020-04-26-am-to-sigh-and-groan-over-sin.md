---
id: 2020-04-26-am
title: To Sigh and Groan Over Sin
date: 2020-04-26
text: Ezekiel 9
preacher: josh-sheldon
series: sovereignty-and-covid19
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2020/200426-ToSighandGroanOverSin.mp3
audioBytes: 64154206
youtube: tXzwDp2HrQs
---
