---
id: 2021-10-17-pm
title: Giving as Shared Suffering
date: 2021-10-17
text: Galatians 6:6
preacher: conley-owens
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2021/211017-GivingAsSharedSuffering.aac
youtube: NvskifGAAPo
---
