---
id: 2007-10-07-am
title: That We Might Not Settle
date: 2007-10-07
text: 1 Samuel 11:13
preacher: josh-sheldon
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2007/2007-10-07+That+We+Might+Not+Settle.mp3
audioBytes: 50068410
---
