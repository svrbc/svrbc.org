---
id: 2024-06-16-am
title: The Power Of Reconciliation
date: 2024-06-16
text: Luke 17:1–4
preacher: conley-owens
series: luke
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2024/240616-ThePowerOfReconciliation.aac
youtube: mhUnAZ1C2h8
---
