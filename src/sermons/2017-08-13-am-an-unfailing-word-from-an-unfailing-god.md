---
id: 2017-08-13-am
title: An Unfailing Word From An Unfailing God
date: 2017-08-13
text: Romans 9:1–6
preacher: josh-sheldon
series: romans
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2017/170813+An+Unfailing+Word+From+An+Unfailing+God.mp3
audioBytes: 49536279
youtube: YVYmGk1Ng5M
---
