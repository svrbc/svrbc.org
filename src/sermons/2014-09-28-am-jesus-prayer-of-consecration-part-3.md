---
id: 2014-09-28-am
title: 'Jesus’ Prayer of Consecration, Part 3'
date: 2014-09-28
text: John 17:20–26
preacher: josh-sheldon
series: john
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2014/140928+Jesus+Prayer+of+Consecration+Part+3.mp3
audioBytes: 47280465
---
