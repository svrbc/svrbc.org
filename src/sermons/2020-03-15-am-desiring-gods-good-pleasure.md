---
id: 2020-03-15-am
title: Desiring God’s Good Pleasure
date: 2020-03-15
text: Philippians 2:12–18
preacher: josh-sheldon
series: philippians
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2020/200315-AM-DesiringGodsGoodPleasure.mp3
audioBytes: 47314409
youtube: N21llfc5Qqc
---
