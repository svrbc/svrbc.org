---
id: 2018-08-12-pm
title: The Lamb’s Silence
date: 2018-08-12
text: Matthew 26:62–63
preacher: josh-sheldon
series: meditations-upon-the-cross
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2018/180812-PM-TheLambsSilence.mp3
audioBytes: 16227841
---
