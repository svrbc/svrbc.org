---
id: 2020-04-12-am
title: 'The Resurrection, A Living Hope'
date: 2020-04-12
text: 1 Peter 1:3–5
preacher: josh-sheldon
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2020/GMT-200412-AM-TheResurrectionALivingHope.mp3
audioBytes: 49250104
youtube: df1P8YgiuLc
---
