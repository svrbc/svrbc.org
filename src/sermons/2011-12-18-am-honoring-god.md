---
id: 2011-12-18-am
title: Honoring God
date: 2011-12-18
text: John 5:16–30
preacher: josh-sheldon
series: john
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2011/2011-12-18+Honoring+God.mp3
audioBytes: 42749226
---
