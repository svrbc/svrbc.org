---
id: 2023-06-25-pm
title: Do Not Pray For Apostates
date: 2023-06-25
text: 1 John 5:16–17
preacher: conley-owens
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2023/230625-DoNotPrayForApostates.aac
youtube: 8tHvAI3_O2E
---
