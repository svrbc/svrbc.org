---
id: 2008-09-21-am
title: Blessings and Folly
date: 2008-09-21
text: 2 Chronicles 14–16
preacher: josh-sheldon
series: kings-of-judah
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2008/2008-09-21+Blessings+and+Folly.mp3
audioBytes: 52886079
---
