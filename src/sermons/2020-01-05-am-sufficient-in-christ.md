---
id: 2020-01-05-am
title: Sufficient In Christ
date: 2020-01-05
text: Philippians 1:1–2
preacher: josh-sheldon
series: philippians
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2020/200105-AM-SufficientInChrist.mp3
audioBytes: 44706332
---
