---
id: 2013-09-29-am
title: The Lord’s Work in My Mother
date: 2013-09-29
text: >-
  Genesis 50:20; Exodus 33:19; Job 42:2; Psalm 139:16; Proverbs 16:9; Isaiah
  45:6–7
preacher: wally-nakamura
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2013/2013-09-29+The+Lords+Work+in+My+Mother.mp3
audioBytes: 45179919
---
