---
id: 2023-08-13-pm
title: Learning In The Belly Of The Fish
date: 2023-08-13
text: Jonah 1:17
preacher: brian-garcia
series: jonah
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2023/230813-LearningInTheBellyOfTheFish.aac
youtube: WFBqRdUC4rA
---
