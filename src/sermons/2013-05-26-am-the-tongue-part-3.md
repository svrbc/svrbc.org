---
id: 2013-05-26-am
title: 'The Tongue, Part 3'
date: 2013-05-26
text: James 4:13–17
preacher: steve-mixsell
series: james
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2013/2013-05-26+The+Tongue+Part+3.mp3
audioBytes: 68451438
---
