---
id: 2008-12-07-am
title: Seeing God
date: 2008-12-07
text: Matthew 5:8
preacher: josh-sheldon
audio: https://storage.googleapis.com/pbc-ca-sermons/2008/2008-12-07+Seeing+God.mp3
audioBytes: 45153231
---
