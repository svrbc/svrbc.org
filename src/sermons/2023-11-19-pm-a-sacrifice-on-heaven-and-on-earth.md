---
id: 2023-11-19-pm
title: A Sacrifice on Heaven and on Earth
date: 2023-11-19
text: Isaiah 34:5–7
preacher: conley-owens
series: isaiah
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2023/231119-ASacrificeonHeavenandonEarth.aac
youtube: qmJDLWqrNAc
---
