---
id: 2017-03-26-pm
title: The Savior Who Stayed
date: 2017-03-26
text: Luke 23:32–43
preacher: josh-sheldon
series: the-seven-last-sayings
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2017/170326+PM+Service+-+The+Savior+Who+Stayed.mp3
audioBytes: 28076159
youtube: ZpdvLMRvT84
---
