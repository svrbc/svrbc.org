---
id: 2017-02-26-am
title: In Abraham’s Footsteps
date: 2017-02-26
text: Romans 4:9–25
preacher: josh-sheldon
series: romans
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2017/170226+In+Abrahams+Footsteps.mp3
audioBytes: 49044686
youtube: B9apZHFpNDo
---
