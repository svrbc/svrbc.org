---
id: 2014-01-19-am
title: The King Has Come
date: 2014-01-19
text: John 12:12–19
preacher: josh-sheldon
series: john
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2014/2014-01-19+The+King+Has+Come.mp3
audioBytes: 49719798
---
