---
id: 2018-09-16-pm
title: The Tale Of Two Cities
date: 2018-09-16
text: Isaiah 1:21–31
preacher: conley-owens
series: isaiah
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2018/180916-PM-TheTaleOfTwoCities.mp3
audioBytes: 32228152
youtube: hl5N2kjb_24
---
