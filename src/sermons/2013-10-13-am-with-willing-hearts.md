---
id: 2013-10-13-am
title: With Willing Hearts
date: 2013-10-13
text: Exodus 35
preacher: josh-sheldon
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2013/2013-10-13+With+Willing+Hearts.mp3
audioBytes: 33813750
---
