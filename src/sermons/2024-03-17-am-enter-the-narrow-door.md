---
id: 2024-03-17-am
title: Enter The Narrow Door
date: 2024-03-17
text: Luke 13:22–30
preacher: brian-garcia
series: luke
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2024/240317-EnterTheNarrowDoor.aac
youtube: rR8xonf6w7k
---
