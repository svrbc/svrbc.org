---
id: 2022-09-11-pm
title: The Pleasant Vineyard
date: 2022-09-11
text: Isaiah 27:2–6
preacher: conley-owens
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2022/220911-ThePleasantVineyard.aac
youtube: swwRrEg_Mdk
---
