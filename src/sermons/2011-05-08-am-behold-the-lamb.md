---
id: 2011-05-08-am
title: Behold the Lamb
date: 2011-05-08
text: John 1:29–34
preacher: josh-sheldon
series: john
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2011/2011-05-08+Behold+the+Lamb.mp3
audioBytes: 38488704
---
