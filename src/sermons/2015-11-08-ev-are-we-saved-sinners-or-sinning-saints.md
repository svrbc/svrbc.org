---
id: 2015-11-08-ev
title: Are We Saved Sinners or Sinning Saints?
date: 2015-11-08
text: Jeremiah 17:9
preacher: josh-sheldon
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2015/151108+Are+We+Saved+Sinner+or+Sinning+Saints.mp3
audioBytes: 47927497
---

This message was delivered at an evening gathering of several local churches.
