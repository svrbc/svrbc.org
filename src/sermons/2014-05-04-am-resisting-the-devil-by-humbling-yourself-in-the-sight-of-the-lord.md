---
id: 2014-05-04-am
title: Resisting the Devil… By Humbling Yourself in the Sight of the Lord
date: 2014-05-04
text: James 4:7–17
preacher: steve-mixsell
series: james
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2014/2014-05-04+Resisting+The+Devil+.mp3
audioBytes: 76024575
---
