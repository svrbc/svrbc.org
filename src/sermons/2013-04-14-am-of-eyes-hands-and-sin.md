---
id: 2013-04-14-am
title: 'Of Eyes, Hands and Sin'
date: 2013-04-14
text: Matthew 5:29–30
preacher: josh-sheldon
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2013/2013-04-14+Of+Eyes+Hands+and+Sin.mp3
audioBytes: 45726606
---
