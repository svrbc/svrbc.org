---
id: 2014-07-27-am
title: From Love To Hatred
date: 2014-07-27
text: John 15:18–27
preacher: josh-sheldon
series: john
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2014/2014-07-27+From+Love+To+Hatred+-+All+The+World+Has+To+Offer.mp3
audioBytes: 50448714
---
