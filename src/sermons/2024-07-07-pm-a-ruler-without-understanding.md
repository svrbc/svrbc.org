---
id: 2024-07-07-pm
title: A Ruler Without Understanding
date: 2024-07-07
text: Proverbs 28:16
preacher: conley-owens
unregisteredSeries: Proverbs
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2024/240707-ARulerWithoutUnderstanding.aac
youtube: xHYD4Z2OIBE
---
