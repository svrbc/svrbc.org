---
id: 2021-04-04-am
title: Recalled to Life
date: 2021-04-04
text: 1 Thessalonians 3:6–10
preacher: josh-sheldon
series: awaiting-christ
audio: https://storage.googleapis.com/pbc-ca-sermons/2021/210404-RecalledToLife.mp3
audioBytes: 59236354
youtube: dpPYdZCajbQ
---
