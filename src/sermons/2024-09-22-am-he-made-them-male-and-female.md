---
id: 2024-09-22-am
title: He Made them Male and Female
date: 2024-09-22
text: Genesis 1:27
preacher: tim-mullet
series: basic-truths
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2024/240922-BasicTruthsfromGenesisHemadethemMaleandFemale.aac
audioBytes: 73742585
youtube: 25nMOCr1GhA
---
