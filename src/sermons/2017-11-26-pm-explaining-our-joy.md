---
id: 2017-11-26-pm
title: Explaining Our Joy
date: 2017-11-26
text: John 16:16–25
preacher: josh-sheldon
series: the-road-to-the-cross
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2017/171126+PM+Service+-+Explaining+Our+Joy.mp3
audioBytes: 24724959
youtube: 6ZuX3dAcXwk
---
