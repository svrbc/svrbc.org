---
id: 2025-01-26-pm
title: The Voice in the Wilderness
date: 2025-01-26
text: Isaiah 40:3–5
preacher: conley-owens
series: isaiah
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2025/250126-TheVoiceInTheWilderness.aac
audioBytes: 36215517
youtube: mMayHQkjK8g
---
