---
id: 2024-12-01-pm
title: The Fruit of the Spirit - Part 1
date: 2024-12-01
text: Galatians 5:16–26
preacher: tim-mullet
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2024/241201-TheFruitoftheSpirit-Part1.aac
audioBytes: 57120363
youtube: V094ws2__Mc
---
