---
id: 2009-08-23-am
title: The Greatest Is Not Good Enough
date: 2009-08-23
text: Acts 18:24–19:20
preacher: josh-sheldon
series: acts
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2009/2009-08-23+The+Greatest+Is+Not+Good+Enough.mp3
audioBytes: 47239482
---
