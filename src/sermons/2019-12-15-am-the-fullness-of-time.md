---
id: 2019-12-15-am
title: The Fullness Of Time
date: 2019-12-15
text: Galatians 4:1–7
preacher: josh-sheldon
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2019/191215-AM-TheFullnessOfTime.mp3
audioBytes: 42152179
---
