---
id: 2023-09-03-pm
title: The Betrayer Betrayed
date: 2023-09-03
text: Isaiah 33:1
preacher: conley-owens
series: isaiah
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2023/230903-TheBetrayerBetrayed.aac
youtube: oEKJt169hWw
---
