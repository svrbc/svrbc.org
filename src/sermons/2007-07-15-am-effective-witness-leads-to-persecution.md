---
id: 2007-07-15-am
title: Effective Witness Leads to Persecution
date: 2007-07-15
text: Acts 4:13–22
preacher: josh-sheldon
series: acts
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2007/2007-07-15+Effective+Witness+Leads+to+Persecution.mp3
audioBytes: 50183919
---
