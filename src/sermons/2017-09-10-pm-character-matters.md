---
id: 2017-09-10-pm
title: Character Matters
date: 2017-09-10
text: 1 Timothy 6:5–10
preacher: josh-sheldon
series: 1-timothy
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2017/170910+PM+Service+-+Chraracter+Matters.mp3
audioBytes: 32867929
youtube: 6cSUm8GYtnI
---
