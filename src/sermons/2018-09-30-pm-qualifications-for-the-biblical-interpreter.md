---
id: 2018-09-30-pm
title: Qualifications For The Biblical Interpreter
date: 2018-09-30
text: John 6:41–51
preacher: josh-sheldon
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2018/180930-PM-QualificationsForTheBiblicalInterpreter.mp3
audioBytes: 41485133
---
