---
id: 2023-09-03-am
title: God on Your Side
date: 2023-09-03
text: Psalm 124
preacher: josh-sheldon
unregisteredSeries: Psalms of Ascent
audio: https://storage.googleapis.com/pbc-ca-sermons/2023/230903-GodOnYourSide.aac
youtube: 9KDMGEUENdk
---
