---
id: 2016-08-07-am
title: Crying out to God
date: 2016-08-07
text: Psalm 77
preacher: josh-sheldon
series: the-psalms
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2016/160807+Crying+Out+To+God.mp3
audioBytes: 43900857
youtube: kkIvVTzGhWs
---
