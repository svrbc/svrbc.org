---
id: 2023-01-08-am
title: 'The Gospel of Luke: Rejoicing in God our Savior'
date: 2023-01-08
text: Luke 1:39–56
preacher: brian-garcia
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2023/230108-TheGospelOfLuke-RejoicingInGodOurSavior.aac
youtube: rvL30HQ9SF8
---
