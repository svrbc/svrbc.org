---
id: 2017-09-17-pm
title: The Man of God
date: 2017-09-17
text: 1 Timothy 6:11
preacher: josh-sheldon
series: 1-timothy
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2017/170917+PM+Service+-+The+Man+of+God.mp3
audioBytes: 25372370
youtube: EmsYRwnV-18
---
