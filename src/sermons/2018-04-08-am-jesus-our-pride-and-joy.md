---
id: 2018-04-08-am
title: 'Jesus, Our Pride And Joy'
date: 2018-04-08
text: Romans 15:22–29
preacher: josh-sheldon
series: romans
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2018/180408-AM-MoreThanAnItinerary.mp3
audioBytes: 42360739
youtube: 3K4-5zKBuus
---
