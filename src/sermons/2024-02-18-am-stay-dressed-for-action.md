---
id: 2024-02-18-am
title: Stay Dressed For Action
date: 2024-02-18
text: Luke 12:35–48
preacher: brian-garcia
series: luke
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2024/240218-StayDressedForAction.aac
youtube: 4mEaRIh29KU
---
