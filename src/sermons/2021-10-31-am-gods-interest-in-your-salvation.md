---
id: 2021-10-31-am
title: God’s Interest in Your Salvation
date: 2021-10-31
text: 2 Thessalonians 2:13–14
preacher: josh-sheldon
series: awaiting-christ
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2021/211031%3DGodsInterestInYourSalvation.aac
audioBytes: 46350239
youtube: m4Ig-zvdRQE
---
