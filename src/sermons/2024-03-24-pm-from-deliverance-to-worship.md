---
id: 2024-03-24-pm
title: From Deliverance To Worship
date: 2024-03-24
text: Psalm 70
preacher: josh-sheldon
unregisteredSeries: Psalms of Ascent
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2024/241224-FromDeliveranceToWorship.aac
youtube: Vt8rZjSHcMM
---
