---
id: 2011-04-17-am
title: The Word Became Flesh
date: 2011-04-17
text: John 1:14–18
preacher: josh-sheldon
series: john
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2011/2011-04-17+The+Word+Became+Flesh.mp3
audioBytes: 46394496
---
