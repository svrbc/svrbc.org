---
id: 2024-10-06-am
title: Pets Are Not People
date: 2024-10-06
text: Genesis 2:18–20
preacher: tim-mullet
series: basic-truths
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2024/241006-BasicTruthsfromGenesisPetsAreNotPeople.aac
audioBytes: 65715365
youtube: BgW8xUEFaFI
---
