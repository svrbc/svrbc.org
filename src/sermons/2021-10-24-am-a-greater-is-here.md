---
id: 2021-10-24-am
title: A Greater is Here
date: 2021-10-24
text: Chronicles 30:22
preacher: josh-sheldon
audio: https://storage.googleapis.com/pbc-ca-sermons/2021/211024-AGreaterIsHere.aac
youtube: 2fDxK1KDMFk
---
