---
id: 2024-06-09-pm
title: The Gospel Of Christ - Personally Yours  II
date: 2024-06-09
text: Ezekiel 36:26
preacher: josh-sheldon
unregisteredSeries: The Gospel Of Christ
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2024/240609-TheGospelOfChrist-PersonallyYours2.aac
youtube: IcaFModRKWY
---
