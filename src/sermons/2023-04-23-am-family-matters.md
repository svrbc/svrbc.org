---
id: 2023-04-23-am
title: Family Matters
date: 2023-04-23
text: Luke 3:23–38
preacher: brian-garcia
series: luke
audio: https://storage.googleapis.com/pbc-ca-sermons/2023/230423-FamilyMatters.aac
youtube: 8OBDRqynkY8
---
