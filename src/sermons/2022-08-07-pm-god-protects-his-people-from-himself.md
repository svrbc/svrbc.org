---
id: 2022-08-07-pm
title: God Protects his People (from Himself)
date: 2022-08-07
text: Psalm 102
preacher: josh-sheldon
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2022/220807-GodProtectsHisPeopleFromHimself.aac
audioBytes: 26340913
youtube: QtzqY_dV6Fo
---
