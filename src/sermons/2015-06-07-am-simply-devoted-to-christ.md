---
id: 2015-06-07-am
title: Simply Devoted to Christ
date: 2015-06-07
text: 2 Corinthians 11:3
preacher: josh-sheldon
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2015/150607+Simply+Devoted+to+Christ.mp3
audioBytes: 42851369
---
