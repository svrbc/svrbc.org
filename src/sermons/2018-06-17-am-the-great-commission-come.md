---
id: 2018-06-17-am
title: 'The Great Commission: “Come!”'
date: 2018-06-17
text: Matthew 28:16–20
preacher: josh-sheldon
series: church-membership
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2018/180617-AM-TheGreatCommission-Come.mp3
audioBytes: 51982586
youtube: 0n83G1ctYBg
---
