---
id: 2019-06-16-pm
title: Firm In Faith
date: 2019-06-16
text: Isaiah 7:1–9
preacher: conley-owens
series: isaiah
audio: https://storage.googleapis.com/pbc-ca-sermons/2019/190616-PM-FirmingFaith.mp3
audioBytes: 45146428
---
