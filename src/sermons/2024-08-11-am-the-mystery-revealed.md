---
id: 2024-08-11-am
title: The Mystery Revealed
date: 2024-08-11
text: Colossians 1:24–2:5
preacher: josh-sheldon
series: the-sufficiency-of-christ
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2024/240811-TheMysteryRevealed.aac
youtube: hLFyDehVAvw
---
