---
id: 2023-08-06-am
title: Your Anxiety Belongs To Jesus
date: 2023-08-06
text: 1 Peter 5:5–8
preacher: josh-sheldon
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2023/230806-YourAnxietyBelongsToJesus.aac
youtube: TZzF8uTKx5w
---
