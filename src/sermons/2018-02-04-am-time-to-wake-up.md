---
id: 2018-02-04-am
title: Time To Wake Up
date: 2018-02-04
text: Romans 13:11–14
preacher: josh-sheldon
series: romans
audio: https://storage.googleapis.com/pbc-ca-sermons/2018/180204-AM-TimeToWakeUp.mp3
audioBytes: 56090691
youtube: o65U4iMvu_E
---
