---
id: 2022-12-11-pm
title: The Potter is not the Clay
date: 2022-12-11
text: Isaiah 29:15–16
preacher: conley-owens
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2022/221211-ThePotterisnottheClay.aac
audioBytes: 28637627
youtube: vallzycXoOM
---
