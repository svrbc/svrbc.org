---
id: 2023-12-24-pm
title: Our God - Faithful and True
date: 2023-12-24
text: Matthew 1:1–17
preacher: josh-sheldon
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2023/231224-OurGodFaithfulandTrue.aac
youtube: vx6UPgaG4vE
---
