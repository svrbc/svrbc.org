---
id: 2009-07-05-am
title: Looking Unto Him Alone
date: 2009-07-05
text: Hebrews 12:1–2
preacher: josh-sheldon
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2009/2009-07-05+Looking+Unto+Him+Alone.mp3
audioBytes: 41009919
---
