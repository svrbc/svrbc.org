---
id: 2013-10-06-am
title: The Persecuted Church
date: 2013-10-06
text: Revelation 2:8–11
preacher: jesse-lu
series: the-churches-of-revelation
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2013/2013-10-06+The+Persecuted+Church.mp3
audioBytes: 64937796
---
