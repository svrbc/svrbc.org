---
id: 2018-09-02-pm
title: The Lord With Us
date: 2018-09-02
text: Haggai 1:12–15
preacher: josh-sheldon
series: minor-prophets
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2018/180902-PM-TheLordWithUs.mp3
audioBytes: 28323994
---
