---
id: 2008-06-15-am
title: Come and See
date: 2008-06-15
text: Acts 10:24–33
preacher: josh-sheldon
series: acts
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2008/2008-06-15+Come+and+See.mp3
audioBytes: 53032863
---
