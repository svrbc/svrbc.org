---
id: 2007-11-18-am
title: A Descendant Or A Child
date: 2007-11-18
text: Acts 7:1–53
preacher: josh-sheldon
series: acts
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2007/2007-11-18+A+Descendant+Or+A+Child.mp3
audioBytes: 48151044
---
