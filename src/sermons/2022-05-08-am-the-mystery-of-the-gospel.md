---
id: 2022-05-08-am
title: The Mystery of the Gospel
date: 2022-05-08
text: Ephesians 3:1–6
preacher: brian-garcia
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2022/220508-TheMysteryoftheGospel.aac
youtube: qDfsP92Mk2o
---
