---
id: 2024-05-05-am
title: Joy from the Word of Life
date: 2024-05-05
text: 1 John 1:1–4
unregisteredPreacher: Tim Mullet
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2024/240505-JoyFromTheWordOfLife.aac
youtube: edNn4VFtx9k
---
