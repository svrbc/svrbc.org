---
id: 2024-12-29-am
title: There Are Two Kinds of Counsel - Part 2
date: 2024-12-29
text: Genesis 3:1–6
preacher: tim-mullet
series: basic-truths
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2024/241229-BasicTruths-ThereAreTwoKindsofCounsel-Part2.aac
audioBytes: 60700021
youtube: dSKw_0yVSaI
---
