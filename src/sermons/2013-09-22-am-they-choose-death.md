---
id: 2013-09-22-am
title: They Choose Death
date: 2013-09-22
text: John 11:45–57
preacher: josh-sheldon
series: john
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2013/2013-09-22+They+Choose+Death.mp3
audioBytes: 40771395
---
