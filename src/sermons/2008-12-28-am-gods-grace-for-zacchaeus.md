---
id: 2008-12-28-am
title: God’s Grace for Zacchaeus
date: 2008-12-28
text: Luke 19
preacher: henry-wiley
audio: https://storage.googleapis.com/pbc-ca-sermons/2008/2008-12-28+sermon.mp3
audioBytes: 31340106
---
