---
id: 2024-01-14-pm
title: From Despair To Hope
date: 2024-01-14
text: Psalm 130
preacher: josh-sheldon
unregisteredSeries: Psalm of Ascents
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2024/240114-FromDespairToHope.aac
youtube: LuUTys4PKhM
---
