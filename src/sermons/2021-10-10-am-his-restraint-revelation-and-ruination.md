---
id: 2021-10-10-am
title: 'His Restraint, Revelation, and Ruination'
date: 2021-10-10
text: 2 Thessalonians 2:5–8
preacher: josh-sheldon
series: awaiting-christ
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2021/211010-HisRestraintRevelation%26Ruination.aac
audioBytes: 48178959
youtube: R43UTweXO_M
---
