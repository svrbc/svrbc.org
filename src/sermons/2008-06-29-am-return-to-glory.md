---
id: 2008-06-29-am
title: Return to Glory
date: 2008-06-29
text: Acts 11:1–18; 15:1–21
preacher: josh-sheldon
series: acts
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2008/2008-06-29+Return+to+Glory.mp3
audioBytes: 51596298
---
