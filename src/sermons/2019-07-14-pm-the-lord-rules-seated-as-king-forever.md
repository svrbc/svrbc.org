---
id: 2019-07-14-pm
title: The Lord Rules Seated As King Forever
date: 2019-07-14
text: Psalm 29
preacher: jesus-mancilla
series: kingship-psalms
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2019/190714-PM-TheLordRulesSeatedAsKingFoever.mp3
audioBytes: 40790472
---
