---
id: 2019-05-12-am
title: Election Brings Humility
date: 2019-05-12
text: Nehemiah 9:7–8
preacher: josh-sheldon
series: ezra-nehemiah
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2019/190512-AM-ElectionBringsHumility.mp3
audioBytes: 55679848
---
