---
id: 2011-06-19-am
title: Finding Christ
date: 2011-06-19
text: John 1:38–48
preacher: josh-sheldon
series: john
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2011/2011-06-19+Finding+Christ.mp3
audioBytes: 50913024
---
