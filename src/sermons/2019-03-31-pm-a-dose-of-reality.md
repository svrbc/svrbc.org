---
id: 2019-03-31-pm
title: A Dose Of Reality
date: 2019-03-31
text: Zechariah 3
preacher: josh-sheldon
series: zechariah
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2019/190331-PM-ADoseOfReality.mp3
audioBytes: 39193445
---
