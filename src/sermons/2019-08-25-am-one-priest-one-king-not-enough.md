---
id: 2019-08-25-am
title: 'One Priest, One King – Not Enough'
date: 2019-08-25
text: Nehemiah 13:25, 30–31
preacher: josh-sheldon
series: ezra-nehemiah
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2019/190825-AM-OnePriestOneKing-NotEnough.mp3
audioBytes: 39584263
---
