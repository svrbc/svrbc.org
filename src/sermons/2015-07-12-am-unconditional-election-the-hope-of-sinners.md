---
id: 2015-07-12-am
title: 'Unconditional Election, the Hope of Sinners'
date: 2015-07-12
text: Ephesians 1:3–14
preacher: josh-sheldon
series: the-doctrines-of-grace
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2015/150712+Unconditional+Election+the+Hope+of+Sinners.mp3
audioBytes: 46185448
youtube: SBZjfHPBiXE
---
