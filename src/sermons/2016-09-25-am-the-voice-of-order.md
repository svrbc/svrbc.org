---
id: 2016-09-25-am
title: The Voice of Order
date: 2016-09-25
text: Psalm 29
preacher: josh-sheldon
series: the-psalms
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2016/160925+The+Voice+of+Order.mp3
audioBytes: 47212767
youtube: d4QREmFuHFc
---
