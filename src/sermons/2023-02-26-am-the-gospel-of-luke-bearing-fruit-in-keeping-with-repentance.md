---
id: 2023-02-26-am
title: 'The Gospel of Luke: Bearing Fruit in Keeping With Repentance'
date: 2023-02-26
text: Luke 3:7–14
preacher: brian-garcia
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2023/230226-BearingFruitInKeepingWithRepentance.aac
youtube: eN-AU9i_HU8
---
