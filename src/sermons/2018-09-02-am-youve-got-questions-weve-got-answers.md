---
id: 2018-09-02-am
title: 'You’ve Got Questions, We’ve Got Answers'
date: 2018-09-02
text: Ezra 5:6–16
preacher: josh-sheldon
series: ezra-nehemiah
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2018/180902-AM-YouveGotQuestions-WeveGotAnswers.mp3
audioBytes: 49662917
---
