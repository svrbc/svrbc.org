---
id: 2024-05-26-am
title: Walk In The Spirit
date: 2024-05-26
text: Galatians 5:13–24
preacher: henry-wiley
unregisteredSeries: Galatians
audio: https://storage.googleapis.com/pbc-ca-sermons/2024/240526-WalkInTheSpirit.aac
youtube: CrtRrhB345c
---
