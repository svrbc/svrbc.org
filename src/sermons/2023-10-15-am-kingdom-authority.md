---
id: 2023-10-15-am
title: Kingdom Authority
date: 2023-10-15
text: Luke 9:1–6
preacher: brian-garcia
series: luke
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2023/231015-KingdomAuthority.aac
youtube: Kmn0P22oBm8
---
