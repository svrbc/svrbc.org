---
id: 2024-04-07-pm
title: God Is Not Mocked
date: 2024-04-07
text: Proverbs 14:9
preacher: josh-sheldon
unregisteredSeries: Proverbs
audio: https://storage.googleapis.com/pbc-ca-sermons/2024/240407-GodIsNotMocked.aac
youtube: NuT-hTvfGBw
---
