---
id: 2016-05-01-am
title: God’s Mercy To His Enemies
date: 2016-05-01
text: 2 Kings 6:18–23
preacher: josh-sheldon
series: 12kings
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2016/160501+Gods+Mercy+to+His+Enemies.mp3
audioBytes: 46080524
youtube: 0Z4uvyeHbR0
---
