---
id: 2023-08-27-am
title: Don’t Be Offended
date: 2023-08-27
text: Luke 7:18–35
preacher: brian-garcia
series: luke
audio: https://storage.googleapis.com/pbc-ca-sermons/2023/230827-DontBeOffended.aac
youtube: 2OqQGt-OtcM
---
