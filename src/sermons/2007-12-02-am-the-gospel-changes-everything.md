---
id: 2007-12-02-am
title: The Gospel Changes Everything
date: 2007-12-02
text: Isaiah 43:1–7
preacher: josh-sheldon
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2007/2007-12-02+The+Gospel+Changes+Everything.mp3
audioBytes: 49245252
---
