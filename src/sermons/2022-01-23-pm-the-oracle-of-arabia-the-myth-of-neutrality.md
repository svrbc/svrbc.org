---
id: 2022-01-23-pm
title: 'The Oracle of Arabia: The Myth of Neutrality'
date: 2022-01-23
text: Isaiah 21:13–17
preacher: conley-owens
series: isaiah
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2022/220123-TheOracleOfArabia-TheMythOfNeutrality.aac
audioBytes: 27942611
youtube: UE2RPK0lF_Y
---
