---
id: 2020-02-16-pm
title: The Deliverer King
date: 2020-02-16
text: Isaiah 9:4–7
preacher: conley-owens
series: isaiah
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2020/200216-PM-TheDelivererKing.mp3
audioBytes: 36513902
youtube: S2Jr59Ge2SU
---
