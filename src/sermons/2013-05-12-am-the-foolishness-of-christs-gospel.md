---
id: 2013-05-12-am
title: The (Foolishness) of Christ’s Gospel
date: 2013-05-12
text: 1 Corinthians 1:18–31
preacher: mike-kelley
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2013/2013-05-12+The+%7BFoolishness%7D+of+Christ%27s+Gospel.mp3
audioBytes: 49017570
---
