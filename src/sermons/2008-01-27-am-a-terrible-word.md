---
id: 2008-01-27-am
title: A Terrible Word
date: 2008-01-27
text: Matthew 7:21–27
preacher: josh-sheldon
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2008/2008-01-27+A+Terrible+Word.mp3
audioBytes: 51600468
---
