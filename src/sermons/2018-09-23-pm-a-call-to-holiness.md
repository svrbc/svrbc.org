---
id: 2018-09-23-pm
title: A Call To Holiness
date: 2018-09-23
text: Haggai 2:10–19
preacher: josh-sheldon
series: minor-prophets
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2018/180923-PM-ACallToHoliness.mp3
audioBytes: 30086529
---
