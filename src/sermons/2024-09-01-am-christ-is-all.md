---
id: 2024-09-01-am
title: Christ is All
date: 2024-09-01
text: Colossians 3:17–4:1
preacher: josh-sheldon
series: the-sufficiency-of-christ
audio: https://storage.googleapis.com/pbc-ca-sermons/2024/240901-ChristisAll.aac
youtube: hB74ESAa1pQ
---
