---
id: 2020-01-12-am
title: An Enduring Partnership
date: 2020-01-12
text: Philippians 1:3–6
preacher: josh-sheldon
series: philippians
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2020/200112-AM-AnEnduringPartnership.mp3
audioBytes: 43732072
---
