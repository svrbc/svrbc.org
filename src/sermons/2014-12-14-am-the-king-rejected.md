---
id: 2014-12-14-am
title: The King Rejected
date: 2014-12-14
text: John 18:33–19:16
preacher: josh-sheldon
series: john
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2014/141214+The+King+Rejected.mp3
audioBytes: 40003833
---
