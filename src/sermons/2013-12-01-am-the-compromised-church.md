---
id: 2013-12-01-am
title: The Compromised Church
date: 2013-12-01
text: Revelation 2:12
preacher: jesse-lu
series: the-churches-of-revelation
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2013/2013-12-01+The+Compromised+Church.mp3
audioBytes: 60868710
---
