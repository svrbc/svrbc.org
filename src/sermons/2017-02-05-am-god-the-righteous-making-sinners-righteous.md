---
id: 2017-02-05-am
title: God the Righteous Making Sinners Righteous
date: 2017-02-05
text: Romans 3:20–26
preacher: josh-sheldon
series: romans
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2017/170205+God+the+Righteous+Making+Sinners+Righteous.mp3
audioBytes: 52058610
youtube: Xj3pOZMFnrI
---
