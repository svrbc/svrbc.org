---
id: 2011-08-14-am
title: The Heavenly Meaning
date: 2011-08-14
text: John 3:14–15
preacher: josh-sheldon
series: john
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2011/2011-08-14+The+Heavenly+Meaning.mp3
audioBytes: 32267904
---
