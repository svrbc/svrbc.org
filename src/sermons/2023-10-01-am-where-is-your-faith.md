---
id: 2023-10-01-am
title: Where Is Your Faith
date: 2023-10-01
text: Luke 8:16–25
preacher: brian-garcia
series: luke
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2023/231001-WhereIsYourFaith.aac
youtube: ESA6e77LHUg
---
