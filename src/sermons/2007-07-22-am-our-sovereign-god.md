---
id: 2007-07-22-am
title: Our Sovereign God
date: 2007-07-22
text: Acts 4:23–31
preacher: josh-sheldon
series: acts
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2007/2007-07-22+Our+Sovereign+God.mp3
audioBytes: 49894938
---
