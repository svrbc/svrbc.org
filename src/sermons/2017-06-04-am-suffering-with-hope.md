---
id: 2017-06-04-am
title: Suffering With Hope
date: 2017-06-04
text: Romans 8:16–25
preacher: josh-sheldon
series: romans
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2017/170604+Suffering+With+Hope.mp3
audioBytes: 59447327
youtube: 3WX5HYOwGLE
---
