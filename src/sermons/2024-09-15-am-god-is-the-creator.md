---
id: 2024-09-15-am
title: God is the Creator
date: 2024-09-15
text: Genesis 1:14
preacher: tim-mullet
series: basic-truths
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2024/240915-BasicTruthsGodistheCreator.aac
audioBytes: 50142205
youtube: gS0w1RB1ZWk
---
