---
id: 2017-07-09-am
title: Order In The Church
date: 2017-07-09
text: 1 Timothy 2:8–12
preacher: josh-sheldon
series: 1-timothy
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2017/170709+Order+In+The+Church.mp3
audioBytes: 50895468
youtube: 640z3EScLoM
---
