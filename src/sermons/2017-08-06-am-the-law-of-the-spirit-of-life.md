---
id: 2017-08-06-am
title: The Law of the Spirit of Life
date: 2017-08-06
text: Romans 8
preacher: josh-sheldon
series: romans
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2017/170806+The+Law+of+the+Spirit+of+Life.mp3
audioBytes: 51538702
youtube: yPvCEmMmAhE
---
