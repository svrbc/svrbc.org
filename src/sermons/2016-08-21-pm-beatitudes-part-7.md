---
id: 2016-08-21-pm
title: Beatitudes Part 7
date: 2016-08-21
text: Matthew 5
preacher: josh-sheldon
series: beatitudes
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2016/160821+Beatitudes+Part+7.mp3
audioBytes: 27385232
youtube: V9AWVU_CK4E
---
