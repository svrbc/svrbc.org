---
id: 2019-09-22-pm
title: Trusting Prayer
date: 2019-09-22
text: Psalm 3
preacher: josh-sheldon
series: the-psalms
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2019/190922-PM-TrustingPrayer.mp3
audioBytes: 35326885
---
