---
id: 2022-12-04-am
title: 'The Gospel of Luke: The Promise'
date: 2022-12-04
text: Luke 1:1–13
preacher: brian-garcia
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2022/221204-TheGospelOfLuke-ThePromise.aac
youtube: nsxxVTGr2JQ
---
