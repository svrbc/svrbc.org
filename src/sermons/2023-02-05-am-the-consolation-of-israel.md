---
id: 2023-02-05-am
title: The Consolation Of Israel
date: 2023-02-05
text: Luke 2:22–40
preacher: brian-garcia
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2023/230205-TheConsolationOfIsrael.aac
youtube: vRGzQ_eIIMk
---
