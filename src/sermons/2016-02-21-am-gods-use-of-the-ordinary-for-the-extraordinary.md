---
id: 2016-02-21-am
title: God’s Use of the Ordinary for the Extraordinary
date: 2016-02-21
text: 2 Kings 4:38–44
preacher: josh-sheldon
series: 12kings
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2016/160221+Gods+Use+of+the+Ordinary+for+the+Extraordinary.mp3
audioBytes: 48853286
youtube: Mx3EAFD21Ao
---
