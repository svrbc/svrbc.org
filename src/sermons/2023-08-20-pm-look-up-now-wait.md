---
id: 2023-08-20-pm
title: 'Look Up, Now Wait'
date: 2023-08-20
text: Psalm 123
preacher: josh-sheldon
unregisteredSeries: Psalms of Ascent
audio: https://storage.googleapis.com/pbc-ca-sermons/2023/230820-LookUpNowWait.aac
youtube: Ub7JY5Nwu7c
---
