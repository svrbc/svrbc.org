---
id: 2014-05-25-am
title: 'One Way, One Truth, One Life'
date: 2014-05-25
text: John 13:36–14:6
preacher: josh-sheldon
series: john
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2014/2014-05-25+One+Way%2C+One+Truth%2C+One+Life.mp3
audioBytes: 49161018
---
