---
id: 2017-09-10-am
title: The Goals of Gospel Ministry
date: 2017-09-10
text: Colossians 2:1–5
preacher: mike-kelley
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2017/170910+The+Goals+of+Gospel+Ministry.mp3
audioBytes: 53006604
youtube: Arrz5JKZ6e4
---
