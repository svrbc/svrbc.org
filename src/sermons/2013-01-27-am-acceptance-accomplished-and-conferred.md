---
id: 2013-01-27-am
title: Acceptance Accomplished and Conferred
date: 2013-01-27
text: Ephesians 1
preacher: josh-sheldon
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2013/2013-01-27+Acceptance+Accomplished+and+Conferred.mp3
audioBytes: 44283786
---
