---
id: 2011-08-28-am
title: Our Strength is in the Lord
date: 2011-08-28
text: 2 Timothy 1:1–7
preacher: henry-wiley
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2011/2011-08-28+Our+Strength+is+in+the+Lord.mp3
audioBytes: 26452608
---
