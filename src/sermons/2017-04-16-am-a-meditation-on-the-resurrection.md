---
id: 2017-04-16-am
title: A Meditation On The Resurrection
date: 2017-04-16
text: 1 Corinthians 15:1–19; Leviticus 9:15–24
preacher: josh-sheldon
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2017/170416+A+Meditation+on+the+Resurrection.mp3
audioBytes: 41436683
youtube: PYRQfoRaT_A
---
