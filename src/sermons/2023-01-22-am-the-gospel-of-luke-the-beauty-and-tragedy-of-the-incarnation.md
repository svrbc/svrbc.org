---
id: 2023-01-22-am
title: 'The Gospel of Luke: The Beauty and Tragedy of the Incarnation'
date: 2023-01-22
text: Luke 2:1–7
preacher: brian-garcia
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2023/230122-TheGospelOfLuke-TheBeautyAndTragedyOfTheIncarnation.aac
youtube: 2I9RaAVrEtU
---
