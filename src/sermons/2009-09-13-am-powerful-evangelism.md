---
id: 2009-09-13-am
title: Powerful Evangelism
date: 2009-09-13
text: Acts 19:23
preacher: josh-sheldon
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2009/2009-09-13+Powerful+Evangelism.mp3
audioBytes: 50835690
---
