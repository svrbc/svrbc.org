---
id: 2024-12-08-pm
title: Symbols Of Salvation
date: 2024-12-08
text: Isaiah 38:7–8, 21–22
preacher: conley-owens
series: isaiah
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2024/241208-SymbolsOfSalvation.aac
audioBytes: 47082779
youtube: 9dWqtfRtFU4
---
