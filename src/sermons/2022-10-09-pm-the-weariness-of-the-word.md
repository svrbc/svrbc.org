---
id: 2022-10-09-pm
title: The Weariness of the Word
date: 2022-10-09
text: Isaiah 28:7–17
preacher: conley-owens
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2022/221009-TheWearinesOfTheWord.aac
audioBytes: 31132379
youtube: EcPw5PV1RbM
---
