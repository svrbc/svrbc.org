---
id: 2012-05-20-am
title: God Helps Not Tempts
date: 2012-05-20
text: James 1:12–18
preacher: henry-wiley
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2012/2012-05-20+God+Helps+Not+Tempts.mp3
audioBytes: 26938671
---
