---
id: 2007-06-10-am
title: The Fear of God
date: 2007-06-10
text: Acts 3:1–16
preacher: josh-sheldon
series: acts
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2007/2007-06-10+The+Fear+of+God.mp3
audioBytes: 42671247
---
