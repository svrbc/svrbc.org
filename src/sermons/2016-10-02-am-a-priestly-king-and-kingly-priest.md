---
id: 2016-10-02-am
title: A Priestly King and Kingly Priest
date: 2016-10-02
text: Psalm 110
preacher: josh-sheldon
series: the-psalms
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2016/160925+The+Voice+of+Order.mp3
audioBytes: 47212767
youtube: ila01RdSAKE
---
