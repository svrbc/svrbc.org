---
id: 2014-07-13-am
title: Without Jesus We Can Do Nothing
date: 2014-07-13
text: John 15:1–8
preacher: josh-sheldon
series: john
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2014/2014-07-13+Without+Jesus+We+Can+Do+Nothing.mp3
audioBytes: 55923090
---
