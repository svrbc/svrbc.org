---
id: 2024-11-17-pm
title: The Sin of Empathy
date: 2024-11-17
text: Hebrews 4:14–16
preacher: tim-mullet
audio: https://storage.googleapis.com/pbc-ca-sermons/2024/241117-TheSinOfEmpathy.aac
audioBytes: 56961195
youtube: o1oQTOV6A2w
---
