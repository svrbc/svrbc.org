---
id: 2015-12-20-am
title: The Meaning of the Advent – A Meditation on the Incarnation
date: 2015-12-20
text: Isaiah 9; Luke 2
preacher: josh-sheldon
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2015/151220+The+Meaning+of+the+Advent.mp3
audioBytes: 38147627
youtube: X4R87Cihzug
---
