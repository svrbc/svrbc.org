---
id: 2019-12-29-am
title: Memorial Stones
date: 2019-12-29
text: Joshua 4:1–7
preacher: josh-sheldon
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2019/191229-AM-MemorialStones.mp3
audioBytes: 41374764
---
