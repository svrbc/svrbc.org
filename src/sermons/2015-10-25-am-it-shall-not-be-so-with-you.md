---
id: 2015-10-25-am
title: It Shall Not Be So With You
date: 2015-10-25
text: Mark 10:32–45
preacher: henry-wiley
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2015/151025+It+Shall+Not+Be+so+With+You.mp3
audioBytes: 34003175
youtube: _cpx_ZA4gKs
---
