---
id: 2020-02-09-am
title: A God You Can Trust
date: 2020-02-09
text: Philippians 1:18–26
preacher: josh-sheldon
series: philippians
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2020/200209-AM-AGodYouCanTrust.mp3
audioBytes: 50582005
youtube: Fau22TOt8aA
---
