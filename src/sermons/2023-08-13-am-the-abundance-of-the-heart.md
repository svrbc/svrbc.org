---
id: 2023-08-13-am
title: The Abundance Of The Heart
date: 2023-08-13
text: Luke 6:43–49
preacher: brian-garcia
series: luke
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2023/230813-TheAbundanceOfTheHeart.aac
youtube: sQh_Rrpls2c
---
