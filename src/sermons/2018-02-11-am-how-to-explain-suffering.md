---
id: 2018-02-11-am
title: How To Explain Suffering
date: 2018-02-11
text: Job 3
preacher: eric-hollingsworth
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2018/180211-AM-HowToExplainSuffering.mp3
audioBytes: 47015117
youtube: jon15W_bDc4
---
