---
id: 2019-06-16-am
title: To Be Willing
date: 2019-06-16
text: Nehemiah 11
preacher: josh-sheldon
series: ezra-nehemiah
audio: https://storage.googleapis.com/pbc-ca-sermons/2019/190616-AM-ToBeWilling.mp3
audioBytes: 50415217
---
