---
id: 2019-06-09-am
title: A Model Of Repentance
date: 2019-06-09
text: Nehemiah 9:38–10:39
preacher: josh-sheldon
series: ezra-nehemiah
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2019/190609-AM-AModelOfRepentance.mp3
audioBytes: 44481475
---
