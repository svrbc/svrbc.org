---
id: 2019-06-02-pm
title: The Hardening Of Hearts
date: 2019-06-02
text: Isaiah 6:8–13
preacher: conley-owens
series: isaiah
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2019/190602-PM-TheHardeningOfHearts.mp3
audioBytes: 37304272
---
