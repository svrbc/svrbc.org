---
id: 2023-04-09-am
title: The Lord Delivers
date: 2023-04-09
text: Psalm 34:19–22
preacher: josh-sheldon
series: the-psalms
audio: https://storage.googleapis.com/pbc-ca-sermons/2023/230409-TheLordDelivers.aac
youtube: kLmYtGAJ1Tk
---
