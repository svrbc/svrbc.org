---
id: 2019-10-13-am
title: 'Sing Praises To God, For He Is The King Of All The Earth'
date: 2019-10-13
text: Psalm 47
preacher: jesus-mancilla
series: kingship-psalms
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2019/191013-AM-SingPraisesToGodForHeIsTheKingOfAllTheEarth.mp3
audioBytes: 52122627
---
