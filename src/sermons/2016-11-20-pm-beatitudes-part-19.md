---
id: 2016-11-20-pm
title: Beatitudes Part 19
date: 2016-11-20
text: Matthew 6:2–18
preacher: josh-sheldon
series: beatitudes
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2016/161120+Afternoon+Service+-+Beatitudes+Part+19.mp3
audioBytes: 21558041
---
