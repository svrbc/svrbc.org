---
id: 2007-11-11-am
title: Who Is Being Accused
date: 2007-11-11
text: Acts 6:8–15
preacher: josh-sheldon
series: acts
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2007/2007-11-11+Who+Is+Being+Accused.mp3
audioBytes: 49318644
---
