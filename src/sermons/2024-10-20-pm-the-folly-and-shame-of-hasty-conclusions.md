---
id: 2024-10-20-pm
title: The Folly and Shame of Hasty Conclusions
date: 2024-10-20
text: Proverbs 18:13
preacher: tim-mullet
unregisteredSeries: Proverbs
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2024/241020-Proverbs-TheFollyandShameofHastyConclusions.aac
audioBytes: 66548515
youtube: WEVlqEI7dD4
---
