---
id: 2014-03-09-am
title: 'A Theology of Unbelief, Part 1'
date: 2014-03-09
text: John 12:37–43
preacher: josh-sheldon
series: john
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2014/2014-03-09+A+Theology+of+Unbelief.mp3
audioBytes: 45958875
---
