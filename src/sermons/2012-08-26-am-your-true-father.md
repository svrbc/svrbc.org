---
id: 2012-08-26-am
title: Your True Father
date: 2012-08-26
text: John 8:31–59
preacher: josh-sheldon
series: john
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2012/2012-08-26+Your+True+Father.mp3
audioBytes: 57789582
---
