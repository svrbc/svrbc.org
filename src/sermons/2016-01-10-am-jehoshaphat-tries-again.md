---
id: 2016-01-10-am
title: Jehoshaphat Tries Again
date: 2016-01-10
text: 2 Kings 3
preacher: josh-sheldon
series: 12kings
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2016/160110+Jehoshaphat+Tries+Again.mp3
audioBytes: 57157275
youtube: 6CbmBUxPe8k
---
