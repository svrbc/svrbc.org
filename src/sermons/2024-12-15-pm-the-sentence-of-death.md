---
id: 2024-12-15-pm
title: The Sentence Of Death
date: 2024-12-15
text: Isaiah 38:9–16
preacher: conley-owens
series: isaiah
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2024/241215-TheSentenceOfDeath.aac
audioBytes: 34613344
youtube: y0lxG0sI7qo
---
