---
id: 2018-04-29-pm
title: The Prophet Hears and Answers
date: 2018-04-29
text: Habakkuk 1:12–17
preacher: josh-sheldon
series: minor-prophets
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2018/180429-PM-TheProphetHearsAndAnswers.mp3
audioBytes: 31958166
youtube: PDosMlVHfL4
---
