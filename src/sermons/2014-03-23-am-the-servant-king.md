---
id: 2014-03-23-am
title: The Servant King
date: 2014-03-23
text: John 13:1–5
preacher: josh-sheldon
series: john
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2014/2014-03-23+The+Servant-King.mp3
audioBytes: 51761847
---
