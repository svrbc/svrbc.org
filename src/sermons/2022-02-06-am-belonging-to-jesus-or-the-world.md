---
id: 2022-02-06-am
title: Belonging to Jesus (or the World)
date: 2022-02-06
text: 1 Corinthians 3:18–23
preacher: josh-sheldon
series: maturity-in-christ
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2022/220206-BelongingToJesusOrTheWorld.aac
audioBytes: 60174311
youtube: xrS_spaTLkg
---
