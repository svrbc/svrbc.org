---
id: 2024-10-27-pm
title: The Sin of Entitlement
date: 2024-10-27
text: Matthew 20:1–17
preacher: tim-mullet
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2024/241027-Matthew-TheSinofEntitlement.aac
audioBytes: 49741312
youtube: zGAqUYAn2_c
---
