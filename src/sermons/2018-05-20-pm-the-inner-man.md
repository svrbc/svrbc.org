---
id: 2018-05-20-pm
title: The Inner Man
date: 2018-05-20
text: 2 Corinthians 5:11–21
preacher: conley-owens
audio: https://storage.googleapis.com/pbc-ca-sermons/2018/180520-PM-TheInnerMan.mp3
audioBytes: 22661062
youtube: 6JBVgaTAxsI
---
