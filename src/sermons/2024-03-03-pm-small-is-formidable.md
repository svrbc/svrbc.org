---
id: 2024-03-03-pm
title: Small is Formidable
date: 2024-03-03
text: Luke 13:18–21
preacher: brian-garcia
series: luke
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2024/240303-SmallisFormidable.aac
youtube: mFLH1K3yiTM
---
