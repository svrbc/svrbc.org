---
id: 2013-07-21-am
title: Who is God and What Does He Require of Us
date: 2013-07-21
text: Isaiah 57:15
preacher: josh-sheldon
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2013/2013-07-21+Who+is+God+and+What+Does+He+Require+of+Us.mp3
audioBytes: 22372938
---
