---
id: 2021-11-21-am
title: Your Prayers Matter
date: 2021-11-21
text: 2 Thessalonians 3:1–3
preacher: josh-sheldon
series: awaiting-christ
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2021/211121-YourPrayersMatter.aac
audioBytes: 41469057
youtube: WLfWRrn60xg
---
