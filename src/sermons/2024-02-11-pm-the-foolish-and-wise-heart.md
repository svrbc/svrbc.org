---
id: 2024-02-11-pm
title: The Foolish and Wise Heart
date: 2024-02-11
text: Luke 12:13–31
preacher: brian-garcia
series: luke
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2024/240211-TheFoolishandWiseHeart.aac
youtube: P0v_kEK4gCk
---
