---
id: 2024-09-08-pm
title: Discouragements For Subordinates
date: 2024-09-08
text: Isaiah 36:11–20
preacher: conley-owens
series: isaiah
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2024/240908-DiscouragementsForSubordinates.aac
audioBytes: 40134284
youtube: 7d-Wf9nDu3E
---
