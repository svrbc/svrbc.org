---
id: 2012-07-01-am
title: A Right View of God
date: 2012-07-01
text: Malachi
preacher: josh-sheldon
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2012/2012-07-01+A+Right+View+of+God.mp3
audioBytes: 28641282
---
