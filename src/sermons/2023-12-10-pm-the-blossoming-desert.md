---
id: 2023-12-10-pm
title: The Blossoming Desert
date: 2023-12-10
text: Isaiah 35:1–2
preacher: conley-owens
series: isaiah
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2023/231210-TheBlossomingDesert.aac
youtube: DJ0hbFeKhqE
---
