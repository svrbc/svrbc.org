---
id: 2007-10-28-am
title: Growth Brings a Problem
date: 2007-10-28
text: Acts 6:1–7
preacher: josh-sheldon
series: acts
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2007/2007-10-28+Growth+Brings+a+Problem.mp3
audioBytes: 41670447
---
