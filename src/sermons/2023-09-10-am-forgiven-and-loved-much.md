---
id: 2023-09-10-am
title: Forgiven and Loved Much
date: 2023-09-10
text: Luke 7:36–50
preacher: brian-garcia
series: luke
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2021/230910-ForgivenAndLovedMuch.aac
youtube: DUyYMBRb5PY
---
