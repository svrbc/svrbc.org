---
id: 2024-01-28-pm
title: Eyes For Justice and the Love of the God
date: 2024-01-28
text: Luke 11:33–44
preacher: brian-garcia
series: luke
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2024/230121-KeepingTheWordOfGodInAWickedGeneration.aac
youtube: vJzzStVOick
---
