---
id: 2019-12-15-pm
title: The Blight Of Hatred
date: 2019-12-15
text: Psalm 9:13–20
preacher: josh-sheldon
series: the-psalms
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2019/191215-PM-TheBlightOfHatred.mp3
audioBytes: 33208685
---
