---
id: 2009-08-09-am
title: In Time of Need
date: 2009-08-09
text: Acts 18:10
preacher: josh-sheldon
series: acts
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2009/2009-08-09+In+Time+of+Need.mp3
audioBytes: 52323546
---
