---
id: 2020-10-25-am
title: The Folly of Human Wisdom
date: 2020-10-25
text: 1 Corinthians 1:18–21
preacher: josh-sheldon
series: kingdom-community
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2020/201026-TheFollyOfHumanWisdom.mp3
audioBytes: 62541575
youtube: Uk0yDl5sWIs
---
