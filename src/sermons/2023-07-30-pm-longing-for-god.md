---
id: 2023-07-30-pm
title: Longing For God
date: 2023-07-30
text: Psalm 84
preacher: josh-sheldon
series: the-psalms
audio: https://storage.googleapis.com/pbc-ca-sermons/2023/230730-LongingForGod.aac
youtube: yWRDA3j9yBE
---
