---
id: 2017-09-03-am
title: Calling All Vessels
date: 2017-09-03
text: Romans 9:19–29
preacher: josh-sheldon
series: romans
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2017/170903+Calling+All+Vessels.mp3
audioBytes: 44310459
youtube: oVnIuH52c8Y
---
