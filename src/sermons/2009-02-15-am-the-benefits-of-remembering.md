---
id: 2009-02-15-am
title: The Benefits of Remembering
date: 2009-02-15
text: 2 Chronicles 30
preacher: josh-sheldon
series: kings-of-judah
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2009/2009-02-15+The+Benefits+of+Remembering.mp3
audioBytes: 48776127
---
