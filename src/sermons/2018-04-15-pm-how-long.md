---
id: 2018-04-15-pm
title: How Long?
date: 2018-04-15
text: Habakkuk 1:1–4
preacher: josh-sheldon
series: minor-prophets
audio: https://storage.googleapis.com/pbc-ca-sermons/2018/180415-PM-HowLong.mp3
audioBytes: 28065270
youtube: yvmgRbfFQM8
---
