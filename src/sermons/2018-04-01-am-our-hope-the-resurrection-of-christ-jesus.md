---
id: 2018-04-01-am
title: 'Our Hope: The Resurrection Of Christ Jesus'
date: 2018-04-01
text: 2 Timothy 2:8
preacher: josh-sheldon
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2018/180401-AM-OurHope-TheResurrectionOfChristJesus.mp3
audioBytes: 48478003
youtube: ECm1zJUy6wg
---
