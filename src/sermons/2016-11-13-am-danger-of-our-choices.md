---
id: 2016-11-13-am
title: Danger of Our Choices
date: 2016-11-13
text: Romans 1:24–32
preacher: josh-sheldon
series: romans
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2016/161113+Danger+of+Our+Choices.mp3
audioBytes: 49198548
youtube: DnnI5mYL4dg
---
