---
id: 2019-01-27-am
title: A Taste of Freedom
date: 2019-01-27
text: Nehemiah 2:6
preacher: josh-sheldon
series: ezra-nehemiah
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2019/190127-AM-ATasteOfFreedom.mp3
audioBytes: 48552380
youtube: qR-zsXJZq_M
---
