---
id: 2017-08-20-pm
title: Their Dignity And Their Support
date: 2017-08-20
text: 1 Timothy 5:17–25
preacher: josh-sheldon
series: 1-timothy
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2017/170820+PM+Service+-+Their+Dignity+And+Their+Support.mp3
audioBytes: 31261862
youtube: kOm49PJJQd0
---
