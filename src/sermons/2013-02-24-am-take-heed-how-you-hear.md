---
id: 2013-02-24-am
title: Take Heed How You Hear
date: 2013-02-24
text: Luke 8:11–15
preacher: josh-sheldon
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2013/2013-02-24+Take+Heed+How+You+Hear.mp3
audioBytes: 53494899
---
