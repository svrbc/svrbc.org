---
id: 2022-07-03-pm
title: Waiting Patiently and Humbly
date: 2022-07-03
text: Isaiah 25:9–12
preacher: conley-owens
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2022/220703-WaitingPatientlyandHumbly.aac
audioBytes: 24353540
youtube: a9XYCoo7gZI
---
