---
id: 2016-10-09-pm
title: Beatitudes Part 13
date: 2016-10-09
text: Matthew 6:9–13
preacher: josh-sheldon
series: beatitudes
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2016/161009+Beatitudes+Part+13.mp3
audioBytes: 22205015
youtube: UfrQ-y4Sx30
---
