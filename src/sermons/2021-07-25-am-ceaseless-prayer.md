---
id: 2021-07-25-am
title: Ceaseless Prayer
date: 2021-07-25
text: 1 Thessalonians 5:16–18
preacher: conley-owens
series: awaiting-christ
audio: https://storage.googleapis.com/pbc-ca-sermons/2021/210725-CeaselessPrayer.aac
audioBytes: 48612941
youtube: rwQ6CVIZylE
---
