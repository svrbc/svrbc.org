---
id: 2016-11-06-pm
title: Beatitudes Part 17
date: 2016-11-06
text: Matthew 6:9–15
preacher: josh-sheldon
series: beatitudes
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2016/161106+Afternoon+Service+-+Beatitudes+Part+17.mp3
audioBytes: 30543739
youtube: xp_fDjA-mHA
---
