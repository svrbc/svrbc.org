---
id: 2018-11-18-am
title: Ezra’s Prayer Part 1
date: 2018-11-18
text: Ezra 9:6–9
preacher: josh-sheldon
series: ezra-nehemiah
audio: https://storage.googleapis.com/pbc-ca-sermons/2018/181118-AM-EzrasPrayer.mp3
audioBytes: 43240946
---
