---
id: 2022-07-17-pm
title: Learning Righteousness
date: 2022-07-17
text: Isaiah 26:7–11
preacher: conley-owens
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2022/220717-LearningRighteousness.aac
youtube: EckU0G4CBlA
---
