---
id: 2024-06-02-pm
title: The Gospel Of Christ - Personally Yours
date: 2024-06-02
text: Galatians 2:19–20
preacher: josh-sheldon
unregisteredSeries: The Gospel Of Christ
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2024/240602-TheGospelofChristPersonallyYours.aac
youtube: LYaCcZwXWT8
---
