---
id: 2014-11-30-am
title: Repentance or Just Regret?
date: 2014-11-30
text: John 18:15–18, 25–27
preacher: josh-sheldon
series: john
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2014/141130+Repentance+or+Just+Regret.mp3
audioBytes: 42044332
---
