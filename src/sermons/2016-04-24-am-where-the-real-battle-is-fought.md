---
id: 2016-04-24-am
title: Where the Real Battle is Fought
date: 2016-04-24
text: 2 Kings 6:8–23
preacher: josh-sheldon
series: 12kings
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2016/160424+Where+the+Real+Battle+is+Fought.mp3
audioBytes: 55324950
youtube: '-waqaAK5LfA'
---
