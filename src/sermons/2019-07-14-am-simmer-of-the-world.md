---
id: 2019-07-14-am
title: Simmer Of The World
date: 2019-07-14
text: Exodus 23:17–21
preacher: ken-tompkins
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2019/190714-AM-SimmerOfTheWorld.mp3
audioBytes: 44706328
---
