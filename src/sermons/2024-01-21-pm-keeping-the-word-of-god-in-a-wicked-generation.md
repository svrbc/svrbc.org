---
id: 2024-01-21-pm
title: Keeping The Word Of God In A Wicked Generation
date: 2024-01-21
text: Luke 11:27–32
preacher: brian-garcia
series: luke
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2024/230121-KeepingTheWordOfGodInAWickedGeneration.aac
youtube: F-FR8-8WcbA
---
