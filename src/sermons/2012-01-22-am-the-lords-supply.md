---
id: 2012-01-22-am
title: The Lord’s Supply
date: 2012-01-22
text: John 6:1–15
preacher: josh-sheldon
series: john
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2012/2012-01-22+The+Lords+Supply.mp3
audioBytes: 43956858
---
