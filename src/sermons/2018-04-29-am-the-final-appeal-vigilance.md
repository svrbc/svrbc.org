---
id: 2018-04-29-am
title: The Final Appeal – Vigilance
date: 2018-04-29
text: Romans 16:17–20
preacher: josh-sheldon
series: romans
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2018/180429-AM-TheFinalAppeal-Vigilance.mp3
audioBytes: 56919099
youtube: WmVqBcvxqy4
---
