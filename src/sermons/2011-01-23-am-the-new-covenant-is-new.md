---
id: 2011-01-23-am
title: The New Covenant is New
date: 2011-01-23
text: Romans 8:1–4
preacher: josh-sheldon
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2011/2011-01-23+The+New+Covenant+is+New.mp3
audioBytes: 57884928
---
