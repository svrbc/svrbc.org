---
id: 2023-08-06-pm
title: Women of Ease
date: 2023-08-06
text: Isaiah 32:9–13
preacher: conley-owens
series: isaiah
audio: https://storage.googleapis.com/pbc-ca-sermons/2023/230806-WomenofEase.aac
youtube: YSnz7OPDgC0
---
