---
id: 2016-09-11-am
title: Our Hope In A Sovereign God
date: 2016-09-11
text: Revelation 4–5
preacher: josh-sheldon
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2016/160911+Our+Hope+in+a+Sovereign+God.mp3
audioBytes: 53735447
youtube: H8EySqrgYfc
---
