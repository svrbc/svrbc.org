---
id: 2025-02-16-am
title: Adam Doomed the World by Listening to his Wife
date: 2025-02-16
text: Genesis 3:17
preacher: tim-mullet
series: basic-truths
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2025/250216-BasicTruths-AdamDoomedtheWorldbyListeningtohisWife.aac
audioBytes: 77044660
youtube: 39RaJ6TrO5g
---
