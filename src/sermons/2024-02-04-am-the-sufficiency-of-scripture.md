---
id: 2024-02-04-am
title: The Sufficiency of Scripture
date: 2024-02-04
text: 2 Peter 1:3–4
unregisteredPreacher: Tim Mullet
unregisteredSeries: 2 Peter
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2024/240204-TheSufficiencyofScripture.aac
youtube: Ytj37bHoSuk
---
