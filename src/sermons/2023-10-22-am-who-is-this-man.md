---
id: 2023-10-22-am
title: Who is this Man?
date: 2023-10-22
text: Luke 9:7–20
preacher: brian-garcia
series: luke
audio: https://storage.googleapis.com/pbc-ca-sermons/2023/231022-WhoIsThisMan.aac
youtube: sN-7qqFP6wQ
---
