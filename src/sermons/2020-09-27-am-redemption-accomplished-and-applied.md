---
id: 2020-09-27-am
title: 'Redemption, Accomplished & Applied'
date: 2020-09-27
text: Ruth 4
preacher: josh-sheldon
series: sovereignty-and-covid19
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2020/200927-RedemptionAcoomplished%26Applied.mp3
youtube: 9LuHCUc20Rs
---
