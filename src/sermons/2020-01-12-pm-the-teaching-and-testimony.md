---
id: 2020-01-12-pm
title: The Teaching And Testimony
date: 2020-01-12
text: Isaiah 8:11–15
preacher: conley-owens
series: isaiah
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2020/200112-PM-TheTeachingAndTestimony.mp3
audioBytes: 32562005
---
