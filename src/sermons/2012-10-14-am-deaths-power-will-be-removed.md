---
id: 2012-10-14-am
title: Death’s Power Will Be Removed
date: 2012-10-14
text: Isaiah 25
preacher: mike-kelley
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2012/2012-10-14+Deaths+Power+Will+Be+Removed.mp3
audioBytes: 50893236
---
