---
id: 2015-08-23-am
title: Elijah Begins
date: 2015-08-23
text: 1 Kings 17:1–16
preacher: josh-sheldon
series: elijah-elisha
audio: https://storage.googleapis.com/pbc-ca-sermons/2015/150823+Elijah+Begins.mp3
audioBytes: 49403734
youtube: k221tJlXD1E
---
