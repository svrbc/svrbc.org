---
id: 2007-01-07-am
title: Whose Acts
date: 2007-01-07
text: Acts 1:1–3
preacher: josh-sheldon
unregisteredPreacher: dummy
series: acts
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2007/2007-01-07+Whose+Acts-Acts+1-1-3.mp3
audioBytes: 42172098
---
