---
id: 2012-07-15-am
title: He Defends the Guilty
date: 2012-07-15
text: John 8:11
preacher: josh-sheldon
series: john
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2012/2012-07-15+He+Defends+the+Guilty.mp3
audioBytes: 55790484
---
