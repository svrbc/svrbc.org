---
id: 2019-12-01-am
title: Prepare To Meet Your God
date: 2019-12-01
text: Amos 4:12
preacher: josh-sheldon
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2019/191201-AM-PrepareToMeetYourGod.mp3
audioBytes: 45416432
---
