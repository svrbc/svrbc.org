---
id: 2017-12-17-am
title: Eternity Invades the Temporal
date: 2017-12-17
text: John 1:1–18
preacher: josh-sheldon
series: advent-2017
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2017/171217+Eternity+Invades+the+Temporal.mp3
audioBytes: 50814802
youtube: qArQmXZmnJI
---
