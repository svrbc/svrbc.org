---
id: 2017-12-10-pm
title: Understood At Last
date: 2017-12-10
text: John 16:25–33
preacher: josh-sheldon
series: the-road-to-the-cross
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2017/171210+PM+Service+-+Understood+At+Last.mp3
audioBytes: 29707450
youtube: yoMe2adCDLw
---
