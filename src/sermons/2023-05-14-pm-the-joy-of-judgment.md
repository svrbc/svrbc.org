---
id: 2023-05-14-pm
title: The Joy of Judgment
date: 2023-05-14
text: Isaiah 30:33
preacher: conley-owens
series: isaiah
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2023/230514-TheJoyOfJudgement.aac
youtube: G0IaNHijaWU
---
