---
id: 2024-04-28-pm
title: The Way Of Holiness
date: 2024-04-28
text: Isaiah 35:8–10
preacher: conley-owens
series: isaiah
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2024/240428-TheWayOfHoliness.aac
youtube: az96lQ3Rhm4
---
