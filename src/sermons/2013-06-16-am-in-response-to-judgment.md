---
id: 2013-06-16-am
title: In Response to Judgment
date: 2013-06-16
text: Lamentations 3:22–33
preacher: josh-sheldon
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2013/2013-06-16+In+Response+to+Judgement.mp3
audioBytes: 50820678
---
