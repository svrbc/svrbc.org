---
id: 2022-03-20-pm
title: 'The Oracle Against Tyre: The Purpose of the Lord'
date: 2022-03-20
text: Isaiah 23:8–14
preacher: conley-owens
series: isaiah
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2022/220320-TheOracleAgainstTyre-ThePurposeOfTheLord.aac
audioBytes: 39920936
youtube: fe4JorIMdCM
---
