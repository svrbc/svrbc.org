---
id: 2024-03-17-pm
title: Beware The Fox (Jerusalem’s Judgment)
date: 2024-03-17
text: Luke 13:31–35
preacher: brian-garcia
series: luke
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2024/240317-BewareTheFox-JerusalemsJudgment.aac
youtube: a8GmTvBmwKQ
---
