---
id: 2024-06-23-am
title: Faith To Obey
date: 2024-06-23
text: Luke 17:5–10
preacher: conley-owens
series: luke
audio: https://storage.googleapis.com/pbc-ca-sermons/2024/240623-FaithToObey.aac
youtube: AESbu0DVxFg
---
