---
id: 2024-11-24-am
title: Sex Is Not Optional
date: 2024-11-24
text: Genesis 2:24–25
preacher: tim-mullet
series: basic-truths
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2024/241124-BasicTruths-SexIsNotOptional.aac
audioBytes: 86126520
youtube: HREdT_O7jnI
---
