---
id: 2023-11-05-am
title: The Transfiguration
date: 2023-11-05
text: Luke 9:28–36
preacher: brian-garcia
series: luke
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2023/231105-TheTransfiguration.aac
youtube: 6mSeLT9lkmE
---
