---
id: 2023-08-20-am
title: Have Such Faith
date: 2023-08-20
text: Luke 7:17
preacher: brian-garcia
series: luke
audio: https://storage.googleapis.com/pbc-ca-sermons/2023/230820-HaveSuchFaith.aac
youtube: Ys1rk3Cmp3I
---
