---
id: 2019-02-10-am
title: What’s The Big Deal? (It’s Just A Wall)
date: 2019-02-10
text: Nehemiah 3
preacher: josh-sheldon
series: ezra-nehemiah
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2019/190210-AM-WhatsTheBigDealItsJustAWall.mp3
audioBytes: 50909270
youtube: fEnBI5FkZUQ
---
