---
id: 2023-06-25-am
title: Trust In God
date: 2023-06-25
text: Isaiah 55
preacher: josh-sheldon
audio: https://storage.googleapis.com/pbc-ca-sermons/2023/230625-TrustInGod.aac
youtube: _Kr3bVVlrZk
---
