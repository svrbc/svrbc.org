---
id: 2018-06-17-pm
title: The Coming Flood
date: 2018-06-17
text: Zephaniah 1:1–6
preacher: josh-sheldon
series: minor-prophets
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2018/180617-PM-TheComingFlood.mp3
audioBytes: 28106657
youtube: Z_Lrd8__j8A
---
