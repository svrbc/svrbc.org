---
id: 2018-10-21-pm
title: God’s Transforming Power
date: 2018-10-21
text: Haggai 2:20–23
preacher: josh-sheldon
series: minor-prophets
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2018/181021-PM-GodsTransformingPower.mp3
audioBytes: 34612615
---
