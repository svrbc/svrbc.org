---
id: 2016-10-09-am
title: To All Those Loved By God
date: 2016-10-09
text: Romans 1:1–7
preacher: josh-sheldon
series: romans
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2016/161009+To+All+Those+Loved+By+God.mp3
audioBytes: 50379233
youtube: xfZPtp3ne3I
---
