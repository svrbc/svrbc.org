---
id: 2024-01-21-am
title: The Glory of the Builder
date: 2024-01-21
text: Hebrews 3:3–4
preacher: conley-owens
series: hebrews
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2024/240121-TheGloryoftheBuilder.aac
youtube: 3VQOlmHbCOE
---
