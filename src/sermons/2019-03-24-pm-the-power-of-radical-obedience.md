---
id: 2019-03-24-pm
title: The Power Of Radical Obedience
date: 2019-03-24
text: Exodus 6:1–13
preacher: joshua-camacho
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2019/190324-PM-ThePowerOfRadicalObedience.mp3
audioBytes: 40759996
---
