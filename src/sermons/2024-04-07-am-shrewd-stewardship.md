---
id: 2024-04-07-am
title: Shrewd Stewardship
date: 2024-04-07
text: Luke 16:1–9
preacher: conley-owens
series: luke
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2024/240407-ShrewdStewardship.aac
youtube: LVK9SfMnEZI
---
