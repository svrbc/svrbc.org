---
id: 2022-08-21-am
title: Submission to Christ in Marriage
date: 2022-08-21
text: Ephesians 5:22–27
preacher: brian-garcia
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2022/220821-SubmissionToChristInMarriage.aac
audioBytes: 58390484
youtube: xjgie2FZT6c
---
