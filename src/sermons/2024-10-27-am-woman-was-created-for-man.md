---
id: 2024-10-27-am
title: Woman Was Created For Man
date: 2024-10-27
text: Genesis 2:18–25
preacher: tim-mullet
series: basic-truths
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2024/241027-BasicTruths-WomanWasCreatedForMan.aac
audioBytes: 76187423
youtube: FTjpq3CvD9E
---
