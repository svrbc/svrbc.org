---
id: 2025-01-05-am
title: God Gave Man Dominion
date: 2025-01-05
text: Genesis 1:26–28
preacher: conley-owens
series: basic-truths
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2025/250105-BasicTruths-GodGaveManDominion.aac
audioBytes: 80835424
youtube: azqVgFA8Ojw
---
