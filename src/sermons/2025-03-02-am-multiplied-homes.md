---
id: 2025-03-02-am
title: Multiplied Homes
date: 2025-03-02
text: Luke 18:28–30
preacher: conley-owens
series: luke
audio: https://storage.googleapis.com/pbc-ca-sermons/2025/250302-MultipliedHomes.aac
audioBytes: 57427142
youtube: W8ZSY-FzrBE
---
