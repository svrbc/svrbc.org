---
id: 2016-10-16-am
title: To Serve God
date: 2016-10-16
text: Romans 1:8–15
preacher: josh-sheldon
series: romans
audio: https://storage.googleapis.com/pbc-ca-sermons/2016/161016+To+Serve+God.mp3
audioBytes: 51205999
youtube: Kb8YYCEmQ8E
---
