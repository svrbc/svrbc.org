---
id: 2025-02-16-pm
title: Dust in the Scales
date: 2025-02-16
text: Isaiah 40:12–15
preacher: conley-owens
series: isaiah
audio: https://storage.googleapis.com/pbc-ca-sermons/2025/250216-DustintheScales.aac
audioBytes: 42475602
youtube: tfMqC4-WWC8
---
