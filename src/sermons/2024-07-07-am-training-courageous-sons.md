---
id: 2024-07-07-am
title: Training Courageous Sons
date: 2024-07-07
text: Proverbs 22:6; John 19:11–18
unregisteredPreacher: Kyle Fitzgerald
unregisteredSeries: Courageous Sons
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2024/240707-TrainingCourageousSons.aac
youtube: aOIR6C9c_Yc
---
