---
id: 2024-08-25-pm
title: 'Heavenly Thinking on Earth, Part 2'
date: 2024-08-25
text: Colossians 3:5–17
preacher: josh-sheldon
series: the-sufficiency-of-christ
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2024/240825-HeavenlyThinkingonEarthPart2.aac
youtube: LufsrLSWw7s
---
