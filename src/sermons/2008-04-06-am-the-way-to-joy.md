---
id: 2008-04-06-am
title: The Way To Joy
date: 2008-04-06
text: John 1:5–10
preacher: josh-sheldon
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2008/2008-04-06+The+Way+To+Joy.mp3
audioBytes: 48275727
---
