---
id: 2009-06-21-am
title: Enjoy the Wedding
date: 2009-06-21
text: Mark 2:18–22
preacher: michael-phillips
audio: https://storage.googleapis.com/pbc-ca-sermons/2009/2009-06-21+Sermon.mp3
audioBytes: 28039551
---
