---
id: 2024-11-17-am
title: The Faith of Children
date: 2024-11-17
text: Luke 18:5–17
preacher: conley-owens
series: luke
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2024/241117-TheFaithofChildren.aac
audioBytes: 81400067
youtube: n21gGRCene0
---
