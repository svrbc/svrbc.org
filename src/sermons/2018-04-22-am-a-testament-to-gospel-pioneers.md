---
id: 2018-04-22-am
title: A Testament To Gospel Pioneers
date: 2018-04-22
text: Romans 16:1–16
preacher: josh-sheldon
series: romans
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2018/180422-AM-ATestamentToGospelPioneers.mp3
audioBytes: 44389519
youtube: BIiaGQsHWJ8
---
