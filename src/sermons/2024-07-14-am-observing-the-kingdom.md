---
id: 2024-07-14-am
title: Observing The Kingdom
date: 2024-07-14
text: Luke 17:20–25
preacher: conley-owens
series: luke
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2024/240714-ObservingTheKingdom.aac
youtube: Kr3hKImLWU8
---
