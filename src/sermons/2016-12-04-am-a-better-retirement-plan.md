---
id: 2016-12-04-am
title: A Better Retirement Plan
date: 2016-12-04
text: Luke 12:13–21
preacher: conley-owens
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2016/161204+A+Better+Retirement+Plan.mp3
audioBytes: 32091421
youtube: UhSYhoNKyII
---
