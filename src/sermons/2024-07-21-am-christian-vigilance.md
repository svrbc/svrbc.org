---
id: 2024-07-21-am
title: Christian Vigilance
date: 2024-07-21
text: Luke 17:26–37
preacher: conley-owens
series: luke
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2024/240721-ChristianVigilance.aac
youtube: motfmGXhHfE
---
