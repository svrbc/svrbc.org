---
id: 2012-11-25-am
title: 'No Partiality, and Why Not'
date: 2012-11-25
text: James 2:1–10
preacher: steve-mixsell
series: james
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2012/2012-11-25+No+Partiality+And+Why+Not.mp3
audioBytes: 59359170
---
