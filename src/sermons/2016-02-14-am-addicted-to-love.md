---
id: 2016-02-14-am
title: Addicted To Love
date: 2016-02-14
text: Acts 2:47
preacher: henry-wiley
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2016/160214+Addicted+to+Love.mp3
audioBytes: 34193328
youtube: _gg-S0xT1Q4
---
