---
id: 2017-05-07-am
title: A New Dimension
date: 2017-05-07
text: Romans 8:5–8
preacher: josh-sheldon
series: romans
audio: https://storage.googleapis.com/pbc-ca-sermons/2017/170507+A+New+Dimension.mp3
audioBytes: 53051708
youtube: Tvp-5qsKi8s
---
