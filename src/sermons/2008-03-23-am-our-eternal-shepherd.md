---
id: 2008-03-23-am
title: Our Eternal Shepherd
date: 2008-03-23
text: Psalm 23
preacher: josh-sheldon
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2008/2008-03-23+Our+Eternal+Shepherd.mp3
audioBytes: 48229023
---
