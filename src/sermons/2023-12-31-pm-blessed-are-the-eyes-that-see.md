---
id: 2023-12-31-pm
title: Blessed Are The Eyes That See
date: 2023-12-31
text: Luke 10:21–24
preacher: brian-garcia
series: luke
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2023/231231-BlessedAreTheEyesThatSee.aac
youtube: NgJ43AM5LDc
---
