---
id: 2023-09-17-am
title: Hold Fast and Bear Fruit
date: 2023-09-17
text: Luke 8:1–15
preacher: brian-garcia
series: luke
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2023/230917-HoldFastAndBearFruit.aac
youtube: PEPMiyI-Xlw
---
