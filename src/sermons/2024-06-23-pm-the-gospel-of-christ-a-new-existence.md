---
id: 2024-06-23-pm
title: The Gospel Of Christ - A New Existence
date: 2024-06-23
text: Romans 6:5–7
preacher: josh-sheldon
unregisteredSeries: The Gospel Of Christ
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2024/240623-TheGospelOfChrist-ANewExistence.aac
youtube: tnnExAshG5I
---
