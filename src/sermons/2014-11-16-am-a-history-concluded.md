---
id: 2014-11-16-am
title: A History Concluded
date: 2014-11-16
text: John 18:12–19:2
preacher: josh-sheldon
series: john
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2014/141116+A+History+Concluded.mp3
audioBytes: 37920311
---
