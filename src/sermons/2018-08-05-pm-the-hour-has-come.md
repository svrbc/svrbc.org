---
id: 2018-08-05-pm
title: The Hour Has Come
date: 2018-08-05
text: Luke 22:53
preacher: josh-sheldon
series: meditations-upon-the-cross
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2018/180805-PM-TheHourHasCome.mp3
audioBytes: 18493169
---
