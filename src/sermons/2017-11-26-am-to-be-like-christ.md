---
id: 2017-11-26-am
title: To Be Like Christ
date: 2017-11-26
text: Romans 12:1–2
preacher: josh-sheldon
series: romans
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2017/171126+To+Be+Like+Christ.mp3
audioBytes: 51466407
youtube: iUaacTBARVo
---
