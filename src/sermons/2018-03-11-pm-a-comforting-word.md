---
id: 2018-03-11-pm
title: A Comforting Word
date: 2018-03-11
text: Nahum 1:9–15
preacher: josh-sheldon
series: minor-prophets
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2018/180311-PM-AComfortingWord.mp3
audioBytes: 28549692
youtube: wDO9T1kbGnM
---
