---
id: 2012-09-30-am
title: 'Jesus, The Good Shepherd'
date: 2012-09-30
text: John 10:1–30
preacher: josh-sheldon
series: john
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2012/2012-09-30+Jesus,+The+Good+Shepherd.mp3
audioBytes: 59047254
---
