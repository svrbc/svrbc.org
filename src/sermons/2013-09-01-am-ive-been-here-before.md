---
id: 2013-09-01-am
title: I’ve Been Here Before
date: 2013-09-01
text: Psalm 122
preacher: josh-sheldon
series: psalms-of-ascents
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2013/2013-09-01+I%27ve+Been+Here+Before.mp3
audioBytes: 35372496
---
