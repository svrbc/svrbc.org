---
id: 2019-05-05-pm
title: The Absolute Standard
date: 2019-05-05
text: Isaiah 5:18–30
preacher: conley-owens
series: isaiah
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2019/190505-PM-TheAbsoluteStandard.mp3
audioBytes: 40231658
---
