---
id: 2024-07-28-am
title: Persistent Prayer
date: 2024-07-28
text: Luke 18:1–8
preacher: conley-owens
series: luke
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2024/240728-PersistentPrayer.aac
youtube: suUBJZMTG_U
---
