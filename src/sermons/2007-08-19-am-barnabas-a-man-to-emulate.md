---
id: 2007-08-19-am
title: 'Barnabas, A Man To Emulate'
date: 2007-08-19
text: Acts 4:36
preacher: josh-sheldon
series: acts
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2007/2007-08-19+Barnabas,+A+Man+To+Emulate.mp3
audioBytes: 38042964
---
