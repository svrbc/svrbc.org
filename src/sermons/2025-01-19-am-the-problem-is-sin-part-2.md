---
id: 2025-01-19-am
title: The Problem Is Sin - Part 2
date: 2025-01-19
text: Genesis 3:11–13
preacher: tim-mullet
series: basic-truths
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2025/250119-BasicTruths-TheProblemIsSin-Part2.aac
audioBytes: 57485919
youtube: yV68_dICOZI
---
