---
id: 2024-12-22-am
title: There Are Two Kinds of Counsel
date: 2024-12-22
text: Genesis 3:1–6
preacher: tim-mullet
series: basic-truths
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2024/241222-BasicTruths-ThereAreTwoKindsofCounsel.aac
audioBytes: 53222055
youtube: mJyBX_K1ZL0
---
