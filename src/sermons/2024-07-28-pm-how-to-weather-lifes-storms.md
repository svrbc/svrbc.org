---
id: 2024-07-28-pm
title: How To Weather Life’s Storms
date: 2024-07-28
text: Mark 8:35–38
unregisteredPreacher: Doug Myers
unregisteredSeries: Weather Life’s Storms
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2024/240728-PersistentPrayer.aac
youtube: MUmedk3mFLM
---
