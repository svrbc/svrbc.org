---
id: 2022-01-23-am
title: God’s Holy Temple
date: 2022-01-23
text: 1 Corinthians 3:16–17
preacher: josh-sheldon
series: maturity-in-christ
audio: https://storage.googleapis.com/pbc-ca-sermons/2022/220123-GodsHolyTemple.aac
audioBytes: 55466507
youtube: 6jRS4wTmJgU
---
