---
id: 2023-11-19-am
title: The Lord of New Beginnings
date: 2023-11-19
text: Psalm 127
preacher: josh-sheldon
unregisteredSeries: Psalms of Ascent
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2023/231119-TheLordOfNewBeginnings.aac
youtube: eN54JUz4HCA
---
