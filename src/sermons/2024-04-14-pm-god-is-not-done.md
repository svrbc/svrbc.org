---
id: 2024-04-14-pm
title: God Is Not Done
date: 2024-04-14
text: John 3:16
preacher: josh-sheldon
series: john
audio: https://storage.googleapis.com/pbc-ca-sermons/2024/240414-TrueRiches.aac
youtube: 0Ouu2akio6A
---
