---
id: 2008-07-06-am
title: History Repeats Itself
date: 2008-07-06
text: Acts 12
preacher: josh-sheldon
series: acts
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2008/2008-07-06+History+Repeats+Itself.mp3
audioBytes: 54725466
---
