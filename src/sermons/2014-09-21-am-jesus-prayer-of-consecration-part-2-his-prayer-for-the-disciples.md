---
id: 2014-09-21-am
title: 'Jesus’s Prayer of Consecration Part 2: His Prayer for the Disciples'
date: 2014-09-21
text: John 17:6–19
preacher: josh-sheldon
series: john
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2014/140921+Jesus+-+Prayer+of+Consecration+Part+2.mp3
audioBytes: 43009758
---
