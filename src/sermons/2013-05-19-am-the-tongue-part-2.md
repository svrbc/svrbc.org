---
id: 2013-05-19-am
title: 'The Tongue, Part 2'
date: 2013-05-19
text: James 3:1–12
preacher: steve-mixsell
series: james
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2013/2013-05-19+The+Tongue+Part+2.mp3
audioBytes: 69551067
---
