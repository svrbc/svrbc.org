---
id: 2011-02-06-am
title: The Preeminence of Christ
date: 2011-02-06
text: Colossians 1:15–20
preacher: jesse-lu
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2011/2011-02-06+The+Preeminence+of+Christ.mp3
audioBytes: 47559552
---
