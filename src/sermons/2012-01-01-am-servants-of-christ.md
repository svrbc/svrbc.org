---
id: 2012-01-01-am
title: Servants of Christ
date: 2012-01-01
text: 2 Timothy 2:1–7
preacher: mike-kelley
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2012/2012-01-01+Servants+of+Christ.mp3
audioBytes: 51303564
---
