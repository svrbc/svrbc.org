---
id: 2013-06-09-am
title: 'The Revelation of Jesus Christ, Part 2'
date: 2013-06-09
text: Revelation 1:9–20
preacher: jesse-lu
series: the-churches-of-revelation
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2013/2013-06-09+The+Revelation+of+Jesus+Christ%2C+Part+2.mp3
audioBytes: 52080018
---
