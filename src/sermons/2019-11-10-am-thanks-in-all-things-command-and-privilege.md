---
id: 2019-11-10-am
title: Thanks In All Things (Command And Privilege)
date: 2019-11-10
text: 1 Thessalonians 5:16–18
preacher: josh-sheldon
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2019/191110-AM-ThanksInAllThingsCommandAndPrivilege.mp3
audioBytes: 39750056
---
