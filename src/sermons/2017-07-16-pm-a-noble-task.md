---
id: 2017-07-16-pm
title: A Noble Task
date: 2017-07-16
text: 1 Timothy 3
preacher: josh-sheldon
series: 1-timothy
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2017/170716+PM+Service+-+A+Noble+Task.mp3
audioBytes: 32541960
youtube: DcAloKtYDG0
---
