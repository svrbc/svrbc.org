---
id: 2019-05-26-am
title: A Busy Soul
date: 2019-05-26
text: Luke 10:38–42
preacher: daniel-uhm
audio: https://storage.googleapis.com/pbc-ca-sermons/2019/190526-AM-ABusySoul.mp3
audioBytes: 45826881
---
