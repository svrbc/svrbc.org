---
id: 2019-02-24-pm
title: Divine Protection
date: 2019-02-24
text: Isaiah 4:2–6
preacher: conley-owens
series: isaiah
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2019/190224-PM-DivineProtection.mp3
audioBytes: 30153400
---
