---
id: 2021-02-07-am
title: Boldness in Our God
date: 2021-02-07
text: 1 Thessalonians 2:1–2
preacher: josh-sheldon
series: awaiting-christ
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2021/210207-BoldnessInOurGod.mp3
audioBytes: 65957137
youtube: iI8Wfz1auls
---
