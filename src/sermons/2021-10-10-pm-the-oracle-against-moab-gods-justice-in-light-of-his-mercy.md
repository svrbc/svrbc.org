---
id: 2021-10-10-pm
title: 'The Oracle Against Moab: God’s Justice in Light of His Mercy'
date: 2021-10-10
text: Isaiah 16:6–14
preacher: conley-owens
unregisteredSeries: The Oracles Against the Nations
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2021/211010-TheOracleAgainstMoab-GodsJusticeInLightOfHisMercy.aac
audioBytes: 30776925
youtube: IOPcrO3tL1o
---
