---
id: 2023-12-24-am
title: The Apostleship Of Christ
date: 2023-12-24
text: Hebrews 3:1–2
preacher: conley-owens
series: hebrews
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2023/231224-TheApostleshipOfChrist.aac
youtube: tuwMspAGeYg
---
