---
id: 2015-09-27-am
title: God Brings Life In The Face Of Death
date: 2015-09-27
text: 1 Kings 17:17–24
preacher: josh-sheldon
series: elijah-elisha
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2015/150927+God+Brings+Life+in+the+Face+of+Death.mp3
audioBytes: 53811107
youtube: RfejfukpegE
---
