---
id: 2024-02-04-pm
title: Hypocrisy and Repentance
date: 2024-02-04
text: Luke 11:45–54
preacher: brian-garcia
series: luke
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2024/230128-HypocrisyandRepentance.aac
youtube: 4Yt8DppFcVA
---
