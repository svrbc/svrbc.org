---
id: 2015-03-01-am
title: Effective and Fervent Prayer Avails Much
date: 2015-03-01
text: James 5:13–18
preacher: steve-mixsell
series: james
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2015/150301+Effective+and+Fervent+Prayer+Avails+Much.mp3
audioBytes: 58414059
---
