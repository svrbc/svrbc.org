---
id: 2016-02-28-am
title: 'Naaman’s Cure, Our Cure'
date: 2016-02-28
text: 2 Kings 5:1–19
preacher: josh-sheldon
series: 12kings
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2016/160228+Namaans+Cure+Our+Cure.mp3
audioBytes: 56440057
youtube: iJ-BCaehfCc
---
