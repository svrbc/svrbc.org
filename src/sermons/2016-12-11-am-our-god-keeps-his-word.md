---
id: 2016-12-11-am
title: Our God Keeps His Word
date: 2016-12-11
text: Luke 1:5–38
preacher: josh-sheldon
series: advent-2016
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2016/161211+Our+God+Keeps+His+Word.mp3
audioBytes: 56575893
youtube: BpFyydUixgY
---
