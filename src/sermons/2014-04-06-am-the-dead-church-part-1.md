---
id: 2014-04-06-am
title: 'The Dead Church, Part 1'
date: 2014-04-06
text: Revelation 3:1–6
preacher: jesse-lu
series: the-churches-of-revelation
audio: https://storage.googleapis.com/pbc-ca-sermons/2014/2014-04-06+12h37m18s.mp3
audioBytes: 60764460
---
