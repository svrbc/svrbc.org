---
id: 2024-08-18-am
title: Asking The Right Question
date: 2024-08-18
text: Colossians 2:8–15
preacher: josh-sheldon
series: the-sufficiency-of-christ
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2024/240818-AskingTheRightQuestion.aac
youtube: D7v83yeMg-0
---
