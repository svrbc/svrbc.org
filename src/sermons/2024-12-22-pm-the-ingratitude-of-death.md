---
id: 2024-12-22-pm
title: The Ingratitude Of Death
date: 2024-12-22
text: Isaiah 38:17–20
preacher: conley-owens
series: isaiah
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2024/241215-TheSentenceOfDeath.aac
audioBytes: 34613344
youtube: xbbDl7OZr-A
---
