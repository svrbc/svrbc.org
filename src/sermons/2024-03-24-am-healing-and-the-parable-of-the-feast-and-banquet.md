---
id: 2024-03-24-am
title: Healing and the Parable of the Feast & Banquet
date: 2024-03-24
text: Luke 14:1–24
preacher: brian-garcia
series: luke
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2024/240324-HealingAndTheParableOfTheFeastAndBanquet.aac
youtube: YGovBbs1cyQ
---
