---
id: 2020-02-23-pm
title: A Call To Humility
date: 2020-02-23
text: Isaiah 9:8–10:4
preacher: conley-owens
series: isaiah
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2020/200223-PM-ACallToHumility.mp3
audioBytes: 41901402
---
