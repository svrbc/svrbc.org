---
id: 2024-03-10-am
title: A Warning for Worship
date: 2024-03-10
text: Hebrews 3:7–11
preacher: conley-owens
series: hebrews
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2024/240310-AWarningforWorship.aac
youtube: UaAF0X9Aa5Y
---
