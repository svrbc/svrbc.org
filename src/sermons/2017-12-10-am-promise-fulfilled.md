---
id: 2017-12-10-am
title: Promise Fulfilled
date: 2017-12-10
text: Matthew 1:1–17
preacher: josh-sheldon
series: advent-2017
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2017/171210+Promise+Fulfilled.mp3
audioBytes: 57296925
youtube: Bo9860_BPD4
---
