---
id: 2017-05-14-pm
title: Timothy’s Charge
date: 2017-05-14
text: 1 Timothy 1:1–3
preacher: josh-sheldon
series: 1-timothy
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2017/170514+PM+Service+-+Timothys+Charge.mp3
audioBytes: 34258192
youtube: e4tUfQxGZcI
---
