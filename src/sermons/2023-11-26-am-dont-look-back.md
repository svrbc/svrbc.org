---
id: 2023-11-26-am
title: Don’t Look Back
date: 2023-11-26
text: Luke 9:46–62
preacher: brian-garcia
series: luke
audio: https://storage.googleapis.com/pbc-ca-sermons/2023/231126-DontLookBack.aac
youtube: gxkpxEk7vyg
---
