---
id: 2014-10-26-am
title: Where Is Your Treasure?
date: 2014-10-26
text: Luke 16:19–31
preacher: henry-wiley
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2014/141026+Where+Is+Your+Treasure.mp3
audioBytes: 27951561
---
