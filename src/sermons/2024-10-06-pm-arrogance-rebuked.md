---
id: 2024-10-06-pm
title: Arrogance Rebuked
date: 2024-10-06
text: Isaiah 37:21–29
preacher: conley-owens
series: isaiah
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2024/241006-ArroganceRebuked.aac
audioBytes: 41882932
youtube: ZTJQtVCDQag
---
