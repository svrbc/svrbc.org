---
id: 2021-10-31-pm
title: Aggressive Investment
date: 2021-10-31
text: Luke 19:11–27
preacher: conley-owens
series: kingdom-investing
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2021/211031-AggressiveInvestment.aac
audioBytes: 38835999
youtube: L26L-V_fjw8
---
