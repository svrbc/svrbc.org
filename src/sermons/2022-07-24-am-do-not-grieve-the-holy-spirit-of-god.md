---
id: 2022-07-24-am
title: Do Not Grieve the Holy Spirit of God
date: 2022-07-24
text: Ephesians 4:25–32
preacher: brian-garcia
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2022/220724-DoNotGrieveTheHolySpiritofGod.aac
audioBytes: 51736177
youtube: iK2Cazwwfcc
---
