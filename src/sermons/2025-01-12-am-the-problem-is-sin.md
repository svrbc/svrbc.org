---
id: 2025-01-12-am
title: The Problem Is Sin
date: 2025-01-12
text: Genesis 3:7–10
preacher: tim-mullet
series: basic-truths
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2025/250112-BasicTruths-TheProblemIsSin.aac
audioBytes: 62771411
youtube: YD5WQc3jiO0
---
