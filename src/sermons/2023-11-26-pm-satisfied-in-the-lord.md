---
id: 2023-11-26-pm
title: Satisfied in the Lord
date: 2023-11-26
text: Psalm 128
preacher: josh-sheldon
unregisteredSeries: Psalms of Ascent
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2023/231126-SatisfiedInTheLord.aac
youtube: 8QtiytxuGK8
---
