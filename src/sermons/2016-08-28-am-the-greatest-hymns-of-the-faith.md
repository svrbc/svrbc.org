---
id: 2016-08-28-am
title: The Greatest Hymns of the Faith
date: 2016-08-28
text: 1 Timothy 3:14–16
preacher: josh-sheldon
series: hymns-of-scripture
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2016/160828+The+Greatest+Hymns+of+Faith.mp3
audioBytes: 45638327
youtube: dXkd-tTvMoI
---
