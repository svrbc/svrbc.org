---
id: 2017-09-24-am
title: An Accomplished Goal
date: 2017-09-24
text: Romans 10:1–4
preacher: josh-sheldon
series: romans
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2017/170924+An+Accomplished+Goal.mp3
audioBytes: 43844910
youtube: 1ADODOY4exE
---
