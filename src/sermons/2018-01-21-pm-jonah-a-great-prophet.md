---
id: 2018-01-21-pm
title: 'Jonah, A Great Prophet'
date: 2018-01-21
text: Jonah 1:1–3
preacher: josh-sheldon
series: jonah
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2018/180121-PM-Jonah-AGreatProphet.mp3
audioBytes: 37641152
youtube: 6mL4w6SyFBk
---
