---
id: 2021-04-25-am
title: Who Killed Jesus
date: 2021-04-25
text: Isaiah 53
preacher: josh-sheldon
audio: https://storage.googleapis.com/pbc-ca-sermons/2021/210425-WhoKilledJesus.aac
audioBytes: 49586849
youtube: MhgM6GzqO7I
---
