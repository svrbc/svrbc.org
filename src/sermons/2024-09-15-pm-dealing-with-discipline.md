---
id: 2024-09-15-pm
title: Dealing With Discipline
date: 2024-09-15
text: Isaiah 36:21–37:4
preacher: conley-owens
series: isaiah
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2024/240915-DealingWithDiscipline.aac
audioBytes: 36917273
youtube: 14b56stgDC8
---
