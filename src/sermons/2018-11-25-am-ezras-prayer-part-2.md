---
id: 2018-11-25-am
title: Ezra’s Prayer Part 2
date: 2018-11-25
text: Ezra 9:10–15
preacher: josh-sheldon
series: ezra-nehemiah
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2018/181125-AM-EzrasPrayerPt2.mp3
audioBytes: 49948784
---
