---
id: 2024-01-28-am
title: A Kingdom Divided
date: 2024-01-28
text: Luke 11:14–26
preacher: brian-garcia
series: luke
audio: https://storage.googleapis.com/pbc-ca-sermons/2024/240128-AKingdomDivided.aac
youtube: phMbkJTSp5A
---
