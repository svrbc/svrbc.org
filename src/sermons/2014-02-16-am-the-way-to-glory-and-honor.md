---
id: 2014-02-16-am
title: The Way to Glory and Honor
date: 2014-02-16
text: John 12:23–26
preacher: josh-sheldon
series: john
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2014/2014-02-16+The+Way+to+Glory+and+Honor.mp3
audioBytes: 48454203
---
