---
id: 2023-05-28-pm
title: Exposing Folly
date: 2023-05-28
text: Isaiah 31:3–5
preacher: conley-owens
series: isaiah
audio: https://storage.googleapis.com/pbc-ca-sermons/2023/230528-ExposingFolly.aac
youtube: xq3_xQVDXH8
---
