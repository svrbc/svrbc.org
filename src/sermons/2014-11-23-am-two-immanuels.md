---
id: 2014-11-23-am
title: Two Immanuels
date: 2014-11-23
text: Isaiah 7:1–8:10
preacher: conley-owens
audio: https://storage.googleapis.com/pbc-ca-sermons/2014/141123+Two+Immanuels.mp3
audioBytes: 38267205
---
