---
id: 2020-08-16-am
title: Contentment In Christ
date: 2020-08-16
text: Philippians 4:10–13
preacher: josh-sheldon
series: philippians
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2020/200816-ContentmentInChrist.mp3
audioBytes: 58497193
youtube: m-GYrrDAm0Q
---
