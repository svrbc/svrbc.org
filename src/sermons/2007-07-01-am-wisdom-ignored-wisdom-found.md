---
id: 2007-07-01-am
title: 'Wisdom Ignored, Wisdom Found'
date: 2007-07-01
text: Job 28
preacher: josh-sheldon
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2007/2007-07-01+Wisdom+Ignored,+Wisdom+Found.mp3
audioBytes: 46594383
---
