---
id: 2014-06-29-am
title: The Peace That Christ Gives
date: 2014-06-29
text: John 14:25–31
preacher: josh-sheldon
series: john
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2014/2014-06-29+The+Peace+That+Christ+Gives.mp3
audioBytes: 48849102
---
