---
id: 2020-12-13-am
title: A Model Church
date: 2020-12-13
text: Acts 2:42–47
preacher: josh-sheldon
series: kingdom-community
audio: https://storage.googleapis.com/pbc-ca-sermons/2020/201213-AModelChurch.mp3
audioBytes: 69519546
youtube: RHz8mEaCzkc
---
