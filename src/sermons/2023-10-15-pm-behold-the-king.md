---
id: 2023-10-15-pm
title: Behold The King
date: 2023-10-15
text: Isaiah 33:17–19
preacher: conley-owens
series: isaiah
audio: https://storage.googleapis.com/pbc-ca-sermons/2023/231015-BeholdTheKing.aac
youtube: BM4FRjDxMN8
---
