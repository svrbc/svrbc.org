---
id: 2018-12-30-am
title: A Great Man Prays A Great Prayer To A Great God
date: 2018-12-30
text: Nehemiah 1:5
preacher: josh-sheldon
series: ezra-nehemiah
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2018/181230-AM-AGreatManPraysAGreatPrayerToAGreatGod.mp3
audioBytes: 56662071
youtube: stlbccFVRkU
---
