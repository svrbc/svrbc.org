---
id: 2012-07-29-am
title: Light of the World
date: 2012-07-29
text: John 8:12
preacher: josh-sheldon
series: john
audio: https://storage.googleapis.com/pbc-ca-sermons/2012/2012-07-29+John+812.mp3
audioBytes: 48309504
---
