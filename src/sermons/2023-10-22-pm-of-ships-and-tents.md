---
id: 2023-10-22-pm
title: Of Ships and Tents
date: 2023-10-22
text: Isaiah 33:20–24
preacher: conley-owens
series: isaiah
audio: https://storage.googleapis.com/pbc-ca-sermons/2023/231022-OfShipsAndTents.aac
youtube: eFy8Zz66lJ4
---
