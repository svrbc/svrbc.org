---
id: 2013-03-17-am
title: The Great Command – Love God
date: 2013-03-17
text: Matthew 24:37
preacher: josh-sheldon
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2013/2013-03-17+The+Great+Command-Love+God.mp3
audioBytes: 43799232
---
