---
id: 2023-03-26-pm
title: A Message Concerning the World
date: 2023-03-26
text: Obadiah 1–15
preacher: josh-sheldon
unregisteredSeries: Obadiah
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2023/230326-AMessageConcerningTheWorld.aac
youtube: YT3N8YyYXUI
---
