---
id: 2020-02-02-am
title: Seeing With The Eyes Of Faith
date: 2020-02-02
text: Philippians 1:12–18
preacher: josh-sheldon
series: philippians
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2020/200202-AM-SeeingWithTheEyesOfFaith.mp3
audioBytes: 48588350
---
