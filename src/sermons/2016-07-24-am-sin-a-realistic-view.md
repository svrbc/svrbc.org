---
id: 2016-07-24-am
title: Sin – A Realistic View
date: 2016-07-24
text: Psalm 39
preacher: josh-sheldon
series: the-psalms
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2016/160724+Sin-A+Realistic+View.mp3
audioBytes: 47877760
youtube: DM5-5YAIjvk
---
