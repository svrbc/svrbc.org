---
id: 2016-07-10-pm
title: Beatitudes Part 1
date: 2016-07-10
text: Matthew 5
preacher: josh-sheldon
series: beatitudes
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2016/160710+The+Beatitudes+Pt+1.mp3
audioBytes: 33735677
youtube: KwoAyGyRSlE
---
