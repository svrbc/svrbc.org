---
id: 2024-01-07-pm
title: Who Owns You?
date: 2024-01-07
text: Psalm 129
preacher: josh-sheldon
unregisteredSeries: Psalms of Ascent
audio: https://storage.googleapis.com/pbc-ca-sermons/2024/240107-WhoOwnsYou.aac
youtube: jSfN87rvgP4
---
