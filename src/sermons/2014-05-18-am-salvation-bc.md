---
id: 2014-05-18-am
title: Salvation BC
date: 2014-05-18
text: Romans 3:23–26
preacher: conley-owens
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2014/2014-05-18+Salvation+BC.mp3
audioBytes: 39520812
---
