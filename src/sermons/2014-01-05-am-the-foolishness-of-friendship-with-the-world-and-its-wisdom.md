---
id: 2014-01-05-am
title: The Foolishness of Friendship With the World and its Wisdom
date: 2014-01-05
text: James 4:4–10
preacher: steve-mixsell
series: james
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2014/2014-01-05+The+Foolishness+of+Friendship+With+the+World+and+its+Wisdom.mp3
audioBytes: 45532284
---
