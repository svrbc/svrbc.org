---
id: 2016-10-30-am
title: A Trustworthy Word
date: 2016-10-30
text: Romans 1:16–17
preacher: josh-sheldon
series: romans
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2016/161030+A+Trustworthy+Word.mp3
audioBytes: 49755689
youtube: V5WP3xCc1ck
---
