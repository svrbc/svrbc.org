---
id: 2018-03-18-am
title: An Acceptable Offering
date: 2018-03-18
text: Romans 15:14–16
preacher: josh-sheldon
series: romans
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2018/180318-AM-AnAcceptableOffering.mp3
audioBytes: 47969333
youtube: ADwPwg-q7gU
---
