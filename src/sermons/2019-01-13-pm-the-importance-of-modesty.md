---
id: 2019-01-13-pm
title: The Importance Of Modesty
date: 2019-01-13
text: Isaiah 3:16–26
preacher: conley-owens
series: isaiah
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2019/190113-PM-TheImportanceOfModesty.mp3
audioBytes: 25744772
youtube: KY20IiDuFXQ
---
