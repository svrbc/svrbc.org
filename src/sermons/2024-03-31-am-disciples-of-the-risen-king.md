---
id: 2024-03-31-am
title: Disciples Of The Risen King
date: 2024-03-31
text: Luke 14:25–35
preacher: brian-garcia
series: luke
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2024/240331-DisciplesOfTheRisenKing.aac
youtube: lXQuB8h_0qw
---
