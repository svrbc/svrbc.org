---
id: 2017-11-19-am
title: Living Sacrifices
date: 2017-11-19
text: Romans 12:1–2
preacher: josh-sheldon
series: romans
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2017/171119+Living+Sacrifices.mp3
audioBytes: 46141185
youtube: wP3s_LfaTfc
---
