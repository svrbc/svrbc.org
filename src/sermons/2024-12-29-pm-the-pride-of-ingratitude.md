---
id: 2024-12-29-pm
title: The Pride of Ingratitude
date: 2024-12-29
text: Isaiah 39
preacher: conley-owens
series: isaiah
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2024/241229-ThePrideofIngratitude.aac
audioBytes: 46019428
youtube: ud6bIlYr_7Y
---
