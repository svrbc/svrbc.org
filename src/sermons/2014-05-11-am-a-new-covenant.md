---
id: 2014-05-11-am
title: A New Covenant
date: 2014-05-11
text: John 13:31–35
preacher: josh-sheldon
series: john
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2014/2014-05-11+A+New+Covenant.mp3
audioBytes: 49057602
---
