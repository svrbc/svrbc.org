---
id: 2020-01-26-pm
title: The King Through Eyes Of Fear And Eyes Of Faith
date: 2020-01-26
text: Psalm 48
preacher: jesus-mancilla
series: kingship-psalms
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2020/200126-PM-TheKingThroughEyeOfFearAndEyeOfFaith.mp3
audioBytes: 40578584
---
