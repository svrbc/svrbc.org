---
id: 2016-10-02-pm
title: Beatitudes Part 12
date: 2016-10-02
text: Matthew 6:5–13
preacher: josh-sheldon
series: beatitudes
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2016/161002+Beatitudes+Part+12.mp3
audioBytes: 28094045
youtube: 44-75eidaqc
---
