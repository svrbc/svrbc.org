---
id: 2024-06-02-am
title: The Rich Man And Lazarus
date: 2024-06-02
text: Luke 16:19–26
preacher: conley-owens
series: luke
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2024/240602-TheRichManAndLazarus.aac
youtube: rkjV1nIR0nk
---
