---
id: 2017-06-04-pm
title: The Law’s Misused
date: 2017-06-04
text: 1 Timothy 1:8–11
preacher: josh-sheldon
series: 1-timothy
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2017/170604-PM-TheLawsMisused.mp3
audioBytes: 31233424
youtube: pH4FurL8zDo
---
