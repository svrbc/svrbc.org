---
id: 2013-08-11-am
title: The Fallen Church
date: 2013-08-11
text: Revelation 2:1–7
preacher: jesse-lu
series: the-churches-of-revelation
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2013/2013-08-11+The+Fallen+Church.mp3
audioBytes: 60879135
---
