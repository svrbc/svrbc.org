---
id: 2011-04-10-am
title: Why John the Baptist
date: 2011-04-10
text: John 1:6–13, 19–28
preacher: josh-sheldon
series: john
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2011/2011-04-10+Why+John+the+Baptist.mp3
audioBytes: 51093888
---
