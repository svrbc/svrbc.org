---
id: 2012-06-17-am
title: The Duty of Specks and Planks
date: 2012-06-17
text: Matthew 7:2–5
preacher: josh-sheldon
series: lessons-from-the-sermon-on-the-mount
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2012/2012-06-17+The+Duty+of+Specks+and+Planks.mp3
audioBytes: 57226632
---
