---
id: 2015-05-03-am
title: Confidence Well-Placed Hope well-Secured
date: 2015-05-03
text: Philippians 1:6
preacher: josh-sheldon
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2015/150503+Confidence+Well-Placed+Hope+well-Secured.mp3
audioBytes: 41400650
---
