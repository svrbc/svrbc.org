---
id: 2015-01-25-am
title: It’s Not Fair
date: 2015-01-25
text: Matthew 20:1–16
preacher: josh-sheldon
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2015/150125++It%27s+Not+Fair.mp3
audioBytes: 42067684
---
