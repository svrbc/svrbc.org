---
id: 2012-04-29-am
title: The Power and the Promise of the Revelation of Jesus Christ
date: 2012-04-29
text: Revelation 1:1–3
preacher: jesse-lu
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2012/2012-04-29+The+Power+and+the+Promise+of+the+Revelation+of+Jesus+Christ.mp3
audioBytes: 57131556
---
