---
id: 2016-05-08-am
title: The Horror of Unbelief
date: 2016-05-08
text: 2 Kings 6:24–7:20
preacher: josh-sheldon
series: 12kings
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2016/160508+The+Horror+of+Unbelief.mp3
audioBytes: 54541685
youtube: XcCVH17FU-g
---
