---
id: 2024-06-16-pm
title: The Gospel Of Christ - Personally Yours  III
date: 2024-06-16
text: Romans 6:1–4
preacher: josh-sheldon
unregisteredSeries: The Gospel Of Christ
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2024/240616-TheGospelOfChrist-PersonallyYours3.aac
youtube: t82lo0Q1cfQ
---
