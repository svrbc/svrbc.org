---
id: 2011-12-11-am
title: The Lord at the Pool
date: 2011-12-11
text: John 5:1–15
preacher: josh-sheldon
series: john
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2011/2011-12-11+The+Lord+at+the+Pool.mp3
audioBytes: 39667179
---
