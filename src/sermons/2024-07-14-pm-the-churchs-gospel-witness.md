---
id: 2024-07-14-pm
title: The Church’s Gospel Witness
date: 2024-07-14
text: Acts 2:42–47
unregisteredPreacher: Stephen Louis
unregisteredSeries: Church's Gospel Witness
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2024/240714-TheChurchsGospelWitness.aac
youtube: kG5CBEmA7DU
---
