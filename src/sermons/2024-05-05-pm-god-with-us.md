---
id: 2024-05-05-pm
title: God With Us
date: 2024-05-05
text: Psalm 132
preacher: josh-sheldon
unregisteredSeries: Psalms of Ascent
audio: https://storage.googleapis.com/pbc-ca-sermons/2024/240505-GodWithUs.aac
youtube: MmN4BDp9KUY
---
