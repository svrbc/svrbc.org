---
id: 2009-01-11-am
title: Victims of our Environment?
date: 2009-01-11
text: 2 Chronicles 29
preacher: josh-sheldon
series: kings-of-judah
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2020/200105-AM-SufficientInChrist.mp3
audioBytes: 44706332
---
