---
id: 2012-09-09-am
title: Where is My Honor?
date: 2012-09-09
text: Malachi 2:6–14
preacher: josh-sheldon
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2012/2012-09-09+Where+Is+My+Honor.mp3
audioBytes: 52091694
---
