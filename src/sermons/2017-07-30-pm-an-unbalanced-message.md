---
id: 2017-07-30-pm
title: An Unbalanced Message
date: 2017-07-30
text: 1 Timothy 4:6–16
preacher: josh-sheldon
series: 1-timothy
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2017/170730+PM+Service+-+An+Unblanced+Message.mp3
audioBytes: 29216746
youtube: rfjTy7FOv8U
---
