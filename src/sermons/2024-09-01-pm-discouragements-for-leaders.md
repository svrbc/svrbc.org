---
id: 2024-09-01-pm
title: Discouragements For Leaders
date: 2024-09-01
text: Isaiah 36:4–10
preacher: conley-owens
series: isaiah
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2024/240901-DiscouragementsForLeaders.aac
youtube: nr6lP4VHaXE
---
