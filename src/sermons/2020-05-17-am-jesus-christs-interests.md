---
id: 2020-05-17-am
title: Jesus Christ’s Interests
date: 2020-05-17
text: Philippians 2:19–30
preacher: josh-sheldon
series: philippians
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2020/200517-JesusChristsInterests.mp3
youtube: aEw76ty1Hus
---
