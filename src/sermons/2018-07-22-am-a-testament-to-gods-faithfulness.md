---
id: 2018-07-22-am
title: A Testament To God’s Faithfulness
date: 2018-07-22
text: Ezra 2
preacher: josh-sheldon
series: ezra-nehemiah
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2018/180722-AM-ATestamentToGodsFaithfulness.mp3
audioBytes: 52517562
youtube: WM2OaA1IXaA
---
