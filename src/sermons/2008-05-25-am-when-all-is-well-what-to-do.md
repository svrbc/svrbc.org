---
id: 2008-05-25-am
title: 'When All Is Well, What to Do'
date: 2008-05-25
text: Acts 9:31–43
preacher: josh-sheldon
series: acts
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2008/2008-05-25+When+All+Is+Well,+What+to+Do.mp3
audioBytes: 47824116
---
