---
id: 2019-04-28-am
title: The Word – Give Us More!
date: 2019-04-28
text: Nehemiah 8:13–18
preacher: josh-sheldon
series: ezra-nehemiah
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2019/190428-AM-TheWord-GiveUsMore.mp3
audioBytes: 45741619
---
