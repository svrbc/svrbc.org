---
id: 2024-05-12-am
title: The Permanence of the Law
date: 2024-05-12
text: Luke 16:14–18
preacher: conley-owens
series: luke
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2024/240512-ThePermanenceOfTheLaw.aac
youtube: RKHSPf1Y0TM
---
