---
id: 2023-11-12-am
title: God’s Majesty in a Faithless Generation
date: 2023-11-12
text: Luke 9:37–45
preacher: brian-garcia
series: luke
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2023/231112-GodsMajestyInAFaithlessGeneration.aac
youtube: PrWP5ldkIH0
---
