---
id: 2008-11-16-am
title: A Common Condition
date: 2008-11-16
text: 2 Chronicles 26:16
preacher: josh-sheldon
series: kings-of-judah
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2008/2008-11-16+A+Common+Condition.mp3
audioBytes: 54213807
---
