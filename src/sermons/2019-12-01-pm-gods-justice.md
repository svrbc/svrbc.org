---
id: 2019-12-01-pm
title: God’s Justice
date: 2019-12-01
text: Psalm 9
preacher: josh-sheldon
series: the-psalms
audio: https://storage.googleapis.com/pbc-ca-sermons/2019/191201-PM-GodsJustice.mp3
audioBytes: 39451732
---
