---
id: 2009-11-01-am
title: What You Can Bear
date: 2009-11-01
text: John 16:12
preacher: josh-sheldon
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2009/2009-11-01+What+You+Can+Bear.mp3
audioBytes: 48497988
---
