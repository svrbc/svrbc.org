---
id: 2024-08-25-am
title: 'Heavenly Thinking on Earth, Part 1'
date: 2024-08-25
text: Colossians 3:1–4
preacher: josh-sheldon
series: the-sufficiency-of-christ
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2024/240825-HeavenlyThinkingonEarthPart1.aac
youtube: W-phhbhlnNg
---
