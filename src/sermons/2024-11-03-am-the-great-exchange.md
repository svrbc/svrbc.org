---
id: 2024-11-03-am
title: The Great Exchange
date: 2024-11-03
text: Psalms 75; 116:12–13
preacher: josh-sheldon
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2024/241103-TheGreatExchange.aac
audioBytes: 48837628
youtube: U_9iy20MWtQ
---
