---
id: 2017-03-19-pm
title: The Seven Sayings
date: 2017-03-19
text: Luke 23:34
preacher: josh-sheldon
series: the-seven-last-sayings
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2017/170319-PM-TheSevenSayings.mp3
audioBytes: 19530075
youtube: Io4_MiIiBeI
---
