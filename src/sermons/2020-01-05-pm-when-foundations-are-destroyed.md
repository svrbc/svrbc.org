---
id: 2020-01-05-pm
title: When Foundations Are Destroyed
date: 2020-01-05
text: Psalm 11
preacher: josh-sheldon
series: the-psalms
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2020/200105-PM-WhenFoundationsAreDestroyed.mp3
audioBytes: 32747676
---
