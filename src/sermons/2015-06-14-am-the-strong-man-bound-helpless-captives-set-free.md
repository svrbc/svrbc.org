---
id: 2015-06-14-am
title: The Strong Man Bound Helpless Captives Set Free
date: 2015-06-14
text: Matthew 12:22–32
preacher: josh-sheldon
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2015/150614+The+Strong+Man+Bound+Helpless+Captives+Set+Free.mp3
audioBytes: 46154942
---
