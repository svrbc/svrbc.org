---
id: 2024-12-08-am
title: Man Is Made For Work
date: 2024-12-08
text: Genesis 2:15
preacher: tim-mullet
series: basic-truths
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2024/241208-BasicTruths-ManIsMadeForWork.aac
audioBytes: 75915895
youtube: pQy7cksmOzs
---
