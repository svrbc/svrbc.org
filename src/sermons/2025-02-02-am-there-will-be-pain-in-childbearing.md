---
id: 2025-02-02-am
title: There Will Be Pain In Childbearing
date: 2025-02-02
text: Genesis 3:16
preacher: tim-mullet
series: basic-truths
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2025/250202-BasicTruths-ThereWillbePainInChildbearing.aac
audioBytes: 61965010
youtube: 2JDLMYxJcMk
---
