---
id: 2018-07-08-am
title: Wide And Narrow Ways
date: 2018-07-08
text: Matthew 7:13–14
preacher: jason-smathers
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2018/180708-AM-WideAndNarrowWays.mp3
audioBytes: 41307064
youtube: f5CmgPrbxzY
---
