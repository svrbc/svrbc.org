---
id: 2022-07-17-am
title: 'Out with the Old, In with the New!'
date: 2022-07-17
text: Ephesians 4:17–24
preacher: brian-garcia
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2022/220710-OutWithTheOldInWithTheNew.aac
audioBytes: 45978869
youtube: 25nYLyv1un4
---
