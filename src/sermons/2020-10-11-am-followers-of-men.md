---
id: 2020-10-11-am
title: Followers of Men
date: 2020-10-11
text: 1 Corinthians 1:10–12
preacher: conley-owens
series: kingdom-community
audio: https://storage.googleapis.com/pbc-ca-sermons/2020/201011-FollowersOfMen.mp3
audioBytes: 57218238
youtube: VTE9kWh5ENs
---
