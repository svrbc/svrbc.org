---
id: 2017-10-29-am
title: The Five Solas – Our Precious Heritage
date: 2017-10-29
text: Isaiah 42:1–9
preacher: josh-sheldon
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2017/171029+The+Five+Solas+-+Our+Precious+Heritage.mp3
audioBytes: 45533901
youtube: ke75amqGGRQ
---
