---
id: 2021-12-12-am
title: Warnings to the Orderly
date: 2021-12-12
text: 2 Thessalonians 3:13–15
preacher: josh-sheldon
series: awaiting-christ
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2021/211212-WarningsToTheOrderly.aac
youtube: Jge-B1dGcok
---
