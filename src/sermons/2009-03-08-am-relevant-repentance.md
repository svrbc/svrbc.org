---
id: 2009-03-08-am
title: Relevant Repentance
date: 2009-03-08
text: 2 Chronicles 34–35
preacher: josh-sheldon
series: kings-of-judah
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2009/2009-03-08+Revelant+Repentance.mp3
audioBytes: 57058164
---
