---
id: 2011-11-13-am
title: The Transforming Power of the Gospel
date: 2011-11-13
text: John 4:28–30
preacher: josh-sheldon
series: john
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2011/2011-11-13+The+Transforming+Power+of+the+Gospel.mp3
audioBytes: 42257166
---
