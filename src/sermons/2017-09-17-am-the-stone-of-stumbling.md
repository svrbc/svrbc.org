---
id: 2017-09-17-am
title: The Stone of Stumbling
date: 2017-09-17
text: Romans 9:30–33
preacher: josh-sheldon
series: romans
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2017/170917+The+Stone+of+Stumbling.mp3
audioBytes: 55292816
youtube: F_4GEL0PQhU
---
