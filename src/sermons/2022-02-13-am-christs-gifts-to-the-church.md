---
id: 2022-02-13-am
title: Christ’s Gifts to the Church
date: 2022-02-13
text: Ephesians 4:7–16
preacher: josh-sheldon
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2022/220213-ChristsGiftsToTheChurch.aac
audioBytes: 44178979
youtube: KAeLwqUFN9Q
---
