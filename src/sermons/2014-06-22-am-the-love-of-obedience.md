---
id: 2014-06-22-am
title: The Love of Obedience
date: 2014-06-22
text: John 14:15–24
preacher: josh-sheldon
series: john
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2014/2014-06-22+The+Love+of+Obedience.mp3
audioBytes: 50842362
---
