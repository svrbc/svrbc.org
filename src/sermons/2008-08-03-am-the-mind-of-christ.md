---
id: 2008-08-03-am
title: The Mind Of Christ
date: 2008-08-03
text: Acts 11:19–30
preacher: josh-sheldon
series: acts
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2008/2008-08-03+The+Mind+Of+Christ.mp3
audioBytes: 44514804
---
