---
id: 2023-07-16-pm
title: The Three Commands
date: 2023-07-16
text: Micah 6:1–8
preacher: josh-sheldon
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2023/230716-TheThreeCommands.aac
youtube: BAzlw3ZWXDY
---
