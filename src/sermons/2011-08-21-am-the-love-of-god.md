---
id: 2011-08-21-am
title: The Love of God
date: 2011-08-21
text: John 3:16–17
preacher: josh-sheldon
series: john
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2011/2011-08-21+The+Love+of+God.mp3
audioBytes: 37363968
---
