---
id: 2008-06-01-am
title: Peter and Cornelius
date: 2008-06-01
text: Acts 10
preacher: josh-sheldon
series: acts
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2008/2008-06-01+Peter+and+Cornelius.mp3
audioBytes: 46859178
---
