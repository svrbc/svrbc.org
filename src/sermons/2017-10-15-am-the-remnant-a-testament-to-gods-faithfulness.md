---
id: 2017-10-15-am
title: The Remnant – A Testament to God’s Faithfulness
date: 2017-10-15
text: Romans 11:1–10
preacher: josh-sheldon
series: romans
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2017/171015+The+Remnant+-+A+Testament+to+Gods+Faithfulness.mp3
audioBytes: 49363254
youtube: Z5tdmXxAyn4
---
