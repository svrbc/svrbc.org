---
id: 2024-01-07-am
title: Choose The Good Portion
date: 2024-01-07
text: Luke 10:38–42
preacher: brian-garcia
series: luke
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2024/240107-ChooseTheGoodPortion.aac
youtube: yDtviwSCbZg
---
