---
id: 2015-02-08-am
title: The Ultimate Sign – The Resurrection of Jesus Christ
date: 2015-02-08
text: John 20:1–18, 24–29
preacher: josh-sheldon
series: the-passion-of-christ
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2015/150208+The+Ultimate+Sign+-+The+Resurrecction+of+Jesus+Christ.mp3
audioBytes: 40679720
---
