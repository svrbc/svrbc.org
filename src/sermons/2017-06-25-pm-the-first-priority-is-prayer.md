---
id: 2017-06-25-pm
title: The First Priority Is Prayer
date: 2017-06-25
text: 1 Timothy 2:1–4
preacher: josh-sheldon
series: 1-timothy
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2017/170625+PM+Service+-+The+First+Priority+Is+Prayer.mp3
audioBytes: 33634191
youtube: u8Tbj9oXL2M
---
