---
id: 2017-12-17-pm
title: 'What We Need: God’s Glory'
date: 2017-12-17
text: John 17:1–5
preacher: josh-sheldon
series: the-road-to-the-cross
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2017/171217+PM+Service+-+What+We+Need+-+Gods+Glory.mp3
audioBytes: 26470778
youtube: bum26IcbtHg
---
