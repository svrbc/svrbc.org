---
id: 2018-10-07-am
title: There And Back Again – The Israelites’ Tale
date: 2018-10-07
text: Ezra 6:19–22
preacher: josh-sheldon
series: ezra-nehemiah
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2018/181007-AM-ThereAndBackAgain-TheIsraelitesTale.mp3
audioBytes: 48340918
---
