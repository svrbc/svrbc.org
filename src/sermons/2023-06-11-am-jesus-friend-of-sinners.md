---
id: 2023-06-11-am
title: 'Jesus, Friend of Sinners'
date: 2023-06-11
text: Luke 5:27–39
preacher: brian-garcia
series: luke
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2023/230611-JesusFriendOfSinners.aac
youtube: B99ewXguCq4
---
