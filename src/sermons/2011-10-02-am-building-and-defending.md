---
id: 2011-10-02-am
title: Building and Defending
date: 2011-10-02
text: Nehemiah 4:17
preacher: josh-sheldon
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2011/2011-10-02+Building+and+Defending.mp3
audioBytes: 36361620
---
