---
id: 2017-02-12-am
title: The Primacy of Faith
date: 2017-02-12
text: Romans 3:27–4:25
preacher: josh-sheldon
series: romans
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2017/170212+The+Primacy+of+Faith.mp3
audioBytes: 47710140
youtube: Dzm--COut5E
---
