---
id: 2014-10-12-am
title: Jesus’ Prayer at Gethsemane – A Final Cry
date: 2014-10-12
text: Matthew 26:36–46
preacher: josh-sheldon
series: john
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2014/141012+Jesus+Prayer+at+Gethsemane+-+A+Final+Cry.mp3
audioBytes: 41327059
---
