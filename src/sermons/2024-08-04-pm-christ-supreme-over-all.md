---
id: 2024-08-04-pm
title: 'Christ, Supreme Over All'
date: 2024-08-04
text: Colossians 1:3–23
preacher: josh-sheldon
series: the-sufficiency-of-christ
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2024/240804-Christ-SupremeOverAll.aac
youtube: ljsAJy6detc
---
