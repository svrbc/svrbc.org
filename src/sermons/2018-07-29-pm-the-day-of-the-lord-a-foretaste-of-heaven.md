---
id: 2018-07-29-pm
title: The Day Of The Lord – A Foretaste Of Heaven
date: 2018-07-29
text: Zephaniah 3:9–20
preacher: josh-sheldon
series: minor-prophets
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2018/180729-PM-TheDayOfTheLord-AForetasteOfHeaven.mp3
audioBytes: 36076749
---
