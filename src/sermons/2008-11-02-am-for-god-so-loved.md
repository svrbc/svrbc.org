---
id: 2008-11-02-am
title: For God So Loved
date: 2008-11-02
text: John 3:16
preacher: josh-sheldon
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2008/2008-11-02+For+God+So+Loved.mp3
audioBytes: 42733380
---
