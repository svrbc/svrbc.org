---
id: 2022-06-26-pm
title: The Folly of Isolation
date: 2022-06-26
text: Proverbs 18:1
preacher: conley-owens
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2022/220626-TheFollyOfIsolation.aac
youtube: piKS7SNkuGI
---
