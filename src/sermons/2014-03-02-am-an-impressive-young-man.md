---
id: 2014-03-02-am
title: An Impressive Young Man
date: 2014-03-02
text: Matthew 19:16–30
preacher: josh-sheldon
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2014/2014-03-02+An+Impressive+Young+Man.mp3
audioBytes: 41631666
---
