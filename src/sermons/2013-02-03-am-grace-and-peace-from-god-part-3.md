---
id: 2013-02-03-am
title: 'Grace and Peace from God, Part 3'
date: 2013-02-03
text: Revelation 1:5
preacher: jesse-lu
series: the-churches-of-revelation
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2013/2013-02-03+Grace+And+Peace+From+God+Part.mp3
audioBytes: 54700446
---
