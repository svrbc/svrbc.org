---
id: 2021-01-17-am
title: True Conversion
date: 2021-01-17
text: 1 Thessalonians 1:1–5
preacher: josh-sheldon
series: awaiting-christ
audio: https://storage.googleapis.com/pbc-ca-sermons/2021/210117-TrueConversion.mp3
audioBytes: 65908863
youtube: R7Sa1t-VfDI
---
