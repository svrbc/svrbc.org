---
id: 2016-09-18-am
title: An Actor’s Guild
date: 2016-09-18
text: Matthew 6:1–18
preacher: josh-sheldon
series: beatitudes
audio: https://storage.googleapis.com/pbc-ca-sermons/2016/160918+An+Actors+Guild.mp3
audioBytes: 47563851
youtube: 4ZUI_5hG55E
---
