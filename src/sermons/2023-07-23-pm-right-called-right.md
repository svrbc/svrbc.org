---
id: 2023-07-23-pm
title: Right Called Right
date: 2023-07-23
text: Isaiah 32:5–8
preacher: conley-owens
series: isaiah
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2023/230723-RightCalledRight.aac
youtube: RTNlq2802uA
---
