---
id: 2008-10-12-am
title: Pray with Purpose
date: 2008-10-12
text: Psalm 51:1–4
preacher: michael-phillips
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2008/2008-10-12+Pray+with+Purpose.mp3
audioBytes: 46145274
---
