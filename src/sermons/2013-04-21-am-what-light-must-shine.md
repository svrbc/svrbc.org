---
id: 2013-04-21-am
title: What Light Must Shine
date: 2013-04-21
text: Matthew 5:14–16
preacher: josh-sheldon
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2013/2013-04-21+What+Light+Must+Shine.mp3
audioBytes: 48515919
---
