---
id: 2013-04-07-am
title: The Grace and Wrath of Jesus Christ
date: 2013-04-07
text: Revelation 1:5–8
preacher: jesse-lu
series: the-churches-of-revelation
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2013/2013-04-07+The+Grace+and+Wrath+of+Jesus+Christ.mp3
audioBytes: 62536710
---
