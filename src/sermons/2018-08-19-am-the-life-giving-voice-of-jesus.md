---
id: 2018-08-19-am
title: The Life-Giving Voice Of Jesus
date: 2018-08-19
text: John 5:25–29
preacher: henry-wiley
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2018/180819-AM-TheLife-GivingVoiceOfJesus.mp3
audioBytes: 34161244
---
