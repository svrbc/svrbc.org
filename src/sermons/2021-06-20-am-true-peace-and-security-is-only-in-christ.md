---
id: 2021-06-20-am
title: True Peace and Security is Only in Christ
date: 2021-06-20
text: 1 Thessalonians 5:1–3
preacher: josh-sheldon
series: awaiting-christ
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2021/210620-TruePeaceAndSecurityIsOnlyInChrist.aac
audioBytes: 53038892
youtube: ImLVbuKWFd4
---
