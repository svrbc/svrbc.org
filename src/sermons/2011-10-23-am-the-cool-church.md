---
id: 2011-10-23-am
title: The Cool Church
date: 2011-10-23
text: Revelation 3:14–22
preacher: wally-nakamura
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2011/2011-10-23+The+Cool+Church.mp3
audioBytes: 46870854
---
