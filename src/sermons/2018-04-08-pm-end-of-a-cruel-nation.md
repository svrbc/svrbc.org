---
id: 2018-04-08-pm
title: End Of A Cruel Nation
date: 2018-04-08
text: Nahum 3
preacher: josh-sheldon
series: minor-prophets
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2018/180408-PM-EndOfACruelNation.mp3
audioBytes: 28744034
youtube: H5T2LTNu5dc
---
