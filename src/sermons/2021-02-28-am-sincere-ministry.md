---
id: 2021-02-28-am
title: Sincere Ministry
date: 2021-02-28
text: 1 Thessalonians 2:9–12
preacher: conley-owens
series: awaiting-christ
audio: https://storage.googleapis.com/pbc-ca-sermons/2021/210228-SincereMinistry.mp3
audioBytes: 65092589
youtube: 9wDYtMCUwIM
---
