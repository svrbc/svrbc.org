---
id: 2016-04-17-am
title: 'Christ, Tabernacling Among Us'
date: 2016-04-17
text: Exodus 33:1–11
preacher: eric-hollingsworth
audio: >-
  https://storage.googleapis.com/pbc-ca-sermons/2016/160417+Christ+Tabernacling+Among+Us.mp3
audioBytes: 42660373
youtube: Rxk7-m9dKoQ
---
