/* eslint-env jest */
import data from './members-meetings.yml'

describe('first-saturdays.yml', () => {
  it('Dates in order', () => {
    const s = data.schedule
    s.forEach((v, i) => {
      if (!i) return
      expect(s[i - 1].date < v.date).toBeTrue()
    })
  })
})
