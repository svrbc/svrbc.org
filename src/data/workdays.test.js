/* eslint-env jest */
import { formatLADate, readLADate } from '../lib/datetime'
import data from './workdays.yml'

describe('first-saturdays.yml', () => {
  it('Dates in order', () => {
    const s = data.schedule
    s.forEach((v, i) => {
      if (!i) return
      expect(s[i - 1].date < v.date).toBeTrue()
    })
  })

  it('Dates all Saturdays', () => {
    data.schedule.forEach((v, i) => {
      expect(formatLADate(readLADate(v.date), 'c')).toEqual('6')
    })
  })
})
