---
title: Workdays
image: ../../banners/hose.jpg
---

import workdays from '~/data/workdays.yml'
import Schedule from '~/components/Schedule'

On the first Saturday of most months, we gather for a workday.  These run from
[9:30am–12:00pm]{.meridiem}.

On workdays, we take care of the property God has generously given us (1 Peter
4:10, Ephesians 4:28).  These are open to the public and we hope you join us!
We have tasks available for all ages and abilities.  Stay for lunch afterward
at noon!

<schedule :dates="workdays.schedule" />

<div class="wrap-list full-width">
  <g-image
    src="./chair.jpg"
    alt="a man washing a chair"
  />
  <g-image
    src="./walldemo.jpg"
    alt="men removing a wall"
  />
  <g-image
    src="./garden.jpg"
    alt="a woman with a shovel"
  />
  <g-image
    src="./bag.jpg"
    alt="a man holding a bag"
  />
  <g-image
    src="./wood.jpg"
    alt="a man with planks of wood"
  />
</div>

<style scoped lang="scss">
.full-width {
  position: relative;
  width: var(--fdw);
  left: calc(-0.5 * (var(--fdw) - 100%));
  //
  img {
    height: 25rem;
    width: auto;
    object-fit: cover;
  }
}
</style>
