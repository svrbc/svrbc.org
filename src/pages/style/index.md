---
title: Style Guide
---

Ever been to a party and realized you've forgotten someone's name, dreading the
embarrassment of having to admit your lapse of memory?  Ever been rescued by a
name tag?

As we create digital and print material for SVRBC—to advertise events,
evangelize, etc.—it is important to use a consistent style, not only to
maintain a respectable appearance, but to establish a visual identity that
helps others remember their interactions with us.  Think of it as a hospitality
measure.

The following style guide should be used in all SVRBC publications.

## Logo {#logo}

<g-image src="./logo-tech.png"
  class="content-float-narrow"
  alt="logo dimensions in a portrait layout"/>

The _silicon cross_ logo attempts to identify our church as united to the
global body of Christ with a common gospel, yet distinguished from other
assemblies through our particular mission field.  The cross is easily the most
widely-recognized symbol of Christianity and indicates our gospel-centric
outlook.  The segmented design of the cross alludes to a [printed circuit board
(PCB)](https://en.wikipedia.org/wiki/Printed_circuit_board), a product nearly
synonymous with Silicon Valley.

The logo strikes a harmony between several polar attributes.  It is simple, yet
distinguished.  It is decorous, yet light-hearted.  It is overt in its
testimony to the death and resurrection of Christ, yet subtle in its reference
to the boundaries God has sovereignly placed us in (Ps. 16:6).

Any church's mission should be primarily to proclaim the gospel in their
particular region, and ours is no different.  May the gospel of the cross of
Christ reach all those who find themselves in proximity of Silicon Valley.

**Guidelines**

-   **Do not omit text:** The silicon cross logo should not be used without the
    full SVRBC name (Silicon Valley Reformed Baptist Church), with the exception
    of contexts that could not tolerate text (e.g., in a favicon).
-   **Favor portrait layouts:** While some contexts may demand landscape layout,
    favor a portrait layout for consistency.  Additionally, the portrait layout
    more prominently separates the logo from the name, providing a less noisy
    presentation.
-   **Favor grayscale colors:** Only in contexts where color is demanded should
    the silicon cross logo be while the logo may be used in a non-grayscale
    color.
-   **Favor mild contrast:** In HSL lightness, the cross should be offset by
    roughly 180 units from the HSL lightness of the background.  In contexts
    where grayscale values cannot be used, white or black is acceptable,
    whichever comes closest to the preceding rule.

<table class="logo-chart">
  <tr>
    <th style="padding: 0 1rem;">transparent</th>
    <th style="padding: 0 1rem;">white</th
  </tr>
  <tr>
    <td>
      <a href="/logo-transparent.svg">svg</a> /
      <a href="/logo-transparent.png">png</a>
    </td>
    <td>
      <a href="/logo-white.svg">svg</a> /
      <a href="/logo-white.png">png</a>
    </td>
  </tr>
</table>

## Colors {#colors}

<table class="palette">
  <tr>
    <td>
      <b>Yellow:</b> Yellow should only be used on the PCB Cross logo.
    </td>
    <td class="color"
      style="background-color: #fffe9d;"
      v-clipboard:copy="link"
      v-clipboard:success="copyNotify"
      >#fffe9d</td>
  </tr>
  <tr>
    <td>
      <b>Blues:</b> Dark blue should only be used as an occasional background
      color.  Blue highlights should only be used as decorations on blue
      backgrounds.
    </td>
    <td class="color"
      style="background-color: #24406c;"
      v-clipboard:copy="link"
      v-clipboard:success="copyNotify"
      >#24406c</td>
    <td class="color"
      style="background-color: #2f568d;"
      v-clipboard:copy="link"
      v-clipboard:success="copyNotify"
      >#2f568d</td>
    <td class="color"
      style="background-color: #82a5d6;"
      v-clipboard:copy="link"
      v-clipboard:success="copyNotify"
      >#82a5d6</td>
  </tr>
  <tr>
    <td>
      <b>Browns:</b> Browns should be used for decorations, liberally.
    </td>
    <td class="color"
      style="background-color: #5d3d33;"
      v-clipboard:copy="link"
      v-clipboard:success="copyNotify"
      >#5d3d33</td>
    <td class="color"
      style="background-color: #794f43;"
      v-clipboard:copy="link"
      v-clipboard:success="copyNotify"
      >#794f43</td>
    <td class="color"
      style="background-color: #c49f94;"
      v-clipboard:copy="link"
      v-clipboard:success="copyNotify"
      >#c49f94</td>
  </tr>
  <tr>
    <td>
      <b>Plums:</b> Plums should be used for decorations, sparingly.
    </td>
    <td class="color"
      style="background-color: #5d3353;"
      v-clipboard:copy="link"
      v-clipboard:success="copyNotify"
      >#5d3353</td>
    <td class="color"
      style="background-color: #79436d;"
      v-clipboard:copy="link"
      v-clipboard:success="copyNotify"
      >#79436d</td>
    <td class="color"
      style="background-color: #c494b9;"
      v-clipboard:copy="link"
      v-clipboard:success="copyNotify"
      >#c494b9</td>
  </tr>
  <tr>
    <td>
      <b>Grays:</b> Grays should be used for text and borders.  Dark gray
      should be used on light backgrounds, and light gray on dark
      backgrounds.  The middle two grays should be used as highlights for
      the extremes.  Avoid using solid black or white in digital media.
      The two extremes may also be used as background colors.
    </td>
    <td class="color"
      style="background-color: #222; color: #5e5e5e;"
      @click="copy"
      >#222222</td>
    <td class="color"
      style="background-color: #5e5e5e;"
      @click="copy"
      >#5e5e5e</td>
    <td class="color"
      style="background-color: #acacac;"
      @click="copy"
      nospell
      >#acacac</td>
    <td class="color"
      style="background-color: #f1f1f1;"
      @click="copy"
      >#f1f1f1</td>
  </tr>
</table>

<!-- TODO: Update colors to match lightness rules -->

## Fonts {#fonts}

Headings and the text of the SVRBC logo should use
[Bellefair](https://fonts.google.com/specimen/Bellefair)
always with a regular weight (never bold).  All other text should use
[Roboto Slab](https://fonts.google.com/specimen/Roboto+Slab).

Font sizes should be consecutive integer multipliers, with the base font size
defaulting to 12pt.  Intermediate sizes that evenly interpolate this pattern
may be used.  For example,

<table class="size-chart">
 <tr>
   <th></th>
   <th>abstract</th>
   <th>example</th>
 </tr>
 <tr>
   <th class="hleft">normal text</th>
   <td>1 unit</td>
   <td>12pt</td>
 </tr>
 <tr>
   <th class="hleft">large text</th>
   <td>1.5 units</td>
   <td>18pt</td>
 </tr>
 <tr>
   <th class="hleft">subheading text</th>
   <td>2 units</td>
   <td>24pt</td>
 </tr>
 <tr>
   <th class="hleft">title text</th>
   <td>3 units</td>
   <td>36pt</td>
 </tr>
 </tr>
</table>

While more than three sizes may be used in artistic presentations, prose should
not have more than first level subheadings.

Text should use a generous line height of 1.8.

_Example:_

<div class="onblue" style="padding: 10pt; border-radius: 10px;">
  <p style="font-family: 'Bellefair', fantasy; font-size: 36pt; margin: 0;">
    Be anxious for nothing
  </p>

  <p style="font-family: 'Roboto Slab', serif; font-size: 12pt; line-height: 1.8;">
    <b>Matthew 6:25-33</b> Therefore I tell you, do not be anxious about
    your life, what you will eat or what you will drink, nor about your
    body, what you will put on. Is not life more than food, and the body
    more than clothing?  <sup>26</sup>Look at the birds of the air: they
    neither sow nor reap nor gather into barns, and yet your heavenly
    Father feeds them.  Are you not of more value than they?
    <sup>27</sup>And which of you by being anxious can add a single hour to
    his span of life?  <sup>28</sup>And why are you anxious about clothing?
    Consider the lilies of the field, how they grow: they neither toil nor
    spin, <sup>29</sup>yet I tell you, even Solomon in all his glory was
    not arrayed like one of these.  <sup>30</sup>But if God so clothes the
    grass of the field, which today is alive and tomorrow is thrown into
    the oven, will he not much more clothe you, O you of little faith?
    <sup>31</sup>Therefore do not be anxious, saying, ‘What shall we eat?’
    or ‘What shall we drink?’ or ‘What shall we wear?’ <sup>32</sup>For the
    Gentiles seek after all these things, and your heavenly Father knows
    that you need them all.  <sup>33</sup>But seek first the kingdom of God
    and his righteousness, and all these things will be added to you.
  </p>
</div>

<style lang="scss" scoped>
table.logo-chart {
  margin: 0 auto;
}
//
table.logo-chart td {
  text-align: center;
}
//
table.palette {
  border-collapse: collapse;
}
//
table.palette td {
  padding: 10px;
}
//
td.color {
  width: 100px;
  height: 100px;
  text-align: center;
  font-size: var(--large-font-size);
  cursor: pointer;
  border: none;
}
//
table.size-chart {
  margin: 0 auto;
}
//
table.size-chart th.hleft {
  text-align: left;
  padding-left: 0;
}
//
table.size-chart td {
  padding: 0 2rem;
}
//
.onblue {
  background-color: var(--dark-blue);
  color: var(--lightest-gray);
}
</style>

<script>
import CopyNotify from '~/mixins/copy-notify'
//
export default {
  mixins: [
    CopyNotify
  ],
  methods: {
    link (e) {
      return e.target.innerHTML
    }
  }
}
</script>

<spell-ignore>
acacac
</spell-ignore>
