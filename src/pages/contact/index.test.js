/* eslint-env jest, browser */
/* global page */
import { describe, visit } from '../../lib/pageutils'

// BEGIN tests ////////////////////////////////////////////////////////////////

describe('Contact page', () => {
  let resp

  beforeAll(async () => {
    resp = await visit('/contact/?debug')
  }, 30000)

  it('200s', async () => {
    expect(resp.status()).toBeOneOf([200, 304])
    expect(resp.errs).toEqual([])
    expect(resp.pageErrs).toEqual([])
  })

  describe('form', () => {
    let form

    beforeAll(async () => {
      form = await expect(page).toMatchElement('form')
    })

    it('reCAPTCHA notice displayed', async () => {
      await expect(form).toMatch('This site is protected by reCAPTCHA')
    })

    it('Message mailed', async () => {
      await expect(page).toFillForm('form', {
        name: 'Person McPerson',
        email: 'person@example.com',
        message: 'Have you heard the word about the bird?'
      })
      await expect(form).toClick('.dark-button')
      const debugEl = await expect(form).toMatchElement('.mail-debug', {
        timeout: 10000
      })

      const debugJson = await debugEl.evaluate(el => el.dataset.obj)
      const debug = JSON.parse(debugJson)
      expect(debug.data.replace(/\r/g, '')).toEqual(`--
Content-Disposition: form-data; name="from"

Person McPerson (person@example.com) via SVRBC.org <webmail@svrbc.org>
--
Content-Disposition: form-data; name="h:Reply-To"

Person McPerson <person@example.com>
--
Content-Disposition: form-data; name="to"

SVRBC Officers <info@svrbc.org>
--
Content-Disposition: form-data; name="subject"

Message from svrbc.org/contact (person@example.com)
--
Content-Disposition: form-data; name="html"

Have you heard the word about the bird?
----
`)
    }, 15000)
  })
})
