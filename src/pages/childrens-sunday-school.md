---
title: Children’s Sunday School
---

Starting at 9:30, there is an hour-long children's Sunday School, for ages 3
to 9.  In the past, the children’s Sunday school has gone through Pilgrim’s
Progress, and are currently in Genesis.
