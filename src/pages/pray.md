---
title: Prayer
image: ../banners/green-path.jpg
---

## Psalms of the day {#psalms}

Choose a Psalm (or multiple).  Read a few verses and then pray as led.

<prooftext v-if="day" :verses="psalms" notoggle />

<script>
import Prooftext from '~/components/Prooftext.vue'
//
export default {
  components: {
    Prooftext
  },
  data () {
    return {
      day: 0
    }
  },
  mounted() {
    this.day = new Date().getDate()
  },
  computed: {
    psalms () {
      if (this.day === 0) return
      const chapters = []
      for (let d = this.day; d <= 150; d += 30) {
        chapters.push(d)
      }
      return `Psalms ${chapters.join('; ')}`
    }
  }
}
</script>
