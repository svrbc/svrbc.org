---
title: History
---

This page is a placeholder, please ping Pam Wells about church history.

## Early years {#early}

How fitting that the planting of a new church would be done on the very site of
one of the many orchards that spanned what's now referred to as the Silicon
Valley.  Orchards that were pruned to the point of removal, in order to make
way for “God's vineyard”.

In the summer of 1960, the foundation was poured on the site of a missionary
church started by the First Baptist Church of San Jose. In its early history,
Lakewood Village Baptist Church was affiliated with the General Association of
Regular Baptist Churches (GARBC). The old building, whose footprint remains
today, held approximately 120 people.

<!-- Add picture -->

By the late 1960's and early 1970's the church became wholly independent of
denominational ties, retaining the name “Baptist” for is doctrinal and
historical distinctives rather than mere denominational affiliation. Several
outstanding men served as pastor of this assembly during those years, including
Pastor George Hare, Pastor Tim Sherman and Pastor David Bell. During the tenure
of Pastor Sherman, the church underwent a doctrinal reformation and clearly
aligned itself with the Biblical and historical faith of our early Baptist
forefathers. For the next 21 years Pastor W.R. Downing served as pastor as the
church solidified its identity as a Reformed Baptist church.

## 80's and 90's {#90s}

In the mid 1980’s, the deacons began to consider enlarging the facilities as
the size of the congregation increased to capacity. After eight years of much
prayer, planning, hard work, and much help, the new addition was finally
completed. On Sunday February 6, 1994, we celebrated this blessing with an Open
House.

The celebration was short lived as a few months later, another pruning took
place as the church faced a heartbreaking split. The Lord, in His infinite
wisdom, showed His church, enormous grace and mercy by providing men to stand
up and preach His word each Sunday. After a lengthy nationwide search, we
welcomed Pastor Donald Michael in 1996. Pastor Michael moved his family from
Grand Rapids, Michigan and served as the under-shepherd of God’s flock for
almost two years. Pastor Michael ended up moving back to Michigan and God
raised up a man from our midst.  George Stevenson became Pastor in 1998.

## Early 2000s {#2000s}

Our current Pastor Joshua Sheldon has served since December of 2004. Under his
shepherding, the church became associated with the Fellowship of Independent
Reformed Evangelicals (FIRE).

Through God’s providence the small assembly in Sunnyvale continued, and in
2008, the church, grateful for God's provision, changed its name to Providence
Bible Church of Sunnyvale.

## Current {#current}

After years of prayer for a plurality of elders, in 2019 the church voted
Conley Owens into the office of Pastor.

Staring in 2020, the name of this church has been Silicon Valley Reformed
Baptist Church.

## Contact {#contact}

<ContactForm
  to="info@svrbc.org"
  subject="message from history page on svrbc.org"
/>

<script>
import ContactForm from '~/components/ContactForm.vue'
//
export default {
  components: {
    ContactForm
  }
}
</script>
