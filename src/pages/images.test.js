/* eslint-env jest */
import path from 'path'

import srcImages from '../lib/src-images'

const ROOT = path.join(__dirname, '**')

async function scan (glob) {
  return srcImages.scan(path.join(ROOT, glob))
}

// BEGIN tests ////////////////////////////////////////////////////////////////

describe('testimonial images', () => {
  let images

  beforeAll(async () => {
    images = await scan('*.testimonial.jpg')
  })

  it('under 100k', () => {
    for (const i of images) {
      const msg = `${i.basename} should be <100kb`
      expect(i.bytes, msg).toBeLessThan(100000)
    }
  })

  it('500x500', () => {
    for (const i of images) {
      const msg = `${i.basename} should be 500x500`
      expect(i.dims.width, msg).toEqual(500)
      expect(i.dims.height, msg).toEqual(500)
    }
  })
})
