---
title: Copyright Information
---

[![Creative Commons 0 logo](./cc-zero.png){class="cc0"}](https://creativecommons.org/publicdomain/zero/1.0/){class="content-float-right no-ext-warn"}
To the extent possible under law, Silicon Valley Reformed Baptist Church has
waived all copyright and related or neighboring rights to svrbc.org. This work
is published from: United States.

* * *

Scripture quotations marked “ESV” are from the ESV® Bible (The Holy Bible,
English Standard Version®), copyright © 2001 by Crossway, a publishing ministry
of Good News Publishers. Used by permission. All rights reserved.

<style lang="scss" scoped>
.cc0 {
  height: 3rem;
  width: auto;
}
</style>
