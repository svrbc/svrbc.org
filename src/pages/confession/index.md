---
title: The Second London Baptist Confession
subtitle: Also known as the Baptist Confession of 1689
image: ../../banners/open-book.jpg
nospell: true
---

import StandardView from '~/components/StandardView'

While creeds (like
[the Nicene creed](https://rts.edu/historical-creeds-of-faith/the-nicene-creed/),
for example) set forward something minimal that every Christian must believe, a
confession offers an expansive statement of faith that guides a church.  In
today’s world, it is a common practice to reduce a church’s beliefs to a
handful of bullet points, but we have adopted the Second London Baptist
Confession in order to:

1.  offer a public transparency about our beliefs,
2.  ensure we benefit from the wisdom of previous generations of Christ’s
    church, and
3.  establish boundaries for the teaching ministry of the church.

This confession is uninspired and will always be subject to criticism through
the lens of Scripture.  Moreover, complete agreement with this document is not
a requirement for membership at SVRBC.  Regardless, it is a priceless tool that
offers an exceptional clarity of doctrine to all who desire to know what the
Bible teaches.

<!-- TODO: Add a nicene creed page -->

<!-- TODO: Link back to the beliefs page -->

<!-- TODO: Limit nospell to relevant sections -->

<!-- TODO: Keep expandable sections from flashing content -->

<!-- TODO: Advertise the orientation class -->

<!-- TODO: Remove script tag at bottom -->

## Charles Spurgeon’s Introduction {#spurgeon expandable}

![Charles Spurgeon](./spurgeon.jpg){.content-narrow .content-float-right}

Dearly-beloved,

This ancient document is a most excellent epitome of the things most surely
believed among us.  By the preserving hand of the Triune Jehovah, we have been
kept faithful to the great points of our glorious gospel, and we feel more
resolved perpetually to abide by them.

This little volume is not issued as an authoritative rule, or code of faith,
whereby you are to be fettered, but as an assistance to you in controversy, a
confirmation in faith, and a means of edification in righteousness.  Here, the
younger members of our church will have a Body of Divinity in small compass,
and by means of the Scriptural proofs, will be ready to give a reason for the
hope that is in them.

Be not ashamed of your faith; remember it is the ancient gospel of martyrs,
confessors, Reformers, and saints.  Above all, it is the truth of God, against
which the gates of hell cannot prevail.

Let your lives adorn your faith, let your example recommend your creed.  Above
all, live in Christ Jesus, and walk in Him, giving credence to no teaching but
that which is manifestly approved of Him, and owned by the Holy Spirit.  Cleave
fast to the Word of God, which is here mapped out to you. May our Father, who
is in Heaven, smile on us as ever! Brethren, pray for—

Your affectionate Minister,<br>
C. H. Spurgeon

<!-- TODO: Add picture of Spurgeon -->

<!-- TODO: Set a different image -->

<!--testimonial name="Charles Spurgeon">
  <template slot="image">
    <g-image src="./spurgeon.jpg"
      alt="Portrait of Charles Spurgeon" />
  </template>
  This little volume is not issued as an authoritative rule, or code of faith,
  whereby you are to be fettered, but as an assistance to you in controversy, a
  confirmation in faith, and a means of edification in righteousness.  Here the
  younger members of our church will have a body of divinity in small compass,
  and by means of the scriptural proofs, will be ready to give a reason for the
  hope that is in them.
</testimonial-->

## Original Preface {#preface expandable}

Courteous Reader,

It is now many years since divers of us (with other sober Christians then
living and walking in the way of the Lord that we professe) did conceive our
selves to be under a necessity of Publishing a Confession of our Faith, for the
information, and satisfaction of those, that did not throughly understand what
our principles were, or had entertained prejudices against our Profession, by
reason of the strange representation of them, by some men of note, who had
taken very wrong measures, and accordingly led others into misapprehensions, of
us, and them: and this was first put forth about the year, 1643. in the name of
seven Congregations then gathered in London; since which time, diverse
impressions thereof have been dispersed abroad, and our end proposed, in good
measure answered, inasmuch as many (and some of those men eminent, both for
piety and learning) were thereby satisfied, that we were no way guilty of those
Heterodoxies and fundamental errors, which had too frequently been charged upon
us without ground, or occasion given on our part. And forasmuch, as that
Confession is not now commonly to be had; and also that many others have since
embraced the same truth which is owned therein; it was judged necessary by us
to joyn together in giving a testimony to the world; of our firm adhering to
those wholesome Principles, by the publication of this which is now in your
hand.

And forasmuch as our method, and manner of expressing our sentiments, in this,
doth vary from the former (although the substance of the matter is the same) we
shall freely impart to you the reason and occasion thereof. One thing that
greatly prevailed with us to undertake this work, was (not only to give a full
account of our selves, to those Christians that differ from us about the
subject of Baptism, but also) the profit that might from thence arise, unto
those that have any account of our labors, in their instruction, and
establishment in the great truths of the Gospel; in the clear understanding,
and steady belief of which, our comfortable walking with God, and fruitfulness
before him, in all our ways, is most neerly concerned; and therefore we did
conclude it necessary to expresse our selves the more fully, and distinctly;
and also to fix on such a method as might be most comprehensive of those things
which we designed to explain our sense, and belief of; and finding no defect,
in this regard, in that fixed on by the assembly, and after them by those of
the Congregational way, we did readily conclude it best to retain the same
order in our present confession: and also, when we observed that those last
mentioned, did in their confession (for reasons which seemed of weight both to
themselves and others) choose not only to express their mind in words
concurrent with the former in sense, concerning all those articles wherein they
were agreed, but also for the most part without any variation of the terms we
did in like manner conclude it best to follow their example in making use of
the very same words with them both, in these articles (which are very many)
wherein our faith and doctrine is the same with theirs, and this we did, the
more abundantly, to manifest our consent with both, in all the fundamental
articles of the Christian Religion, as also with many others, whose orthodox
confessions have been published to the world; on behalf of the Protestants in
divers Nations and Cities: and also to convince all, that we have no itch to
clogge Religion with new words, but do readily acquiesce in that form of sound
words, which hath been, in consent with the holy Scriptures, used by others
before us; hereby declaring before God, Angels, & Men, our hearty agreement
with them, in that wholesome Protestant Doctrine, which with so clear evidence
of Scriptures they have asserted: some things indeed, are in some places added,
some terms omitted, and some few changed, but these alterations are of that
nature, as that we need not doubt, any charge or suspition of unsoundness in
the faith, from any of our brethren upon the account of them.

In those things wherein we differ from others, we have exprest our selves with
all candor and plainness that none might entertain jealousie of ought secretly
lodged in our breasts, that we would not the world should be acquainted with;
yet we hope we have also observed those rules of modesty, and humility, as will
render our freedom in this respect inoffensive, even to those whose sentiments
are different from ours.

We have also taken care to affix texts of Scripture, in the margin for the
confirmation of each article in our confession; in which work we have
studiously indeavoured to select such as are most clear and pertinent, for the
proof of what is asserted by us: and our earnest desire is, that all into whose
hands this may come, would follow that (never enough commended) example of the
noble Bereans, who searched the Scriptures daily, that they might find out
whether the things preached to them were so or not.

There is one thing more which we sincerely professe, and earnestly desire
credence in, viz. That contention is most remote from our design in all that we
have done in this matter: and we hope the liberty of an ingenuous unfolding our
principles, and opening our hearts unto our Brethren, with the Scripture
grounds on which our faith and practise leanes, will by none of them be either
denyed to us, or taken ill from us. Our whole design is accomplished, if we may
obtain that Justice, as to be measured in our principles, and practise, and the
judgement of both by others, according to what we have now published; which the
Lord (whose eyes are as a flame of fire) knoweth to be the doctrine, which with
our hearts we must firmly believe, and sincerely indeavour to conform our lives
to. And oh that other contentions being laid asleep, the only care and
contention of all upon whom the name of our blessed Redeemer is called, might
for the future be, to walk humbly with their God, and in the exercise of all
Love and Meekness towards each other, to perfect holyness in the fear of the
Lord, each one endeavouring to have his conversation such as becometh the
Gospel; and also suitable to his place and capacity vigorously to promote in
others the practice of true Religion and undefiled in the sight of God and our
Father. And that in this backsliding day, we might not spend our breath in
fruitless complaints of the evils of others; but may every one begin at home,
to reform in the first place our own hearts, and wayes; and then to quicken all
that we may have influence upon, to the same work; that if the will of God were
so, none might deceive themselves, by resting in, and trusting to, a form of
Godliness, without the power of it, and inward experience of the efficacy of
those truths that are professed by them.

And verily there is one spring and cause of the decay of Religion in our day,
which we cannot but touch upon, and earnestly urge a redresse of; and that is
the neglect of the worship of God in Families, by those to whom the charge and
conduct of them is committed. May not the grosse ignorance, and instability of
many; with the prophaneness of others, be justly charged upon their Parents and
Masters; who have not trained them up in the way wherein they ought to walk
when they were young? but have neglected those frequent and solemn commands
which the Lord hath laid upon them so to catechize, and instruct them, that
their tender years might be seasoned with the knowledge of the truth of God as
revealed in the Scriptures; and also by their own omission of Prayer, and other
duties of Religion in their families, together with the ill example of their
loose conversation, have inured them first to a neglect, and then contempt of
all Piety and Religion? we know this will not excuse the blindness, or
wickedness of any; but certainly it will fall heavy upon those that have thus
been the occasion thereof; they indeed dye in their sins; but will not their
blood be required of those under whose care they were, who yet permitted them
to go on without warning, yea led them into the paths of destruction? and will
not the diligence of Christians with respect to the discharge of these duties,
in ages past, rise up in judgment against, and condemn many of those who would
be esteemed such now?

We shall conclude with our earnest prayer, that the God of all grace, will pour
out those measures of his holy Spirit upon us, that the profession of truth may
be accompanyed with the sound belief, and diligent practise of it by us; that
his name may in all things be glorified, through Jesus Christ our Lord, Amen.

<section v-for="(c, i) in $static.chapters.edges" :key="i">
  <subheading :id="String(c.node.id)" v-if="c.node.title">
    {{ c.node.id }}. {{ c.node.title }}
  </subheading>
  <template v-for="p in c.node.paragraphs.edges">
    <hr v-if="p.node.paragraph > 1">
    <standard-view
      docname="The Second London Baptist Confession of Faith"
      :obj="p.node"
      :link="`${$route.path}/${p.node.id}/`"
      head
      :id="p.node.id"
    />
  </template>
</section>

<static-query>
query {
  chapters: allConfessionChapter(sort: {by: "id", order: ASC}) {
    edges {
      node {
        id
        title
        num_paragraphs
        paragraphs: belongsTo(
          filter: {typeName: {eq: ConfessionParagraph}},
          sort: {by: "paragraph", order: ASC}
        ) {
          edges {
            node {
              ... on ConfessionParagraph {
                id
                paragraph
                segments {
                  text
                  verses
                }
              }
            }
          }
        }
      }
    }
  }
}
</static-query>
