---
title: Meet our leaders
image: ../../banners/green-path.jpg
---

import Leader from '~/components/Leader.vue'
import LeadershipForm from '~/forms/LeadershipForm.vue'

SVRBC is a congregational church, led by elders (also known as pastors) and
deacons.  While these are the only two offices the Bible describes, we
believe that each member of the church bears the responsibility to use their
talents and resources to assist the church, often in leadership capacities.

## Pastors {#pastors}

<div class="leader-container">
  <!-- FUTURE: Add when others also have social links
       facebook="xcco3x"
       instagram="_cco3_"
       linkedin="conley-owens-17908928"
  -->
  <leader
    name="Conley Owens"
    :img="$static.conleyOwens.image"
    email="conley@svrbc.org"
    class="content-float-left clear"
  />
  <div v-html="$static.conleyOwensTxt.content"/>
</div>


## Lay leaders {#laymen}

<div class="lay-leaders wrap-list">
  <leader
    name="Dale Hardin"
    :img="$static.daleHardin.image"
    area="Treasury"
  />
  <leader
    name="James Torrefranca"
    :img="$static.jamesTorrefranca.image"
    area="Audio/Visual"
  />
</div>

## Get in touch with one of our leaders! {#contact}

<leadership-form />

<static-query>
query {
  conleyOwens: srcImage(id: "people/conley-owens.jpg") { image }
  conleyOwensTxt: srcText(id: "pages/leadership/conley") { content }
  daleHardin: srcImage(id: "people/dale-hardin.jpg") { image }
  jamesTorrefranca: srcImage(id: "people/james-torrefranca.jpg") { image }
}
</static-query>

<style lang="scss" scoped>
.leader-container {
  + .leader-container {
    margin-top: 3rem;
  }
  //
  &::after {
    content: "";
    clear: both;
    display: table;
  }
}
//
.lay-leaders {
  --flex-spacing: var(--double-inline-margin);
  //
  margin-top: 1rem;
  //
  > * {
    flex-grow: 0;
  }
}
</style>
