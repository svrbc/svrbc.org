Brian Garcia is originally from Connecticut and was converted in 2008 from a
Jehovah’s Witness family after studying the Bible for himself and seeing the
clear teaching of the divinity of Jesus Christ and salvation by grace through
faith. He has since done Christian ministry working with the homeless and
addict populations in Edmonton, Canada, where he also planted two churches. He
has served as a pastor in both Canada and Wisconsin and has now answered the
call to minister the gospel in the SF Bay Area. 

Brian is currently enrolled in a Master’s program from Redemption Seminary. He
has been married to his wife Tatiana for over 10 years and together they have 4
children. Brian has a passion for evangelism and reaching the lost with the
life saving message of the gospel of Jesus Christ. 
