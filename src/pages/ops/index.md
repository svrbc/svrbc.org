---
title: Operations Guidelines
---

-   [Recording](recording/)

-   [Verse formatter](verses/)

-   External tools
    -   [Firestore](https://console.cloud.google.com/firestore/data?project=svrbc-org)
    -   [Google business account permissions](https://myaccount.google.com/u/0/b/102580977710948084123/permissions)

<!-- -   [Small Group Leader Report Form](/smallgroups-report/) -->
