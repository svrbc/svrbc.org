---
title: Recording
---

## Uploading sermons to YouTube {#youtube}

Sermons uploaded to YouTube will be automatically pulled down and posted on the
website.  This job is labeled
[_Fetch new sermons_](https://gitlab.com/svrbc/svrbc.org/pipeline_schedules){data-allowstatus="503"},
and can be manually run in the event of a failure.  It runs at midnight every
Sunday.

Note:

-   This isn’t fully implemented yet
-   In the future we may run more frequently

In order for the sermon to be detected, it’s important that the description in
YouTube be formatted as follows:

    Sermon: <title>
    Date: <date>, [Morning|Afternoon]
    Text: <biblical text>
    Series: <series name>
    Preacher: <preacher>
    Audio: <link to mp3 file>

    <additional description>

For example:

    Sermon: A Biblical Argument Against S’mores
    Date: March 6, 2042, Afternoon
    Text: Job 6:6-7
    Series: Opinions and Eisegesis
    Preacher: Charles Spurgeon
    Audio: http://example.com/audio.mp3

    A surprising message!  Guest preacher Charles Spurgeon offers some insight
    into one of the most puzzling verses in the book of Job.

Notes:

-   Feel free to decorate the title of the actual video (i.e., not the one in
    in the description) with a reason to watch.  For example, in the example
    above, the title might be: _Something to share with your boy scout troop: A
    Biblical Argument Against S’mores_.
-   The _Series_ field is not required, but if it is not provided it will
    default to `Miscellaneous <current year>`.
-   The _additional description_ is optional.

## Manually trigger video sync {#sync}

This is only for testing purposes.

<form action="/.netlify/functions/onyoutubeupdate" method="POST">
  <label for="id">Video ID</label>
  <input type="text" name="id" />

  <label for="hmac">HMAC secret</label>
  <input type="text" name="hmac" />

  <input type="submit"/>
</form>

## Automated video normalization {#normalization}

NOTE: This is still in progress and does not fully work yet.

In order to normalize YouTube titles/descriptions, we need to authorize as the
SVRBC account.  Because this app is not verified for use by anyone with a
Google account, it will appear with a warning.  Click “advanced” and “go to
svrbc.org.”

<!-- Note: vue-remark will escape our ampersands -->

<a target="_blank" rel="noreferrer" :href="oauthURL">Authorize normalizer</a>

<script>
import oauth from '~/lib/oauth'
//
export default {
  data () {
    return {
      oauthURL: ''
    }
  },
  mounted () {
    this.oauthURL = oauth.getAuthURL(window.location.origin)
  }
}
</script>
