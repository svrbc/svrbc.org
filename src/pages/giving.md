---
title: Ways to Give
image: ../banners/gift.jpg
bannerstyle: dark
---

## Considering partnering with SVRBC? {#partner}

>  Each one must give as he has decided in his heart, not reluctantly or under
>  compulsion, for God loves a cheerful giver. And God is able to make all
>  grace abound to you, so that having all sufficiency in all things at all
>  times, you may abound in every good work.
>
> <p class="citation">2 Corinthians 9:7-8 (ESV)</p>

We are working to advance the gospel here in Silicon Valley.  We are excited
to share the gospel and serve our community!  If you would like to
contribute to our work, we would love to partner with you financially.

Your gifts help support our many ministries and outreaches: it provides for our
free counseling ministry, our community food distribution, materials for
children's and adults' Sunday school, and more!  

## Give Online {#online}

<a class="button"
href="#givingflow&fund=General+Church+Fund&interval=Every+1st+and+15th">Give</a>

## Give In-Person or By mail: {#mail}

Make checks payable to "Silicon Valley Reformed Baptist Church". You can give
in-person, or mail to

709 Lakewood Drive
Sunnyvale CA 94089

<style scoped>
.button {
  margin-top: 1rem;
}
</style>
