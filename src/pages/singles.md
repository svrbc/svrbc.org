---
title: Monthly Singles Dinner
image: ../banners/puffins.jpg
bannerstyle: light
---

import singles from '~/data/singles-dinners.yml'
import Schedule from '~/components/Schedule'

>  He who finds a wife finds a good thing and obtains favor from the LORD.
>
> <p class="citation">Proverbs 18:22 (ESV)</p>

Want to make meaningful connections with other marriage-minded Christian
singles?  SVRBC hosts free monthly singles' dinners!  Events include dinner,
a brief message, organized discussion, and free time.

[RSVP here](https://www.eventbrite.com/e/free-christian-singles-dinner-tickets-572042252777){.button}

During the winter months, events will be held at
First OPC Sunnyvale:<br>
1210 Brookfield Ave<br>
Sunnyvale, CA 94087

<schedule :dates="singles.schedule" />
