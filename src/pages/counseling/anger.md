---
title: Free Biblical Anger Counseling in Sunnyvale
image: ../../banners/broken-window.jpg
---

<covid-counsel-update />

Do you struggle with anger and impatience with those close to you?  Is your
harsh demeanor damaging relationships at work or at home?  Do you want to
learn to control your emotional outbursts, rather than letting them control
you?

God's word has all the answers, and our biblical counselors can guide you to
them.

[Sign up for counseling](../signup/){.button}

Not sure if you're ready?  [Learn more about counseling at SVRBC.](../)

<script>
import CovidCounselUpdate from '~/components/CovidCounselUpdate.vue'
//
export default {
  components: {
    CovidCounselUpdate
  }
}
</script>
