---
title: Free Biblical Marriage Counseling in Sunnyvale
image: ../../banners/couple-hands.jpg
---

<covid-counsel-update />

Biblical marriage is meant to be a picture of Christ and his church, full
of love, honor, and respect.  But too often, marriages can suffer from poor
communication, lack of connection, and little love.  What are couples to do
when they "fall out of love"?  God offers hope, even for couples who feel on
the brink of divorce.  Our experienced Christian counselors are ready and
eager to teach you biblical principles that can restore your marriage to a
God-honoring and love-filled relationship.

God's word has all the answers, and our biblical counselors can guide you to
them.

[Sign up for counseling](../signup/){.button}

Not sure if you're ready?  [Learn more about counseling at SVRBC.](../)

<script>
import CovidCounselUpdate from '~/components/CovidCounselUpdate.vue'
//
export default {
  components: {
    CovidCounselUpdate
  }
}
</script>
