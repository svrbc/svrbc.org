---
title: Free Biblical Alcohol Addiction Counseling in Sunnyvale
image: ../../banners/beer-bottles.jpg
---

<covid-counsel-update />

Do you have a problem that you've been hiding?  Do you feel like a slave to
your alcoholism, as it destroys your life and relationships?  Are you ready to
take back control of your life?

God's word has all the answers, and our biblical counselors can guide you to
them.

[Sign up for counseling](../signup/){.button}

Not sure if you're ready?  [Learn more about counseling at SVRBC.](../)

<script>
import CovidCounselUpdate from '~/components/CovidCounselUpdate.vue'
//
export default {
  components: {
    CovidCounselUpdate
  }
}
</script>
