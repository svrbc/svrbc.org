---
title: Partner with Us in Biblical Counseling
image: ../../banners/small-plant.jpg
bannerstyle: dark
---

Have you been blessed by [the counseling ministry at SVRBC](../)?  Maybe
someone you know is currently receiving counseling with us.  Perhaps you are
simply excited about the work that God is doing through this ministry.

If any of these are true of you, we’d love it if you would consider the ways
you can partner with us.

## Prayer {#prayer}

The most important way you can partner with us is through prayer.

First, you can pray for the counselors:

-   that they would have wisdom (James 1:5)
-   that they would would have the time and resources they need (Matthew 6:11)
-   that they would be encouraged by others (Romans 1:11–12)

> Pray for us, for we are sure that we have a clear conscience, desiring to act
> honorably in all things.
>
> [Hebrews 13:18 (ESV)]{.citation}

Second, you can pray for the counselees:

-   that they would resist temptation (Matthew 6:13)
-   that they would not grow weary (Hebrews 12:5)
-   that they would set their hope in Christ (1 Peter 1:13)

This is especially important if you know one of the counselees and are privy to
their struggles.

## Referrals {#referrals}

Naturally, you can help us by referring others to our ministry.  There is a
good chance you will encounter someone in the near future who could use the
help we offer.  There is no problem too big for the word of God,

## Training {#training}

If you would like to join us in the actual task of counseling, we’d love to
help you get started.  While many operate as lone rangers, Christ intended for
his servants to work together under the auspices of the local church.  We can
mentor you through the process of
[ACBC certification](https://biblicalcounseling.com/about/) and offer you
accountability and guidance on your counseling journey.  

## Giving {#giving}

We provide counseling free of charge because Christ has freely given us his
gospel (Matthew 10:8).  However, he loves it when his servants partner together
to bear financial burdens in ministry (Matthew 10:9-10).

The reality is that providing counseling requires financial sacrifice.  In
addition to the time the counselors themselves generously offer, SVRBC invests
significant funds in books and training for our counselors, so that they can
offer excellent wisdom to all the counselees they help.

> Yet it was kind of you to share my trouble.… Not that I seek the gift, but I
> seek the fruit that increases to your credit.
>
> [Philippians 4:14,17 (ESV)]{.citation}

If you serve the same God we do and want to see this ministry grow, we’d love
to partner with you financially.

[Give](#givingflow&fund=General+Church+Fund&interval=Every+1st+and+15th){.button}

## Coordinating pastoral care {#coordinate}

Many counselees at SVRBC attend other churches in the area.  If you are a
pastor of a counselee and you represent another area church, we ask that you
check in regularly with the counselor handling the case.  We do not intend to
replace your role as a shepherd, and we can be of most help when coordinating
with you.

## Get in touch {#contact}

Want to discuss the opportunities?<br>
Have some other idea of how you can partner with us?<br>
Send us a message; we’d love to hear from you!

<contact-form
  to="info"
  subject="Counseling partnership"
  persist-key="/counseling"
/>

<script>
import ContactForm from '~/components/ContactForm.vue'
//
export default {
  components: {
    ContactForm
  }
}
</script>
