---
title: Sign up for counseling
image: ../../banners/small-plant.jpg
bannerstyle: dark
---

<!-- TODO: privacy form -->

So you are considering [receiving counseling at SVRBC](/counseling/)… Great!
In order for us to give the best counsel we can beginning with the first
session, we’ll need some information from you.  Complete this form and we will
get back to you.

This form is confidential and the data you submit will not be shared.  See
[our privacy policy](../privacy/) for more information.


<span style="color: red; font-weight: bold;">
Please be advised that we have limited counseling resources and often are not
able to accept applications from those who live outside the area.</span>

If you are not
able to visit SVRBC for in-person counseling, we recommend finding counseling
through the [ACBC website](http://biblicalcounseling.com/find-a-counselor/).

<data-form :template="template">
</data-form>

<script>
import DataForm from '~/components/DataForm'
import template from '~/data/forms/counseling-application.yml'
//
export default {
  components: {
    DataForm
  },
  data() {
    return {
      template
    }
  }
}
</script>
