---
title: Fellowship Groups
image: ../../banners/men-bench.jpg
---

import DataForm from '~/components/DataForm'
import MidwayCta from '~/components/MidwayCta'

We spend Sunday together as a church, but Christians ought to spend time with
each other throughout the week, encouraging and praying for each other.
Joining a fellowship group is a great way to make that happen!

> But exhort one another every day, as long as it is called “today,” that none
> of you may be hardened by the deceitfulness of sin.
>
> [Hebrews 3:13 (ESV)]{.citation}

## Men's group {#mens-group}

Join us every first Friday, as the men get together to eat dinner and study
the Proverbs.  Meeting location varies, contact Albert for more information.

<midway-cta>
  <data-form id="mensgroupform" data-button-text="Get more info" :template="mensgroupSignup" />
</midway-cta>


## Women's group {#womens-group}

Join our women every second Friday, as we gather for prayer and fellowship.
Meeting location varies, contact Sarah for more information.

<midway-cta>
  <data-form id="womensgroupform" data-button-text="Get more info" :template="womensgroupSignup" />
</midway-cta>
  


<script>
import mensgroupSignup from '~/data/forms/mensgroup-signup.yml'
import womensgroupSignup from '~/data/forms/womensgroup-signup.yml'
//
export default {
  data() {
    return {
      mensgroupSignup,
      womensgroupSignup
    }
  }
}
</script>
