module.exports = {
  use: '@gridsome/source-filesystem',
  options: {
    baseDir: './src',
    path: 'preachers/*.md',
    typeName: 'Preacher'
  }
}
