import dataformat from '../../lib/dataformat'
import { slugify } from '../lib/datautils'

export default {
  type: 'Preacher',
  subdir: 'preachers',
  query: `node {
  id
  fileInfo {
    path
  }
  lastname
  title
}`,
  normalizeFilename: n => `${slugify(n.title)}.md`,
  normalizeContent: n => {
    return dataformat.formatMd(n, [
      'id',
      'title',
      'lastname'
    ], {
      noquote: []
    })
  }
}
