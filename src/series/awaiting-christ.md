---
id: awaiting-christ
title: Awaiting Christ
text: 1–2 Thessalonians
preacher: Pastor Josh Sheldon
image: ../banners/cow-sun.jpg
---

Life is hard, especially for one trying to serve God amid persecution.  How
are we to endure?  Looking forward to Jesus’s return and the resurrection of
the dead is the mother of Christian patience.  In 1 & 2 Thessalonians, Paul
encourages the church in Thessalonica by setting their minds on these
matters.
