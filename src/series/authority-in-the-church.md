---
id: authority-in-the-church
title: Authority in the Church
text: Matthew 28:18; 20:18; Hebrews 13:17; Acts 6:3
preacher: Pastor Josh Sheldon, Deacon Conley Owens
image: ../banners/keys.jpg
bannerstyle: dark
---
