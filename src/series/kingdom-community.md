---
id: kingdom-community
title: Kingdom Community
text: 1 Corinthians 1–2
preacher: The Pastors of SVRBC
image: ../banners/church-door.jpg
---

It’s the year 2020.  Current events have a way of dividing people like no time
in recent memory, and many churches have felt a strain on the bonds they share
in Christ.  God has been gentle to SVRBC in this regard, but given the reality
of spiritual warfare, we must pursue prepare ourselves for the danger all
around out us.

This look at 1 Corinthians 1-2 examines Paul’s concern for unity in the
Corinthian church and the solution for their division: the mind of Christ.
