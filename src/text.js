module.exports = {
  use: '@gridsome/source-filesystem',
  options: {
    baseDir: './src',
    path: [
      '**/*.text.md'
    ],
    typeName: 'SrcText'
  }
}
