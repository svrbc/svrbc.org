/* eslint-env jest */
import delay from 'delay'

import Timer from './timer'

describe('Timer.runFor', () => {
  it('runfor expires', async () => {
    const timer = new Timer()
    timer.runFor(0)
    await delay(0)
    expect(timer.running).toEqual(false)
    timer.stop()
  })

  it('runfor cancels', async () => {
    const timer = new Timer()
    timer.runFor(0)
    timer.runFor(10000)
    await delay(0)
    expect(timer.running).toEqual(true)
    timer.stop()
  })
})
