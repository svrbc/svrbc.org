/* eslint-env jest */
import bible from './bible'

// BEGIN tests ////////////////////////////////////////////////////////////////

describe('bible.reformat()', () => {
  it('works for book names', () => {
    expect(bible.reformat('James', bible.LONG)).toEqual('James')
  })
  it('works for special range', () => {
    expect(bible.reformat('1–2 Kings', bible.LONG)).toEqual('1–2 Kings')
  })
  it('Use en-dashes', () => {
    expect(bible.reformat('Gen 1:1-2', bible.LONG)).toEqual('Genesis 1:1–2')
  })
  it('Handles empty', () => {
    expect(bible.reformat('', bible.LONG)).toEqual('')
  })
  it('handles multi-book', () => {
    expect(bible.reformat('Nahum–Haggai', bible.LONG)).toEqual('Nahum–Haggai')
  })
})
