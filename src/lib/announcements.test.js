/* eslint-env jest */
import announcements from './announcements'
import { fromLAISO } from './datetime'

describe('filterSort', () => {
  it('empty works', () => {
    expect(announcements.filterSort([])).toEqual([])
  })

  it('one works', () => {
    const as = announcements.filterSort([{
      schedule: [{
        start: '2000-01-01',
        end: '3000-01-01',
        content: 'test'
      }]
    }])
    expect(as.length).toEqual(1)
    expect(as[0]).toEqual(expect.objectContaining({
      content: 'test'
    }))
  })

  it('last day works', () => {
    const as = announcements.filterSort([{
      schedule: [{
        start: '2000-01-01',
        // There may be a timezone bug with this line.
        end: (new Date()).toISOString().slice(0, 10),
        content: 'test'
      }]
    }])
    expect(as.length).toEqual(1)
    expect(as[0]).toEqual(expect.objectContaining({
      content: 'test'
    }))
  })

  it('catechism works', () => {
    const as = announcements.filterSort([
      announcements.catechismEvent()
    ])
    expect(as[0].content).toMatch('Catechism question')
  })
})

describe('articleEvent', () => {
  const monthAndDayAgo = '2019-11-30T00:00:00.000Z'
  const monthAgo = '2019-12-01T00:00:00.000Z'
  const now = '2020-01-01T00:00:00.000Z'
  const dayAhead = '2020-11-01T00:00:00.000Z'

  it('returns null for correct days', () => {
    const c = d => announcements
      .articleEvent({ date: d }, fromLAISO(now))
    expect(c(monthAndDayAgo)).toBeNull()
    expect(c(monthAgo)).not.toBeNull()
    expect(c(now)).not.toBeNull()
    expect(c(dayAhead)).toBeNull()
  })
})

describe('scheduleEvent', () => {
  const now = fromLAISO('2020-01-01T00:00:00.000Z')
  const ths = fromLAISO('2020-01-04T00:00:00.000Z')
  const nxt = fromLAISO('2020-01-11T00:00:00.000Z')
  const shy = fromLAISO('2020-01-14T00:00:00.000Z')
  const far = fromLAISO('2020-01-15T00:00:00.000Z')

  it('returns expected for date', () => {
    [
      [ths, 'Workday this Saturday!'],
      [nxt, 'Workday next Sat, Jan 11!'],
      [shy, 'Workday next Tue, Jan 14!']
    ].forEach(([date, content]) => {
      const e = {
        type: 'workday',
        date
      }

      const res = announcements.scheduleEvent('Workday', null, { e, now })
      expect(res).not.toBeNull()
      expect(res.content).toEqual(content)
    })
  })

  it('returns null for far', () => {
    [
      [far, 'Workday next Tue, Jan 14!']
    ].forEach(([date, content]) => {
      const e = {
        type: 'workday',
        date
      }
      const res = announcements.scheduleEvent(null, null, { e, now })
      expect(res).toBeNull()
    })
  })
})
