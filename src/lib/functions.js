/* eslint-env browser */
import querystring from 'query-string'
import { load as loadRecaptcha } from 'recaptcha-v3'

const RECAPTCHA_SITE_KEY = '6LcuSsgUAAAAAEIR1GFNkm2CS6pJK6x6NLSSNBPg'
const FUNCTIONS_ROOT = '/.netlify/functions'

function getPath (name) {
  return `${FUNCTIONS_ROOT}/${name}`
}

let recaptcha
async function genToken (action) {
  if (process.env.SVRBC_TEST === 'true') return 'fake_token'
  if (!action) return
  if (!recaptcha) recaptcha = await loadRecaptcha(RECAPTCHA_SITE_KEY)
  return recaptcha.execute(action)
}

async function doFetch (name, params, opts) {
  const qStr = querystring.stringify(params)
  const path = getPath(name)
  const resp = await fetch(`${path}?${qStr}`, opts)
  if (resp.status === 404) {
    return {
      status: 404,
      message: 'endpoint not found'
    }
  }
  let obj
  try {
    obj = await resp.json()
  } catch (e) {
    return {
      status: 0,
      message: 'JSON content was not returned'
    }
  }
  obj.status = resp.status
  return obj
}

async function get (name, params, opts) {
  opts = Object.assign({}, opts)
  return doFetch(name, params, {
    headers: {
      'Recaptcha-Token': await genToken(opts.recaptchaAction)
    }
  })
}

async function post (name, body, opts) {
  opts = Object.assign({}, opts)
  return doFetch(name, {}, {
    method: 'POST',
    body: JSON.stringify(body),
    headers: {
      'Recaptcha-Token': await genToken(opts.recaptchaAction),
      'Content-Type': 'text/json'
    }
  })
}

export default {
  getPath,
  get,
  post
}
