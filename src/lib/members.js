import functions from './functions'

export default {
  add: async (email) => functions.post('addmember', {
    email
  })
}
