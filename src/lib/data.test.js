/* eslint-env jest */
import { Sermon } from './data'

describe('Sermon', () => {
  describe('preacherName', () => {
    it('works for preacher', () => {
      const s = new Sermon({
        preacher: {
          title: 'Charles Spurgeon'
        }
      })
      expect(s.preacherName).toEqual('Charles Spurgeon')
    })

    it('works for unregisteredPreacher', () => {
      const s = new Sermon({
        preacher: null,
        unregisteredPreacher: 'Charles Spurgeon'
      })
      expect(s.preacherName).toEqual('Charles Spurgeon')
    })
  })

  describe('realDate', () => {
    it('works for y2k', () => {
      const s = new Sermon({
        date: '2000-01-01T00:00:00.000Z'
      })
      expect(s.realDate.getTime()).toEqual(946713600000)
    })
  })

  describe('dateId', () => {
    it('works for y2k', () => {
      const s = new Sermon({
        date: '2000-01-01T00:00:00.000Z'
      })
      expect(s.dateId).toEqual('2000-01-01')
    })
  })

  describe('monthDay', () => {
    it('works for y2k', () => {
      const s = new Sermon({
        date: '2000-01-01T00:00:00.000Z'
      })
      expect(s.monthDay).toEqual('January 1')
    })
  })

  describe('fullDate', () => {
    it('works for y2k', () => {
      const s = new Sermon({
        date: '2000-01-01T00:00:00.000Z'
      })
      expect(s.fullDate).toEqual('January 1, 2000')
    })
  })

  describe('seriesCount', () => {
    it('works for two sermons', () => {
      const s = new Sermon({
        series: {
          sermons: {
            edges: [null, null]
          }
        }
      })
      expect(s.seriesCount).toEqual(2)
    })
  })
})
