import delay from 'delay'

import { fromISO, readFutureLADate } from './datetime'
import functions from './functions'

const MINUTE = 60 * 1000
const HOUR = 60 * MINUTE

async function wait (target) {
  const duration = target ? target - new Date() : 0
  await delay(Math.max(MINUTE, Math.min(HOUR, duration)))
}

async function live (typ) {
  return functions.get('youtubelive', {}, {
    recaptchaAction: `live_${typ}`
  })
}

async function liveOrSoon (typ) {
  const resp = await functions.get('youtubelive', {
    strategy: 'liveorsoon'
  }, {
    recaptchaAction: `live_${typ}`
  })
  Object.assign(resp, resp.broadcast
    ? {
        started: Boolean(resp.broadcast.snippet.actualStartTime),
        scheduledStart: fromISO(resp.broadcast.snippet.scheduledStartTime)
      }
    : {
        started: false,
        scheduledStart: null
      })
  return resp
}

function makePoll (f) {
  return async (typ, cb) => {
    while (true) {
      const resp = await f(typ)
      cb(resp)
      typ = 'poll'
      if (resp.status !== 200) {
        console.error('Error getting YouTube live status:', resp.status)
        await wait()
      } else if (resp.started) {
        await wait()
      } else if (resp.scheduledStart) {
        await wait(resp.scheduledStart)
      } else {
        // Target the Sunday Service.
        await wait(readFutureLADate('11:00am on Sunday'))
      }
    }
  }
}

export default {
  live,
  liveOrSoon,
  pollLiveOrSoon: makePoll(liveOrSoon)
}
