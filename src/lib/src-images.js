import { promises as fs } from 'fs'
import path from 'path'
import { promisify } from 'util'

import fileType from 'file-type'
import glob from 'glob-promise'
import _imageSize from 'image-size'
import readChunk from 'read-chunk'

const imageSize = promisify(_imageSize)

async function parseImg (filepath) {
  const stat = await fs.stat(filepath)
  if (!stat.isFile()) return null

  const buffer = await readChunk(filepath, 0, fileType.minimumBytes)
  const type = fileType(buffer)
  if (!type || !type.mime.startsWith('image/')) return null

  const data = {
    path: filepath,
    type: type.ext,
    basename: path.basename(filepath),
    bytes: stat.size
  }

  data.dims = await imageSize(filepath)

  return data
}

async function scan (...pathParts) {
  const paths = await glob(path.join(...pathParts))
  return Promise.all(paths.map(parseImg))
}

export default {
  scan
}
