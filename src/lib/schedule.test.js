/* eslint-env jest */
import workdays from '../data/workdays.yml'
import {
  readLADate
} from './datetime'
import {
  getYearTables,
  getNextCatechismQuestion,
  getNext,
  readSchedule,
  sermonSoon,
  sundayInSession
} from './schedule'

describe('getYearTables', () => {
  it('works for workdays', () => {
    const schedule = readSchedule(workdays.schedule)
    let l
    expect(() => { l = getYearTables(schedule) }).not.toThrow()
    expect(l.length).toEqual(2)
  })
})

describe('getNext', () => {
  it('works for workdays', () => {
    const schedule = readSchedule(workdays.schedule)
    let n
    expect(() => (n = getNext(schedule))).not.toThrow()
    expect(n).not.toBeNull()
  })
})

describe('getNextCatechismQuestion', () => {
  it('works', () => {
    let c
    expect(() => (c = getNextCatechismQuestion())).not.toThrow()
    expect(c).toBeGreaterThanOrEqual(1)
    expect(c).toBeLessThanOrEqual(114)
  })
})

describe('sundayInSession', () => {
  it('works', () => {
    const c = t => sundayInSession(readLADate(t))
    expect(c('2020-01-05T05:00:00.000Z')).toBeFalse() // 5:00am
    expect(c('2020-01-05T06:00:00.000Z')).toBeTrue() // 6:00am
    expect(c('2020-01-05T11:00:00.000Z')).toBeTrue() // 11:00am
    expect(c('2020-01-05T17:00:00.000Z')).toBeTrue() // 5:00pm
    expect(c('2020-01-05T18:00:00.000Z')).toBeFalse() // 6:00pm
  })
})

describe('sermonSoon', () => {
  it('works', () => {
    const c = t => sermonSoon(readLADate(t))
    expect(c('2020-01-05T10:30:00.000Z')).toBeFalse() // 10:30am
    expect(c('2020-01-05T10:45:00.000Z')).toBeTrue() // 10:45am
    expect(c('2020-01-05T11:30:00.000Z')).toBeTrue() // 11:30am
    expect(c('2020-01-05T11:45:00.000Z')).toBeFalse() // 11:45pm
  })
})
