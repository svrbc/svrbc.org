/* eslint-env browser */
import seedrandom from 'seedrandom'

function getWaveformUrl (url) {
  const re = /https:\/\/(.*?).storage.googleapis.com\/(.*).mp3/
  const match = url.match(re)
  return 'https://svrbc-web.storage.googleapis.com/waveforms/' +
    `${match[1]}/${match[2]}.waveform.json`
}

function createAudio (src) {
  const elem = document.createElement('audio')
  elem.src = src
  // Set crossOrigin to anonymous to avoid CORS restrictions
  elem.crossOrigin = 'anonymous'
  // Without this line, sometimes the audio doesn't play.
  // It's not clear if it's a bug with wavesurfer or race condition in the way
  // we are creating the dynamic element.
  elem.load()
  return elem
}

function genFakeWaves (src) {
  // We seed the random number generator so that it's always the same
  // waveform for a given audio file.
  const r = seedrandom(src)
  // (2*r.quick()) - 1  gives us a random number between -1 and 1.
  return Array.apply(null, Array(100)).map(() => (2 * r.quick()) - 1)
}

// Fetch and normalize the peaks.
async function fetchPeaks (src) {
  const resp = await fetch(getWaveformUrl(src))
  if (resp.status === 404) {
    console.log(`No waveform found for ${src}.  Using a random waveform.`)
    return genFakeWaves(src)
  }

  const peaks = await resp.json()
  const max = Math.max(...peaks.data.map(Math.abs))
  return peaks.data.map(d => d / max)
}

async function load (wavesurfer, src, fakewaves) {
  const audio = createAudio(src)
  const peaks = fakewaves ? genFakeWaves(src) : await fetchPeaks(src)
  wavesurfer.load(audio, peaks)
}

function createWavesurfer (src, elem, opts) {
  opts = Object.assign({
    callback: () => {},
    fakewaves: false
  }, opts)

  // wavesurfer.js does not support SSR.
  // https://github.com/katspaugh/wavesurfer.js/issues/751
  const WaveSurfer = require('wavesurfer.js')
  const cs = getComputedStyle(elem)
  const AudioContext = window.AudioContext || window.webkitAudioContext
  const audioContext = new AudioContext()

  const wavesurfer = WaveSurfer.create({
    backend: 'MediaElementWebAudio',
    container: elem,
    fillParent: true,
    height: elem.clientHeight,
    waveColor: cs.getPropertyValue('--accent-color-light'),
    progressColor: cs.getPropertyValue('--accent-color'),
    cursorWidth: 0.5,
    cursorColor: cs.getPropertyValue('--dark-gray'),
    hideScrollbar: true,
    responsive: true,
    barWidth: 4,
    barRadius: 3,
    barMinHeight: 1,
    audioContext
  })

  const events = [
    'ready',
    'finish',
    'audioprocess',
    'play',
    'pause',
    'seek'
  ]
  events.forEach(e => wavesurfer.on(e, opts.callback))
  wavesurfer.on('error', console.error)

  // The first time we try to play, resume the context.
  // This is necessary in chrome, where the audio context is suspended if
  // there is no interaction.
  function resumeContext () {
    audioContext.resume()
    wavesurfer.un('play', resumeContext)
  }
  wavesurfer.on('play', resumeContext)

  // Load in the background, but don't wait to return.
  load(wavesurfer, bucketApiUrl(src), opts.fakewaves)
  return wavesurfer
}

function bucketApiUrl (url) {
  return url.replace(
    /^https:\/\/storage.googleapis.com\/(.*?)\//,
    'https://$1.storage.googleapis.com/')
}

export default {
  createWavesurfer
}

const test = {
  bucketApiUrl
}

export {
  test
}
