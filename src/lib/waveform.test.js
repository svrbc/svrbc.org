/* eslint-env jest */
import { test } from './waveform'

describe('bucketApiUrl', () => {
  it('already OK not changed', () => {
    const url = 'https://svrbc-web.storage.googleapis.com/orrick-catechism/orrick-catechism-q001.mp3'
    expect(test.bucketApiUrl(url)).toEqual(url)
  })

  it('simple example works', () => {
    expect(test.bucketApiUrl('https://storage.googleapis.com/pbc-ca-sermons/2007/2007-01-07+Whose+Acts-Acts+1-1-3.mp3'))
      .toEqual('https://pbc-ca-sermons.storage.googleapis.com/2007/2007-01-07+Whose+Acts-Acts+1-1-3.mp3')
  })
})
