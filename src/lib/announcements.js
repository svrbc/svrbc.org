import {
  addMonths,
  daysFromNow,
  readLADate,
  readFutureLADate,
  formatLADate,
  longWeekdayFmt,
  shortMonthWeekdayFmt
} from './datetime'
import {
  getNextCatechismDate,
  getNextCatechismQuestion,
  getNext,
  readSchedule
} from './schedule'

function filterSort (announcements) {
  const now = new Date()
  return announcements
    .filter(a => !!a)
    .map(a => {
      if (!a.schedule) return a

      const starts = a.schedule.map(s => readLADate(s.start))
      const ends = a.schedule.map((s, i) => s.end
        ? readLADate(s.end)
        : starts[i + 1])
        .map(e => new Date(e.getTime() + (24 * 60 * 60 * 1000)))
      const urgencies = a.schedule.map((s, i) => s.urgency
        ? readFutureLADate(s.urgency)
        : ends[i])

      const ret = {
        image: a.image,
        style: a.style
      }
      for (let i = 0; i < a.schedule.length; i++) {
        if (a.schedule[i].content) ret.content = a.schedule[i].content
        if (a.schedule[i].link) ret.link = a.schedule[i].link
        if (starts[i] <= now && ends[i] >= now) {
          return Object.assign(ret, {
            date: urgencies[i]
          })
        }
      }
      return null
    })
    .filter(d => d)
    .sort((a, b) => a.date - b.date)
}

function articleEvent (article, now = null) {
  now = now || new Date()
  const date = readLADate(article.date)
  if (addMonths(date, 1) < now || date > now) return null
  return {
    date: addMonths(date, 2),
    content: `Article: ${article.title}`,
    link: article.path,
    image: article.image,
    style: article.bannerstyle
  }
}

function catechismEvent (image) {
  const num = getNextCatechismQuestion()
  const date = getNextCatechismDate()
  const prettyDate = formatLADate(date, shortMonthWeekdayFmt)
  return {
    date,
    content: `Catechism question for ${prettyDate}: #${num}`,
    link: `https://baptistcatechism.org/${num}/`,
    image,
    style: 'light'
  }
}

function scheduleEvent (name, schedule, testopts = {}) {
  const e = testopts.e || getNext(readSchedule(schedule))
  const days = daysFromNow(e.date, testopts)
  if (days > 13) return null
  const prettyWeekday = formatLADate(e.date, longWeekdayFmt)
  const prettyDate = formatLADate(e.date, shortMonthWeekdayFmt)
  const prettyThisWeekday = `this ${prettyWeekday}`
  const prettyNextDate = `next ${prettyDate}`
  const proximity = days < 7 ? prettyThisWeekday : prettyNextDate
  return {
    date: e.date,
    content: `${name} ${proximity}!`
  }
}

export default {
  articleEvent,
  catechismEvent,
  scheduleEvent,
  filterSort
}
