import { bcv_parser as BcvParser } from 'bible-passage-reference-parser/js/en_bcv_parser'
import osisToEn from 'bible-reference-formatter'

const bcvParser = new BcvParser()
bcvParser.set_options({
  book_alone_strategy: 'full',
  book_sequence_strategy: 'include'
})

const LONG = 'esv-long'
const SHORT = 'esv-short'

function reformat (refStr, format) {
  if (refStr === '') return ''
  const match = refStr.match(/^[0-9][-–]/)
  let prefix = ''
  if (match) prefix = match[0]
  const bcvRef = bcvParser.parse(refStr)
  if (!bcvRef.entities.length) {
    throw new Error(`Could not parse ref: "${refStr}"`)
  }

  let en
  try {
    en = osisToEn(format, bcvRef.osis())
  } catch (e) {
    return refStr
  }
  return prefix + en
    // Ranges should be en-dashes, not em-dashes.
    .replace(/—/g, '–')
}

export default {
  LONG,
  SHORT,
  reformat
}
