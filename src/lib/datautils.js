// FUTURE(node v14): Use import syntax.
// import sslugify from '@sindresorhus/slugify'
const sslugify = require('@sindresorhus/slugify')

function normalizeKey (k) {
  k = k.toLowerCase()
    .replace(/['’]/g, '')
    .replace(/\s+[–—]\s+/g, ' ')
  // FUTURE(Android > 11): Remove the conditional.
  // https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String/normalize#Browser_compatibility
  if (k.normalize) k = k.normalize('NFD')
  return k.replace(/[\u0300-\u036f]/g, '')
}

function slugify (s) {
  // This handles cases like McDonald.
  s = s.replace(/\bMc([A-Z][a-z])/g, (m, m1) => {
    return `mc${m1.toLowerCase()}`
  })
  return sslugify(s, {
    customReplacements: [
      ['s\'s', 's'],
      ['s’s', 's'],
      ['\'', ''],
      ['’', '']
    ]
  })
}

// FUTURE(node v14): Use export syntax.
module.exports = {
  normalizeKey,
  slugify
}
