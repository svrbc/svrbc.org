/* eslint-env browser */
import delay from 'delay'
import domelo from 'domelo'

import tips from './tips'

const ERR_COLOR = '#f97b79'

let styled = false
function addStyles () {
  if (styled) return
  styled = true
  document.querySelector('head').appendChild(domelo(`
<style type="text/css">
h1.scan,
h2.scan,
h3.scan,
h4.scan,
h5.scan,
h6.scan,
h7.scan {
 background-color: ${ERR_COLOR};
}

img.scan {
  border: 3px ${ERR_COLOR} solid;
}
</style>
`))
}

// BEGIN utilities /////////////////////////////////////////////////////////////

function getIgnoreWords () {
  const div = document.querySelector('.spell-ignore')
  if (!div) return []
  return div.innerText.trim().split('\n')
}

// BEGIN scans /////////////////////////////////////////////////////////////////

const defs = [{
  shortName: 'h3+ tags',
  errorMessage: 'h tags should not have depth greater than 2.',
  selector: () => {
    if (window.location.pathname.startsWith('/constitution')) return []
    return Array.from(document.querySelectorAll('h3,h4,h5,h6,h7'))
      .filter(h => !h.closest('.passage'))
  }
}, {
  shortName: 'missing h1 tag',
  errorMessage: 'there should be at least one h1 tag',
  selector: () => {
    if (window.location.pathname === '/') return []
    if (window.location.pathname.startsWith('/data/')) return []
    if (document.querySelectorAll('h1').length > 0) return []
    return [document.querySelector('.main-narrow')]
  }
}, {
  shortName: 'multiple h1 tags',
  errorMessage: 'there should be only one h1 tag per page',
  selector: () => {
    const h1s = document.querySelectorAll('h1')
    return h1s.length > 1 ? Array.from(h1s) : []
  }
}, {
  shortName: 'anchored h1 tags',
  errorMessage: 'h1 tags should never have ids ' +
    '({#anchor} in markdown).',
  selector: () => Array.from(document.querySelectorAll('h1[id]'))
}, {
  shortName: 'anchorless h2 tags',
  errorMessage: 'h2 tags should always have explicit ids ' +
    '({#anchor} in markdown).',
  selector: () =>
    Array.from(document.querySelectorAll('h2:not([id]),h2[id=""]'))
      .filter(h => !h.closest('.passage'))
      .filter(h => !h.classList.contains('subtitle'))
}, {
  shortName: 'linkless headers',
  errorMessage: 'h2 tags should have a link icon.',
  selector: () => Array.from(document.querySelectorAll('h2'))
    .filter(h => !h.querySelector('.icon'))
    .filter(h => !h.classList.contains('no-link'))
    .filter(h => !h.closest('.passage'))
}, {
  shortName: 'altless images',
  errorMessage: 'img tags should always have alt text.',
  selector: () => Array.from(document.querySelectorAll('img:not([alt])'))
}, {
  shortName: 'non-square testimonial images',
  errorMessage: 'images in .testimonial should always be square.',
  selector: () => Array.from(document.querySelectorAll('.testimonial img'))
    .filter(img => img.offsetWidth !== img.offsetHeight)
}, {
  shortName: 'extra spell-ignores',
  errorMessage: 'there should be at most one <spell-ignore> component.',
  selector: () => {
    const divs = Array.from(document.querySelectorAll('.spell-ignore'))
    if (divs.length < 2) return []
    return divs
  }
}, {
  shortName: 'unsorted spell-ignore',
  errorMessage: 'the contents of the <spell-ignore> component.',
  selector: () => {
    let words = []
    const div = document.querySelector('.spell-ignore')
    if (div) words = div.innerText.trim().split('\n')
    for (let i = 1; i < words.length; i++) {
      const [w1, w2] = [words[i - 1], words[i]]
      if (w1 >= w2) return [div]
    }
    return []
  }
}, {
  shortName: 'superfluous spell-ignore words',
  errorMessage: 'the contents of the <spell-ignore> should appear in the page.',
  selector: () => {
    let words = []
    const div = document.querySelector('.spell-ignore')
    if (div) words = div.innerText.trim().split('\n')
    const text = document.body.innerText
    for (const word of words) {
      if (!text.includes(word)) return [word]
    }
    return []
  }
}]

let runTime = null
let scanning = false
async function run () {
  if (scanning) return
  // Wait for page stabilization before actual run.
  const newRunTime = runTime = new Date().getTime()
  await delay(500)
  if (runTime !== newRunTime) return
  try {
    await doRun()
  } catch (e) {
    console.error(e)
  }
}

let prevElems = null
async function doRun (rescan = false) {
  if (scanning && !rescan) return
  scanning = true

  const scanElems = Array.from(document.querySelectorAll('.scan'))
  if (prevElems === null) prevElems = scanElems

  // Add new results.
  const errors = []
  const elems = []
  defs.filter(scan => scan.web !== false).forEach(scan => {
    scan.selector().filter(elem => !elems.includes(elem)).forEach(elem => {
      elems.push(elem)
      errors.push([scan.errorMessage, elem])
    })
  })

  // Continue running until we stabilize.
  if (elems.some(elem => !prevElems.includes(elem)) ||
    prevElems.some(elem => !elems.includes(elem))) {
    prevElems = elems
    doRun(true)
    return
  }

  // At this point, the page seems stable, so let's style everything.
  scanElems.filter(elem => !elems.includes(elem)).forEach(elem => {
    elem.classList.remove('scan')
    elem.tip.destroy()
  })
  errors.filter(([_, elem]) => !scanElems.includes(elem))
    .forEach(([msg, elem]) => {
      elem.classList.add('scan')
      elem.tip = tips.hover(elem, msg)
    })
  errors.filter(([_, elem]) => !scanElems.includes(elem))
    .forEach(([msg, elem]) => {
      console.error(msg, elem)
    })

  addStyles()
  prevElems = null
  // Let our dom changes propogate before turning off scanning.
  await delay(1)
  scanning = false
}

export default {
  defs,
  getIgnoreWords,
  run
}
