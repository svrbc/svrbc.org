/* eslint-env jest */
import { normalizeKey, slugify } from './datautils'

describe('normalizeKey', () => {
  it('removes diacritics', () => {
    expect(normalizeKey('Jesús')).toEqual('jesus')
  })
})

describe('slugify', () => {
  it('works for basic data', () => {
    expect(slugify('A Biblical Argument Against Smores'))
      .toEqual('a-biblical-argument-against-smores')
  })

  it('slugify possessives correctly', () => {
    expect(slugify('God’s Biblical Argument Against Smores'))
      .toEqual('gods-biblical-argument-against-smores')
  })

  it('slugify non-curly possessives correctly', () => {
    expect(slugify('God\'s Biblical Argument Against Smores'))
      .toEqual('gods-biblical-argument-against-smores')
  })
})
