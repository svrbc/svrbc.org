import crypto from 'crypto'

import handler from '../lib/lambda'
import mail from '../lib/mail-server'

// TODO: cache the token value locally and not honor any subsequent request
// with the same token. This will prevent replay attacks.
// TODO: check if the timestamp is not too far from the current time.
const verify = ({ timestamp, token, signature }) => {
  const encodedToken = crypto
    .createHmac('sha256', process.env.MAILGUN_WEBHOOK_SIGNING_KEY)
    .update(timestamp.concat(token))
    .digest('hex')

  if (encodedToken !== signature) throw new Error('could not verify signature')
}

function consume (data) {
  if (!data.signature) throw new Error('no signature provided')
  verify(data.signature)

  const eventData = data['event-data']
  if (!eventData) throw new Error('no event data provided')

  const blob = JSON.stringify(eventData, null, 2)
  console.log('failed mail', blob)

  return mail.createSendRequest({
    from: { address: 'do-not-reply@svrbc.org' },
    to: ['conley'],
    data: { blob },
    template: 'mailerror'
  })
}

/*
 * https://documentation.mailgun.com/en/latest/user_manual.html#webhooks
 */
exports.handler = handler(consume, mail.send)
