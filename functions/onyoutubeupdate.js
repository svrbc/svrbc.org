import handler from '../lib/lambda'
import updateRoutine from '../lib/routines/updatefromyoutube'
import youtube from '../lib/youtube-server'

function consume (data) {
  if (data.httpMethod === 'GET') {
    const challenge = data['hub.challenge']
    const topic = data['hub.topic']
    const mode = data['hub.mode']
    if (!challenge) throw new Error('no challenge specified')
    if (!topic) throw new Error('no topic specified')
    if (!mode) throw new Error('no mode specified')
    if (topic !== youtube.SVRBC_TOPIC_URL) {
      throw new Error(`wrong topic url: ${topic}`)
    }
    if (mode !== 'subscribe') throw new Error(`wrong mode: ${mode}`)
    return {
      mode: 'verify',
      challenge: data['hub.challenge']
    }
  }

  if (data.id) {
    // If ID is set, we are trying to manually trigger this function in a form.
    return {
      mode: 'update',
      ids: [data.id]
    }
  }

  if (!data.feed) throw new Error('no feed specified')
  if (!data.feed.entry) throw new Error('no feed specified')
  const ids = data.feed.entry
    .map(e => e['yt:videoId'])
    .flat()
  if (ids.some(id => !id)) throw new Error('missing video id')
  return {
    mode: 'update',
    ids
  }
}

// This will help us prevent normalization loops should we ever
// accidentally create one.
const needsUpdate = {}
const blocked = {}

async function produce ({ mode, ids, challenge }) {
  if (mode === 'verify') return challenge

  ids = ids.filter(id => !blocked[id])
  const { normalized, updated } = await updateRoutine(ids, { onepass: true })
  normalized.filter(id => needsUpdate[id])
    .forEach(id => (blocked[id] = true))
  normalized.forEach(id => (needsUpdate[id] = true))
  updated.forEach(id => (needsUpdate[id] = false))

  return `(${normalized.join()}) normalized; (${updated.join()}) updated`
}

/*
 * https://developers.google.com/youtube/v3/guides/push_notifications
 * To test this run the following (with vid.xml populated and no newline at
 * end)
 * curl \
 *   -X POST \
 *   -H "Content-Type: application/atom+xml" \
 *   -H "X-Hub-Signature: sha1=$(cat vid.xml | openssl dgst -sha1 -hmac sabusntatbeusrcaexscae | grep -o '[^ ]*$')" \ (This line doesn't work yet...)
 *   --data-binary @vid.xml \
 *   -w "\n" \
 *   'http://localhost:8888/.netlify/functions/onyoutubeupdate'
 */
exports.handler = handler(consume, produce, {
  responseContentType: 'text',
  hmacSecret: process.env.YOUTUBE_CALLBACK_SECRET
})
