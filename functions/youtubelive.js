import handler from '../lib/lambda'
import youtube from '../lib/youtube-server'

function consume (data) {
  let filter = broadcasts => ({ broadcasts })
  if (data.strategy === 'liveorsoon') {
    filter = broadcasts => ({ broadcast: youtube.liveOrSoon(broadcasts) })
  }
  return { filter }
}

// This will keep us from querying youtube with every hit.
let cache = null
let timestamp = 0
const lifetime = 60 * 1000 // 1 minute
async function getBroadcasts () {
  const yt = await youtube.getClient()
  if (Date.now() < lifetime + timestamp) return cache

  timestamp = Date.now()
  cache = await yt.getBroadcasts()
  return cache
}

async function produce (config) {
  const broadcasts = await getBroadcasts()
  return config.filter(broadcasts)
}

exports.handler = handler(consume, produce, {
  recaptchaActions: [
    'live_load',
    'live_poll'
  ]
})
