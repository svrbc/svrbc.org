import handler from '../lib/lambda'
import mail from '../lib/mail-server'

exports.handler = handler(mail.createSendRequest, mail.send, {
  recaptchaActions: ['mail']
})
