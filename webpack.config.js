const nodeExternals = require('webpack-node-externals')

// This is loaded for use with netlify-lambda.
// To change the webpack config for gridsome see configureWebpack in
// gridsome.config.js or configureWebpack in gridsome.server.js.
module.exports = {
  // This is necessary for firebase.
  // https://github.com/netlify/netlify-lambda#webpack-configuration
  externals: [nodeExternals()],
  module: {
    rules: [
      { test: /\.handlebars$/, loader: 'handlebars-loader' }
    ]
  },
  optimization: {
    minimize: !!process.env.MINIMIZE
  }
}
