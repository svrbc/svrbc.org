/* eslint-env jest */
import { promises as fs } from 'fs'
import path from 'path'

const FILEPATH = path.join(__dirname, 'emails.txt')

describe('emails.txt tests', () => {
  let emails

  // The first page load can be especially slow.
  beforeAll(async () => {
    const content = await fs.readFile(FILEPATH, 'utf8')
    emails = content.split(/\s+/).filter(w => w !== '')
  })

  it('emails are sorted', async () => {
    const sortedEmails = emails.slice(0)
    sortedEmails.sort()
    expect(emails).toEqual(sortedEmails)
  })
})
