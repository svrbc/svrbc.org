// Server API makes it possible to hook into various parts of Gridsome
// on server-side and add custom data to the GraphQL data layer.
// Learn more: https://gridsome.org/docs/server-api
if (!process.env.TEST) {
  // jest handles modules correctly on its own these days, and if we swap out
  // `require`, we'll run into issues.
  require = require('esm')(module) // eslint-disable-line no-global-assign
}
const fs = require('fs').promises
const glob = require('glob-promise')
const path = require('path')

const podcast = require('./lib/podcast')
const redirects = require('./lib/redirects')

async function genPodcast ({ outputDir }) {
  const start = Date.now()
  const data = await podcast.generate()
  const outFile = path.join(outputDir, 'sermons.rss')
  await fs.writeFile(outFile, data)
  const seconds = (Date.now() - start) / 1000
  console.log(`Podcast RSS generated - ${seconds}s`)
}

async function genRedirects ({ outputDir }) {
  const start = Date.now()
  const data = await redirects.generate()
  const outFile = path.join(outputDir, '_redirects')
  await fs.writeFile(outFile, data)
  const seconds = (Date.now() - start) / 1000
  console.log(`_redirects generated - ${seconds}s`)
}

// Changes here requires a server restart.
// To restart press CTRL + C in terminal and run `gridsome develop`
module.exports = (api, options) => {
  api.configureServer(app => {
    app.get('/sermons.rss', async (req, res) => {
      const data = await podcast.generate()
      res.type('application/xml')
        .send(data)
    })
  })
  api.afterBuild(async ({ config }) => {
    await genPodcast(config)
    await genRedirects(config)
  })
  api.loadSource(async actions => {
    const srcImages = actions.addCollection({
      typeName: 'SrcImage'
    })
    const src = path.join(__dirname, 'src')
    const images = await glob(path.join(src, '**', '*.jpg'))
    for (const img of images) {
      await srcImages.addNode({
        id: path.relative(src, img),
        image: img
      })
    }

    const srcText = actions.getCollection('SrcText')
    const nodes = await srcText.findNodes()
    for (const node of nodes) {
      await srcText.removeNode(node.id)
      node.id = node.path.replace(/\.?\/(.*)-text\//, '$1')
      await srcText.addNode(node)
    }
  })
}
