import admin from 'firebase-admin'

function initialize () {
  if (admin.apps.length) return
  let key = process.env.GCP_SERVICE_ACCOUNT_PRIVATE_KEY
  if (key.startsWith('"')) key = JSON.parse(key)
  admin.initializeApp({
    credential: admin.credential.cert({
      type: 'service_account',
      project_id: 'svrbc-org',
      private_key_id: '82bae781e44c7e05f5168d39b6e851fac5da6a0e',
      private_key: key,
      client_email: 'svrbc-org@appspot.gserviceaccount.com',
      client_id: '108966353513664489409',
      auth_uri: 'https://accounts.google.com/o/oauth2/auth',
      token_uri: 'https://oauth2.googleapis.com/token',
      auth_provider_x509_cert_url: 'https://www.googleapis.com/oauth2/v1/certs',
      client_x509_cert_url: 'https://www.googleapis.com/robot/v1/metadata/x509/svrbc-org%40appspot.gserviceaccount.com'
    })
  })
}

class FirestoreClient {
  constructor (origin) {
    this.origin = origin
    this.tokens = {}

    initialize()
    this.db = admin.firestore()
  }

  getKey (name) {
    const prefix = this.origin.replace(/https?:\/\//, '')
    return `${prefix}-${name}`
  }

  tokenRef (name) {
    return this.db.collection('tokens').doc(this.getKey(name))
  }

  async getToken (name) {
    if (name in this.tokens) return this.tokens[name]

    let doc
    try {
      doc = await this.tokenRef(name).get()
    } catch (e) {
      console.error(e)
      console.error(e.stack)
      return null
    }
    if (!doc.exists) {
      return null
    }
    this.tokens[name] = doc.data()
    return this.tokens[name]
  }

  async setToken (name, doc) {
    await this.tokenRef(name).set(doc)
  }
}

function getClient (origin) {
  return new FirestoreClient(origin)
}

export default {
  getClient
}

export {
  FirestoreClient
}
