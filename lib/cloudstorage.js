import { Storage } from '@google-cloud/storage'
import { promises as fs } from 'fs'
import path from 'path'

let storage
function getStorage () {
  if (storage) return storage

  storage = new Storage({
    projectId: 'svrbc-org',
    credentials: {
      client_email: 'svrbc-org@appspot.gserviceaccount.com',
      private_key: process.env.GCP_SERVICE_ACCOUNT_PRIVATE_KEY
    }
  })
  return storage
}

class Client {
  constructor (bucketName) {
    this.bucket = getStorage().bucket(bucketName)
  }

  async list (prefix) {
    const resp = await this.bucket.getFiles({
      prefix
    })
    return resp[0].filter(f => !f.metadata.name.endsWith('/'))
  }

  async download (file, destination) {
    await fs.mkdir(path.dirname(destination), { recursive: true })
    return file.download({ destination })
  }

  async upload (path, destination) {
    return this.bucket.upload(path, {
      destination
    })
  }
}

function getClient (bucketName) {
  return new Client(bucketName)
}

export default {
  getClient
}
