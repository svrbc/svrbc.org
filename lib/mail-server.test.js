/* eslint-env jest */
import FormData from 'form-data'

import mail from './mail-server'

// Normalize FormData
function n (formdata) {
  const boundaryRe = new RegExp(formdata.getBoundary(), 'g')
  return formdata.getBuffer().toString().replace(boundaryRe, '')
}

describe('createMgRequest', () => {
  const healthyReq = {
    from: {
      name: 'Person McPerson',
      address: 'from@example.com'
    },
    to: ['info'],
    subject: 'Subject',
    template: 'plain',
    data: {
      name: 'Person McPerson',
      email: 'from@example.com',
      path: '/path',
      message: 'This is a message.'
    }
  }

  it('plain template works', async () => {
    const res = FormData()
    res.append('from',
      'Person McPerson (from@example.com) via SVRBC.org <webmail@svrbc.org>')
    res.append('h:Reply-To', 'Person McPerson <from@example.com>')
    res.append('to', 'SVRBC Officers <info@svrbc.org>')
    res.append('subject', 'Message from svrbc.org/path (from@example.com)')
    res.append('html', 'This is a message.')

    const req = mail.createSendRequest(healthyReq)
    expect(n(req)).toEqual(n(res))
  })
})
