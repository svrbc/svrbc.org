import chalk from 'chalk'
import { promises as fs } from 'fs'
import path from 'path'

import cli from './cli'

const MODULE_NAME = 'lambda-env'
const KEYS = [
  'GCP_SERVICE_ACCOUNT_PRIVATE_KEY',
  'SVRBC_BOT_GPG_PRIVATE_KEY'
]

async function main ({ _: [prefix] }) {
  console.log(chalk.cyan('Generating keys…'))
  const filename = path.join(__dirname, `${MODULE_NAME}.js`)
  const data = KEYS
    .map(k => `process.env.${k} = ${JSON.stringify(process.env[k] || '')}`)
    .join('\n')
  await fs.writeFile(filename, data)

  return 0
}

cli.run('genkeys', main, {
  // We don't require the keys be present because this needs to still run in
  // our CI tests that don't have access to the variables.
  env: []
})
