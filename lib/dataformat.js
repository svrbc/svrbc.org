import { Parser } from 'htmlparser2'
import yaml from 'js-yaml'

import bible from '../src/lib/bible'
import { spellCheck } from './spell'

function iterateFields (node, specs) {
  const ret = []
  for (const s of specs) {
    let cur = [[node, null]]
    for (const f of s.split(/\./g)) {
      const next = []
      for (const c of cur) {
        const sub = c[0][f]
        if (!sub) continue
        const subList = Array.isArray(sub) ? sub : [sub]
        next.push(...subList.map(s => [s, v => { c[0][f] = v }]))
      }
      cur = next
    }
    ret.push(...cur)
  }
  return ret
}

function checkVerses (spec) {
  return (node, warn, fix) => {
    for (const [v, w] of iterateFields(node, [spec])) {
      const v2 = bible.reformat(v, bible.LONG)
      if (v === v2) continue
      w(v2)
      if (!fix) warn(`${v} is not formatted; should be ${v2}`)
    }
  }
}

function textFromHtml (html) {
  const blocks = []
  const parser = new Parser({
    ontext (text) {
      blocks.push(text)
    }
  })
  parser.write(html)
  parser.end()
  return blocks.join(' ')
}

function checkSpelling (key, opts) {
  opts = Object.assign({
    html: false,
    offline: false
  }, opts)
  return async (node, warn, fix, offline) => {
    if (opts.offline && !offline) return
    const t = opts.html ? textFromHtml(node[key]) : node[key]
    const messages = await spellCheck(t)
    messages.forEach(m => warn(node, m))
  }
}

function formatMd (node, keys, opts) {
  const fm = formatYaml(node, keys, opts).trim()

  const content = (node.mdContent || node.txtContent || '').trim()
  return `---
${fm}
---

${content}
`.trim() + '\n'
}

function formatYaml (node, keys, opts) {
  opts = Object.assign({
    noquote: []
  }, opts)
  const obj = {}
  keys.forEach(k => {
    if (![undefined, null, ''].includes(node[k])) obj[k] = node[k]
  })
  let content = yaml.dump(obj, {
    lineWidth: 79,
    sortKeys: (a, b) => keys.indexOf(a) - keys.indexOf(b),
    styles: {
      '!!timestamp': 'YYYY'
    }
  })
  opts.noquote.forEach(k => {
    const re = new RegExp(`^(\\s*${k}: )(['"])(.*)\\2`, 'mg')
    content = content.replace(re, '$1$3')
  })
  return content
}

export default {
  checkSpelling,
  checkVerses,
  formatMd,
  formatYaml
}
