import gql from 'graphql-tag'

import { normalizeKey } from '../src/lib/datautils'
import graphql from './graphql'

async function getSeriesTitleMap () {
  const res = await graphql.query(gql`
    {
      allSermonSeries {
        edges {
          node {
            id
            title
          }
        }
      }
    }
  `)
  const series = res.data.allSermonSeries.edges.map((e) => e.node)
  return Object.fromEntries(series.map((s) => [normalizeKey(s.title), s.id]))
}

async function getPreachers () {
  const res = await graphql.query(gql`
    {
      allPreacher {
        edges {
          node {
            id
            title
            lastname
          }
        }
      }
    }
  `)
  const preachers = res.data.allPreacher.edges.map((e) => e.node)
  return Object.fromEntries(
    preachers.map((s) => [normalizeKey(s.title), s.id])
  )
}

async function getSermons () {
  const res = await graphql.query(gql`
    {
      allSermon {
        edges {
          node {
            id
            date
            text
            title
            slug
            preacher
            audio
            fileInfo {
              extension
              name
            }
            series {
              id
              name
            }
          }
        }
      }
    }
  `)
  return res.data.allSermon.edges.map((e) => e.node)
}

async function getSermonMap () {
  const sermons = await getSermons()
  return Object.fromEntries(sermons.map((s) => [s.id, s]))
}

export default {
  getSeriesTitleMap,
  getSermonMap,
  getSermons,
  getPreachers
}
