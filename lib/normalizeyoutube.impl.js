import chalk from 'chalk'

import cli from './cli'
import youtube from './youtube-server'

async function main ({ _: [...ids] }) {
  if (!ids.length) {
    console.log(chalk.red('Need at least one id!'))
    return 1
  }

  console.log(chalk.cyan('Creating YouTube client…'))
  const yt = await youtube.getClient()

  await yt.normalizeVideos(ids, { verbose: true })

  console.log(chalk.green('All sermons normalized!'))

  return 0
}

cli.run('normalizeyoutube', main, {
  env: [
    'GCP_SERVICE_ACCOUNT_PRIVATE_KEY'
  ]
})
