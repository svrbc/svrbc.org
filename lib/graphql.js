import gridsome from './gridsome'
import Singleton from './singleton'

const client = new Singleton(async (app) => {
  return new Client(app)
}, { async: ['query'] })

class Client {
  constructor (app) {
    this.app = app
  }

  async init () {
    if (this.app) return
    this.app = await gridsome.createApp()
  }

  async query (q) {
    await this.init()
    return this.app.graphql(q)
  }
}

async function query (q) {
  return client.query(q)
}

export default {
  Client,
  query
}
