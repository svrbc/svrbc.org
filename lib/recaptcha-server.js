import FormData from 'form-data'
import fetch from 'node-fetch'

const RECAPTCHA_VERIFY_URL = 'https://www.google.com/recaptcha/api/siteverify'

async function verify (token, actions) {
  if (!process.env.RECAPTCHA_API_KEY) {
    console.log('reCAPTCHA API key not set')
    return
  }
  if (process.env.SVRBC_TEST === 'true') return

  const body = new FormData()
  body.append('secret', process.env.RECAPTCHA_API_KEY)
  body.append('response', token)
  const resp = await fetch(RECAPTCHA_VERIFY_URL, {
    method: 'POST',
    body
  })
  if (resp.status !== 200) { throw new Error(`Error ${resp.status} from reCAPTCHA server`) }
  const data = await resp.json()

  if (!data.success) throw new Error('reCAPTCHA token does not match site')
  if (!actions.includes(data.action)) {
    throw new Error(`Action "${data.action}" not found in ${actions}`)
  }
  console.log(`reCAPTCHA score: ${data.score}`)
  if (data.score < 0.5) throw new Error('reCAPTCHA score too low')
}

export default {
  verify
}
