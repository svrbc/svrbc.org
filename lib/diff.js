import chalk from 'chalk'
import { diffChars } from 'diff'

function colorDiff (a, b) {
  return diffChars(a, b)
    .map(p => p.added
      ? `+${chalk.green(p.value)}+`
      : p.removed
        ? `-${chalk.red(p.value)}-`
        : p.value)
    .join('')
}

export {
  colorDiff
}
