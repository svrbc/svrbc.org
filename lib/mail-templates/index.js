import counselingBody from './counseling.body.html.handlebars'
import counselingSubject from './counseling.subject.html.handlebars'
import dataBody from './data.body.html.handlebars'
import dataSubject from './data.subject.html.handlebars'
import mailerrorBody from './mailerror.body.html.handlebars'
import mailerrorSubject from './mailerror.subject.html.handlebars'
import plainBody from './plain.body.html.handlebars'
import plainSubject from './plain.subject.html.handlebars'
import smallgroupsReportBody from './smallgroups-report.body.html.handlebars'
import smallgroupsReportSubject from './smallgroups-report.subject.html.handlebars'

export default {
  data: {
    body: dataBody,
    subject: dataSubject
  },
  counseling: {
    body: counselingBody,
    subject: counselingSubject
  },
  mailerror: {
    body: mailerrorBody,
    subject: mailerrorSubject
  },
  plain: {
    body: plainBody,
    subject: plainSubject
  },
  'smallgroups-report': {
    body: smallgroupsReportBody,
    subject: smallgroupsReportSubject
  }
}
