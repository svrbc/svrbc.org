import chalk from 'chalk'

import cli from './cli'
import updateRoutine from './routines/updatefromyoutube'

async function main ({ _: [...ids] }) {
  if (!ids.length) {
    console.log(chalk.red('No ids provided!'))
    return 1
  }

  await updateRoutine(ids)

  return 0
}

cli.run('updatefromyoutube', main, {
  env: [
    'CI_PROJECT_NAME',
    'CI_REPOSITORY_URL',
    'CI_SERVER_URL',
    'SVRBC_BOT_GPG_PRIVATE_KEY',
    'SVRBC_BOT_PASSWORD'
  ]
})
