import chalk from 'chalk'
import dotenv from 'dotenv'
import minimist from 'minimist'

import Timer from '../src/lib/timer'

function env () {
  try {
    const dotenvResult = dotenv.config()
    if (dotenvResult.error) {
      throw dotenvResult.error
    }
  } catch (e) {
    if (e.code !== 'ENOENT') throw e
  }
}

async function run (name, main, opts) {
  const timer = new Timer()
  opts = Object.assign({
    summary: true,
    env: []
  }, opts)
  const argv = minimist(process.argv.slice(2))
  env()

  const missingEnv = opts.env.filter(v => !process.env[v])
  missingEnv.forEach(v => {
    console.log(chalk.red(`$${v} must be defined to use this command`))
  })
  if (missingEnv.length) process.exit()

  let ret = -1
  try {
    ret = await main(argv)
  } catch (e) {
    console.error(e)
    console.error(e.stack)
  }
  if (opts.summary) timer.pretty(name)
  process.exit(ret)
}

export default {
  env,
  run
}
