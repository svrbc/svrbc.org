import chalk from 'chalk'

import sermons from '../sermons'
import youtube from '../youtube-server'
import gitlab from '../gitlab-server'

export default async (ids, opts) => {
  opts = Object.assign({
    onepass: false
  }, opts)
  const gl = await gitlab.getClient({
    prefix: 'updatefromyoutube'
  })
  const yt = await youtube.getClient()

  let normalized = []
  let updated = []
  const ret = () => ({ normalized, updated })

  try {
    console.log(chalk.cyan('Fetching sermon manifest from GitLab…'))
    const manifest = await gl.fetchSermonManifest()

    console.log(chalk.cyan(`Fetching videos from YouTube (${ids.join()})…`))
    const toNormalize = await yt.getVideos(ids)

    console.log(chalk.cyan('Normalizing videos…'))
    normalized = await yt.normalizeVideos(toNormalize, {
      preachers: manifest.preachers,
      series: manifest.series
    })
    if (normalized.length) {
      console.log(chalk.green(`Normalized ${normalized.join()}.`))
      if (opts.onepass) return ret()
    } else {
      console.log(chalk.green('No videos normalized!'))
    }

    console.log(chalk.cyan('Fetching audio length…'))
    const toUpdate = await Promise.all(toNormalize
      .filter(v => !normalized.includes(v.id))
      .map(v => yt.parseSermon(v, {
        preachers: manifest.preachers,
        series: manifest.series
      }))
      .filter(s => !!s)
      .map(s => sermons.populateAudioBytes(s))
    )

    updated = toUpdate.map(v => v.id)
    const mods = sermons.fileMods(toUpdate, manifest)

    if (mods.length) {
      console.log(chalk.cyan('Posting commit info to GitLab…'))
      await gl.createCommit(mods, 'Update sermons from YouTube (automated)')
    } else {
      console.log(chalk.green('No new changes!'))
      return ret()
    }

    console.log(chalk.green('Finished!'))
  } catch (e) {
    console.log(e)
    console.log(e.stack)
  } finally {
    console.log(chalk.cyan('Cleaning up…'))
    await gl.cleanup()
  }
  return ret()
}
