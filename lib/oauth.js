import { google } from 'googleapis'

import oauth from '../src/lib/oauth'

const GCP_CLIENT_ID = '96942811577-4l30equ3in7bno3a7a36p4d32h77npvk.apps.googleusercontent.com'

async function getClient (origin, opts) {
  const client = new google.auth.OAuth2(
    GCP_CLIENT_ID,
    process.env.GCP_CLIENT_SECRET,
    oauth.getCallbackURL(origin)
  )
  if (opts.code) {
    const x = await client.getToken(opts.code)
    // const { tokens } = await client.getToken(opts.code)
    const tokens = x.tokens
    client.setCredentials(tokens)
  }
  if (opts.refresh_token) {
    client.setCredentials(opts)
  }
  return client
}

function getAuthURL (origin) {
  const client = getClient(origin)
  return client.generateAuthUrl({
    access_type: 'offline',
    scope: [
      'https://www.googleapis.com/auth/youtube'
    ]
  })
}

export default {
  getClient,
  getAuthURL
}
