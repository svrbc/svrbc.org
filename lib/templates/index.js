import { promises as fs } from 'fs'
import handlebars from 'handlebars'
import path from 'path'

async function load (name) {
  const p = path.join(__dirname, `${name}.handlebars`)
  const src = await fs.readFile(p, 'utf8')
  return handlebars.compile(src)
}

export default {
  load
}
