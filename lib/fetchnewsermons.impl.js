import chalk from 'chalk'
import { promises as fs } from 'fs'
import gql from 'graphql-tag'
import path from 'path'
import rimraf from 'rimraf-promise'

import datetime from '../src/lib/datetime'
import cli from './cli'
import graphql from './graphql'
import youtube from './youtube-server'

const WEEK = 7 * 24 * 60 * 60 * 1000
const NEW_SERMONS_DIR = '/tmp/newsermons'

// This returns the date of the second to most recent sermon if it is more than
// one week old.  Otherwise, it returns null.
// This is useful for establishing if we already have both of this week's
// sermons.
async function getNeededSermonDate () {
  const res = await graphql.query(gql`{
    allSermon (
      sortBy: "date"
      limit: 2
    ) {
      edges {
        node {
          date
        }
      }
    }
  }`)
  const oldSermons = res.data.allSermon.edges.map(e => e.node)
  const date = datetime.fromISO(oldSermons[1].date)
  const weekAgo = new Date(Date.now() - WEEK)
  return date > weekAgo ? null : date
}

async function newSermonIds (ids) {
  // This should filter based on ids, but for now it just grabs all sermons.
  const res = await graphql.query(gql`{
    allSermon {
      edges {
        node {
          id
        }
      }
    }
  }`)
  const allIds = res.data.allSermon.edges.map(e => e.node.id)
  return ids.filter(i => !allIds.includes(i))
}

// TODO: If we keep this script around, we should use the functions in
// lib/sermons.js.
async function getSeriesTitleMap (titles) {
  // This should filter based on titles, but for now it just grabs all series.
  const res = await graphql.query(gql`{
    allSermonSeries {
      edges {
        node {
          id
          title
        }
      }
    }
  }`)
  const series = res.data.allSermonSeries.edges.map(e => e.node)
  return Object.fromEntries(series.map(s => [s.title, s.id]))
}

async function handleSermonData (data) {
  const filename = `${data.id}-${data.slug}.md`
  await fs.writeFile(path.join(NEW_SERMONS_DIR, filename), `---
id: ${data.id}
date: ${data.date}
text: ${data.text}
preacher: ${data.preacher}
series: ${data.series}
audio: ${data.audio}
youtube: ${data.youtube}
---

# ${data.title}

${data.content}
`.trim() + '\n')
}

async function main () {
  const after = await getNeededSermonDate()
  if (!after) {
    console.log(chalk.green('Already have two sermons from the past week.'))
    return 0
  }

  console.log(chalk.cyan(`Getting all videos after ${after}...`))
  const newVideos = await youtube.getNewVideos(after)
  console.log(chalk.cyan(`Got ${newVideos.length} videos`))
  let sermonData = newVideos.map(v => {
    const data = youtube.parseSermonDataFromVideo(v)
    const url = youtube.urlFromVideo(v)
    if (!data) {
      console.log(chalk.cyan(`${v.snippet.title} (${url}) ` +
        'does not have sermon-related metadata.'))
    }
    return data
  })
    .filter(d => !!d)
  if (!sermonData.length) {
    console.log(chalk.green('No new sermons found'))
    return 0
  }

  const seriesMap = await getSeriesTitleMap(sermonData.map(s => s.series))
  sermonData.forEach(s => {
    const title = s.series
    s.series = seriesMap[title]
    if (!s.series) {
      throw Error(`series "${title}" does not exist (${s.youtubeUrl})`)
    }
  })

  // It is possible that we already have some of these sermons, so let's test
  // first.
  const newIds = await newSermonIds(sermonData.map(s => s.id))
  sermonData = sermonData.filter(s => newIds.includes(s.id))
  if (!sermonData.length) {
    console.log(chalk.green('No new sermons found'))
    return 0
  }

  await rimraf(NEW_SERMONS_DIR)
  fs.mkdir(NEW_SERMONS_DIR)

  for (const s of sermonData) {
    console.log(chalk.green(`adding "${s.title}" (${s.youtubeUrl})`))
    await handleSermonData(s)
  }

  return 0
}

cli.run('fetchsermons', main)
