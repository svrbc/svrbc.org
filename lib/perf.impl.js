import chalk from 'chalk'
import { launch as launchChrome } from 'chrome-launcher'
import lighthouse from 'lighthouse'

import Timer from '../src/lib/timer'

const URL = 'https://staging.svrbc.org'

const TESTS = [
  {
    path: '/',
    minScores: {
      'service-worker': 0,
      'works-offline': 0,
      'first-contentful-paint': 0.87,
      'first-meaningful-paint': 0.87,
      'speed-index': 0.78,
      'estimated-input-latency': 0.03,
      'total-blocking-time': 0.07,
      'max-potential-fid': 0.03,
      'first-cpu-idle': 0.73,
      interactive: 0.41,
      'mainthread-work-breakdown': 0.29,
      'bootup-time': 0.72,
      'uses-rel-preconnect': 0.52,
      'font-display': 0,
      'offline-start-url': 0,
      'offscreen-images': 0.88,
      'color-contrast': 0,
      'uses-long-cache-ttl': 0.97,
      'unused-css-rules': 0.67,
      'uses-webp-images': 0.32,
      'uses-optimized-images': 0.38,
      'no-vulnerable-libraries': 0,
      'link-text': 0
    }
  }
]

async function main () {
  const timer = new Timer()

  const chrome = await launchChrome({
    chromeFlags: ['--headless', '--disable-gpu']
  })

  let ret = 0
  for (const test of TESTS) {
    const results = await lighthouse(URL + test.path, { port: chrome.port })
    for (const result of Object.values(results.lhr.audits)) {
      let color
      if (result.id in test.minScores) {
        const min = test.minScores[result.id]
        if (result.score < min) {
          color = chalk.red
          ret = 1
        } else if (result.score > min) {
          color = chalk.green
        } else {
          color = chalk.cyan
        }
      } else if (result.score === 1 || result.score === null) {
        continue
      } else {
        color = chalk.red
        ret = 1
      }

      console.log(color(result.id), result.score)
      console.log(result.description)
    }
  }

  await chrome.kill()

  timer.pretty('perf')
  return ret
}

main().then(process.exit, console.error)
