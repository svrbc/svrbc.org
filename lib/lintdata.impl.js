import chalk from 'chalk'
import path from 'path'
import { promises as fs } from 'fs'
import stringify from 'json-stable-stringify'
import gql from 'graphql-tag'
import matter from 'gray-matter'

import confessionChecks from '../src/confession/checks'
import preacherChecks from '../src/preachers/checks'
import sermonChecks from '../src/sermons/checks'
import seriesChecks from '../src/series/checks'
import cli from './cli'
import { colorDiff } from './diff'
import graphql from './graphql'
import sermons from './sermons'

const src = path.join(__dirname, '..', 'src')

const DATA = [
  confessionChecks,
  preacherChecks,
  sermonChecks,
  seriesChecks
]
  .map(d => Object.assign({
    nodeChecks: [],
    collectionChecks: [],
    normalizeFilename: n => path.basename(n.fileInfo.path)
  }, d))

function checkUniqueIds (nodes) {
  const ids = {}
  let ret = true
  const messages = []
  for (const n of nodes) {
    if (n.id in ids) {
      messages.push(`duplicate id ${n.id} in ${n.fileInfo.path} ` +
        `(${ids[n.id]})`)
      ret = false
    }
    ids[n.id] = n.fileInfo.path
  }
  return ret
}

async function checkFormat (node, warn, data, fix) {
  const formattedFilename = data.normalizeFilename(node)
  const filepath = path.join(__dirname, '..', 'src', node.fileInfo.path)
  const formattedFilepath =
    path.join(path.dirname(filepath), formattedFilename)
  const text = await fs.readFile(filepath, 'utf8')
  const opts = {}
  if (node.fileInfo.path.endsWith('.md')) {
    const { data, content } = matter(text)
    opts.data = data
    opts.content = content
  }

  const formattedContent = data.normalizeContent(node, opts)
  const tcontent = text.replace(/(# .*?)\n.*/s, '$1')
  const tformatted = formattedContent.replace(/(# .*?)\n.*/s, '$1')

  let write = false
  if (tcontent !== tformatted) {
    if (fix) {
      write = true
    } else {
      warn('content is not formatted correctly')
      console.log(colorDiff(tcontent, tformatted))
    }
  }
  if (filepath !== formattedFilepath) {
    if (fix) {
      await fs.unlink(filepath)
      write = true
    } else {
      warn('filename is not formatted correctly ' +
        `(should be ${formattedFilepath})`)
    }
  }
  if (write) await fs.writeFile(formattedFilepath, formattedContent)
}

async function main ({ fix }) {
  const queries = DATA
    .map(d => `all${d.type} {
      edges {
        ${d.query.replace(/\n/g, '        ')}
      }
    }`)
  const res = await graphql.query(gql`{
    ${queries.join('\n')}
  }`)

  let ret = 0
  // Some checks could break from an automated commit and should not run on
  // regular CI
  const offline = process.env.JOB_TYPE === 'lintdata'
  for (const d of DATA) {
    const nodes = res.data[`all${d.type}`].edges.map(e => e.node)
    const warn = (message, node) => {
      ret = 1
      const prefix = node ? `src/${node.fileInfo.path}` : d.type
      console.log(chalk.red(`${prefix}: ${message}`))
    }
    checkUniqueIds(nodes, warn)
    for (const n of nodes) {
      const nodeWarn = (message) => warn(message, n)
      for (const c of d.nodeChecks) {
        await c(n, nodeWarn, fix, offline)
      }
      await checkFormat(n, nodeWarn, d, fix)
    }
    for (const c of d.collectionChecks) {
      await c(nodes, warn, fix, offline)
    }
  }

  const manifestGen = await sermons.readSrc()
  const manifestDisk = await sermons.readManifest()
  const manifestGenJson = stringify(manifestGen, { space: 2 })
  const manifestDiskJson = stringify(manifestDisk, { space: 2 })
  if (manifestGenJson !== manifestDiskJson) {
    if (fix) {
      const filename = path.join(src, 'sermons', 'manifest.json')
      await fs.writeFile(filename, manifestGenJson + '\n')
    } else {
      console.log(chalk.red('src/sermons/manifest.json is not correct'))
      ret = 1
    }
  }

  if (ret) {
    console.log(chalk.red('Data not normalized'))
  } else {
    console.log(chalk.green('Data normalized'))
  }

  return ret
}

cli.run('lintdata', main)
