class Singleton {
  constructor (func, opts) {
    this._func = func
    opts = Object.assign({
      async: [],
      sync: []
    }, opts)
    for (const name of opts.sync) {
      this[name] = (...args) => {
        if (this.val === undefined) {
          throw new Error('used a sync function without calling .init() first!')
        }
        return this.val[name](...args)
      }
    }
    for (const name of opts.async) {
      this[name] = async (...args) => {
        const instance = await this.get()
        return instance[name](...args)
      }
    }
  }

  async init () {
    if (this._valp === undefined) {
      this._valp = Promise.resolve(this._func())
    }
    this.val = await this._valp
  }

  async get () {
    await this.init()
    return this.val
  }
}

export default Singleton
