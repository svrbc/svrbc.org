/* eslint-env jest */
import dataformat from './dataformat'

describe('formatMd', () => {
  it('extra ignored', () => {
    const obj = {
      id: 'myid',
      v1: 'value1',
      extra: 'extra'
    }
    const keys = ['id', 'v1']
    expect(dataformat.formatMd(obj, keys)).toEqual(`---
id: myid
v1: value1
---
`)
  })

  it('keys custom sorted', () => {
    const obj = {
      id: 'myid',
      v1: 'value1',
      v2: 'value2'
    }
    const keys = ['id', 'v2', 'v1']
    expect(dataformat.formatMd(obj, keys)).toEqual(`---
id: myid
v2: value2
v1: value1
---
`)
  })

  it('removes quotes', () => {
    const obj = {
      date: '2020-01-01'
    }
    const keys = ['date']
    const opts = { noquote: keys }
    expect(dataformat.formatMd(obj, keys, opts)).toEqual(`---
date: 2020-01-01
---
`)
  })
})
