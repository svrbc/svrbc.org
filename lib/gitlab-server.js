import fetch from 'node-fetch'
import querystring from 'querystring'

import sermons from './sermons'

const GITLAB_HOST = 'gitlab.com'
const GITLAB_PATH = 'svrbc/svrbc.org'
const GITLAB_PROJECT_ID = 10743206
const GITLAB_USER = 'svrbc_bot'
const GITLAB_NAME = 'SVRBC Bot'
const GITLAB_EMAIL = 'bot@svrbc.org'
const GITLAB_API = 'https://gitlab.com/api/v4'

class GitlabClient {
  constructor (opts) {
    if (!process.env.SVRBC_BOT_PASSWORD) {
      console.error('SVRBC_BOT_PASSWORD is not defined')
    }
    opts = Object.assign({
      host: GITLAB_HOST,
      path: GITLAB_PATH,
      user: GITLAB_USER,
      projectId: GITLAB_PROJECT_ID,
      pass: process.env.SVRBC_BOT_PASSWORD,
      prefix: 'git'
    }, opts)
    const auth = `https://${opts.user}:${opts.pass}@${opts.host}`
    this.url = `${auth}/${opts.path}.git`
    this.projectId = opts.projectId
    this.prefix = opts.prefix
  }

  async readFiles () {
    return sermons.readSrc(this.srcDir)
  }

  async fetch (path, opts) {
    opts = Object.assign({
      method: 'GET',
      data: null
    }, opts)
    if (!process.env.GITLAB_ACCESS_TOKEN) {
      console.error('GITLAB_ACCESS_TOKEN is not defined')
    }
    const url = `${GITLAB_API}/${path}`
    const fetchOpts = {
      method: opts.method,
      headers: {
        'PRIVATE-TOKEN': process.env.GITLAB_ACCESS_TOKEN
      }
    }
    if (opts.data) {
      fetchOpts.headers['Content-Type'] = 'application/json'
      fetchOpts.body = JSON.stringify(opts.data)
    }
    const resp = await fetch(url, fetchOpts)
    if (!resp.ok) {
      throw new Error(`${url}: ${resp.statusText}`)
    }
    return resp.json()
  }

  async fetchFile (path, ref = 'master') {
    const uriSafePath = querystring.escape(path)
    path = `projects/${this.projectId}/repository/files/${uriSafePath}/raw`
    return this.fetch(`${path}?ref=${ref}`)
  }

  async fetchSermonManifest () {
    return this.fetchFile('src/sermons/manifest.json')
  }

  async createCommit (actions, msg, opts) {
    opts = Object.assign({
      branch: 'master'
    }, opts)
    return this.fetch(`projects/${this.projectId}/repository/commits`, {
      method: 'POST',
      data: {
        branch: opts.branch,
        commit_message: msg,
        author_email: GITLAB_EMAIL,
        author_name: GITLAB_NAME,
        actions
      }
    })
  }

  async cleanup () {
  }
}

function getClient (opts = {}) {
  return new GitlabClient(opts)
}

export default {
  getClient
}

export {
  GitlabClient
}
