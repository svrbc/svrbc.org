import defer from 'p-defer'
import dictionary from 'dictionary-en'
import { promises as fs } from 'fs'
import path from 'path'
import retext from 'retext'
import spell from 'retext-spell'

import Singleton from './singleton'

const spellChecker = new class extends Singleton {
  async process (text) {
    const instance = await this.get()
    const sp = defer()
    instance.process(text, (err, file) => sp.resolve([err, file]))
    const [err, file] = await sp.promise
    if (err) throw err
    return file
  }
}(async () => {
  const words = await fs.readFile(path.join(__dirname, '..', 'words.txt'))
  return retext().use(spell, {
    dictionary,
    personal: words,
    ignore: [
      'img',
      'src'
    ],
    ignoreDigits: true,
    ignoreLiteral: true
  })
})

async function spellCheck (text, ignore = []) {
  text = text
    .replace(/\n/g, ' ')
    .replace(/&\w*;/g, '')
    .trim()
  if (!text.match(/\w/)) return []
  const file = await spellChecker.process(text)
  const messages = []

  for (const m of file.messages) {
    // If there is no m.actual, then it's just a warning about capacity.
    if (!m.actual) continue
    if (m.actual.match(/[0-9@.:]/)) continue
    if (ignore.includes(m.actual)) continue
    const subwords = m.actual.split('-')
    if (subwords.length > 1) {
      const newMessages = await spellCheck(subwords.join(' '), ignore)
      messages.push(...newMessages)
      continue
    }

    let expected = ''
    if (m.expected) expected = ` (${m.expected})`
    messages.push(`${m.message}${expected}`)
  }
  return messages
}

export {
  spellCheck
}
