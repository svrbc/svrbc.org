import chalk from 'chalk'
import { promises as fs } from 'fs'
import path from 'path'

import cli from './cli'
import youtube from './youtube-server'

async function main () {
  const videos = await youtube.getAllVideos()
  for (const v of videos) {
    v.sermon = youtube.parseSermonDataFromVideo(v)
  }

  const outFile = path.join(__dirname, '..', 'youtube.json')
  console.log(chalk.cyan('Writing youtube.json…'))
  await fs.writeFile(outFile, JSON.stringify(videos, null, 2))

  return 0
}

cli.run('listyoutube', main)
