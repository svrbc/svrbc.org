// If we don't import the dist file, we get this error:
// require.extensions is not supported by webpack. Use a loader instead.
// But configuring webpack for netlify-lambda and jest is too hard at the
// moment.
import LRU from 'lru-cache'
import fetch from 'node-fetch'
import querystring from 'querystring'
import xss from 'xss'

const API_ROOT = 'https://api.esv.org/v3/'
const PASSAGE_HTML_ENDPOINT = 'passage/html/'

const cache = new LRU({
  max: 500
})

function apiUrl (path, params) {
  const qStr = querystring.stringify(Object.assign({
    'include-audio-link': 'false',
    'include-book-titles': 'false',
    'include-footnotes': 'false',
    'include-headings': 'false',
    'include-short-copyright': 'false'
  }, params))
  return `${API_ROOT}${path}?${qStr}`
}

// https://api.esv.org/docs/passage-html/
function getEndpoint (req) {
  if (!req.q) throw new Error('query is missing in request')
  return apiUrl(PASSAGE_HTML_ENDPOINT, {
    q: req.q
  })
}

function mockQuery (endpoint) {
  console.log('=== ESV query ===')
  console.log(endpoint)
  console.log('=================')
  return {
    passages: ['God helps those who help themselves']
  }
}

async function query (endpoint) {
  const res = cache.get(endpoint)
  if (res) {
    console.log(`cache hit (${endpoint})`)
    return res
  }

  if (!process.env.ESV_API_KEY) {
    console.error('ESV API key not set')
    return mockQuery(endpoint)
  }
  if (process.env.SVRBC_TEST) return mockQuery(endpoint)

  console.log(`fetching (${endpoint})`)
  const resp = await fetch(endpoint, {
    headers: {
      Authorization: `Token ${process.env.ESV_API_KEY}`
    }
  })

  const data = await resp.json()
  if (resp.status !== 200) {
    console.error(data)
    throw new Error(`Error ${resp.status} from ESV API server`)
  }

  data.passages = data.passages.map(xss)
  cache.set(endpoint, data)
  return data
}

export default {
  getEndpoint,
  query
}
