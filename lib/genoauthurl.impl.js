import cli from './cli'
import oauth from './oauth'

async function main ({ _: [...ids] }) {
  console.log(oauth.getAuthURL('https://svrbc.org'))

  return 0
}

cli.run('genoauthurl', main, [
  'GCP_CLIENT_ID',
  'GCP_CLIENT_SECRET'
])
