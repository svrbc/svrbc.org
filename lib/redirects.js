import { promises as fs } from 'fs'
import gql from 'graphql-tag'
import path from 'path'

import graphql from './graphql'

async function generate () {
  const res = await graphql.query(gql`{
    allArticle {
      edges {
        node {
          id
          path
        }
      }
    }
    allSermon {
      edges {
        node {
          id
          path
        }
      }
    }
    allSermonSeries {
      edges {
        node {
          id
          path
        }
      }
    }
  }`)

  const inFile = path.join(__dirname, '..', 'redirects.txt')

  const header = await fs.readFile(inFile, 'utf8')
  const lines = Object.entries({
    articles: res.data.allArticle.edges,
    sermons: res.data.allSermon.edges,
    series: res.data.allSermonSeries.edges
  })
    .map(([k, v]) => v.map(e => [k, e.node]))
    .flat()
    .map(([k, n]) => `/${k}/${n.id}/* ${n.path}`)
    .join('\n')

  return `${header}${lines}\n`
}

export default {
  generate
}

export {
  generate
}
