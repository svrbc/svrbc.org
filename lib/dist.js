import { promises as fs } from 'fs'
import path from 'path'
import { promisify } from 'util'

import glob from 'glob-promise'
import { Parser } from 'htmlparser2'
import _imageSize from 'image-size'

const imageSize = promisify(_imageSize)

const DIST_PATH = path.join(__dirname, '..', 'dist')

async function parseHtml (filepath, data) {
  data.type = 'html'
  const contents = await fs.readFile(filepath, 'utf8')

  const ignoreTags = [
    'code',
    'script',
    'style'
  ]
  const tags = [
    'a',
    'div',
    'em',
    'form',
    'h1',
    'h2',
    'h3',
    'h4',
    'h5',
    'h6',
    'h7',
    'html',
    'img',
    'main',
    'section',
    'span',
    'strong',
    'title'
  ]
  const tagStack = Object.fromEntries(tags.map(t => [t, 0]))
  ignoreTags.forEach(name => { tagStack[name] = 0 })
  tags.forEach(t => { data[t] = [] })
  const realStack = []

  const parser = new Parser({
    onopentag (name, attribs) {
      if (name in tagStack) {
        tagStack[name] += 1
      }
      if (name in data) {
        const obj = {
          name,
          attribs,
          text: [],
          open: true,
          parents: [...realStack]
        }
        data[name].push(Object.assign({
          stack: realStack.slice(0)
        }, obj))
        realStack.push(obj)
      }
    },
    ontext (text) {
      if (ignoreTags.some(name => tagStack[name])) return
      Object.entries(tagStack)
        .filter(([name, v]) => v)
        .map(([name, v]) => data[name][data[name].length - 1])
        .filter(e => e.open)
        .filter(e => e.name !== 'html' ||
          !realStack.some(obj => obj.attribs.nospell !== undefined))
        .forEach(e => {
          e.text.push(text)
        })
    },
    onclosetag (name) {
      if (name in tagStack) {
        tagStack[name] -= 1
      }
      if (name in data) {
        const elem = data[name][data[name].length - 1]
        elem.open = false
        realStack.pop()
      }
    }
  })
  parser.write(contents)
  parser.end()

  data.titleText = data.title
    .map(t => t.text)
    .flat()
    .join(' ')
    .replace(/ - SVRBC$/, '')
    .replace(/:/g, '')
    .replace(/\u00A0/g, ' ') // nbsp -> normal space
    .split(' | ')
    .reverse()
    .join(' ')
    .replace(/\s+/g, ' ')
    .trim()
  data.h1Text = data.h1
    .map(t => t.text)
    .flat()
    .join(' ')
    .replace(/:/g, '')
    .replace(/\s+/g, ' ')
    .replace(/ link$/, '')
    .trim()
  data.ignoreWords = data.div
    .filter(d => d.attribs.class === 'spell-ignore')
    .map(t => t.text)
    .flat()
    .map(s => s.split(/\s+/))
    .flat()
    .filter(s => s.length)
  data.allText = data.html
    .filter(e => !e.nospell)
    .map(e => e.text)
    .flat()
    .join(' ')
  data.a
    .filter(a => !a.attribs.href.match(/^[a-z]*:/))
    .forEach(a => {
      a.path = path.resolve(data.reldir, a.attribs.href.replace(/#.*?$/, ''))
    })
  data.img
    .filter(i => !i.attribs.src.match(/^[a-z]*:/))
    .forEach(i => { i.path = path.resolve(data.reldir, i.attribs.src) })
  data.img
    .forEach(i => {
      i.basename = i.attribs.src
        .match(/[^/]*$/)[0]
        .replace(/\.[0-9a-f]{7}\.[0-9a-f]{32}/, '')
    })
  data.timeperiods = data.span
    .filter(s => s.attribs.class && s.attribs.class.match(/\btimeperiod\b/))
    .map(s => s.text.join(''))
  return data
}

async function parseImg (filepath, data) {
  data.type = 'img'
  await Promise.all([
    (async () => {
      try {
        data.dims = await imageSize(filepath)
      } catch (e) {
        console.error(`Could not get dimensions for ${filepath}: ${e.message}`)
        console.error(e.stack)
      }
    })(),
    (async () => {
      const stat = await fs.stat(filepath)
      data.bytes = stat.size
    })()
  ])
  return data
}

async function scan (callback) {
  const paths = await glob(path.join(DIST_PATH, '**', '*'))
  return Promise.all(paths.map(async p => {
    const relpath = p.replace(DIST_PATH, '')
    const data = {
      path: p,
      relpath,
      reldir: path.dirname(relpath)
    }
    if (relpath.match(/\.html$/)) {
      await parseHtml(p, data)
    } else if (relpath.match(/\.jpg$/)) {
      await parseImg(p, data)
    }
    return callback(data)
  }))
}

export default {
  scan,
  path: DIST_PATH
}
