import { promises as fs } from 'fs'
import path from 'path'

import chalk from 'chalk'
import glob from 'glob-promise'

import { colorDiff } from './diff'
import dist from './dist'
import Singleton from './singleton'
import { spellCheck } from './spell'

// BEGIN Singletons ////////////////////////////////////////////////////////////

const emailWhitelist = new Singleton(async () => {
  const p = path.join(__dirname, '..', 'emails.txt')
  const emails = await fs.readFile(p, 'utf8')
  return new Set(emails.trim().split(/\s+/))
}, { sync: ['has'] })

const allPaths = new Singleton(async () => {
  const val = new Set()
  for (const p of await glob(path.join(dist.path, '**'))) {
    const rel = p.replace(dist.path, '')
    val.add(rel)
    val.add(rel.replace(/\/index.html$/, '/'))
  }
  return val
}, { sync: ['has'] })

function isSorted (arr) {
  const sorted = arr.slice(0)
  sorted.sort()
  return JSON.stringify(arr) === JSON.stringify(sorted)
}

const CHECKS = {
  html: [
    // h3+ tags
    data => [].concat(data.h3, data.h4, data.h5, data.h6, data.h7)
      .filter(_ => data.relpath !== '/constitution/index.html')
      .map(h => `${h.name} tag too deep (should be h1 or h2)`),
    // missing h1 tag
    data => [data]
      .filter(d => d.relpath !== '/index.html')
      .filter(d => !d.relpath.startsWith('/test/'))
      .filter(d => !d.relpath.startsWith('/data/'))
      .filter(d => !d.h1.length)
      .map(d => 'there should be at least one h1 tag'),
    // multiple h1 tags
    data => data.h1.slice(1).map(h => `extra h1 tag ${h.text.join(' ')}`),
    // anchored h1 tags
    data => data.h1.filter(h => h.attribs.id)
      .map(h => `h1 tag should not have id (${h.attribs.id})`),
    // mismatched title
    data => [data]
      .filter(d => d.h1.length)
      .filter(d => d.h1Text !== d.titleText)
      .map(d => colorDiff(d.h1Text, d.titleText))
      .map(c => `h1 tag should match title ${c}`),
    // anchorless h2 tags
    data => data.h2
      .filter(h => !h.attribs.id)
      .filter(h => !(h.attribs.class || '').match(/\bsubtitle\b/))
      .map(h => `h2 tag (${h.text.join(' ')}) should always have explicit ` +
        'ids ({#anchor} in markdown)'),
    // sectionless h2 tags
    data => data.h2
      .filter(h => !h.parents.some(p => p.name === 'section'))
      .filter(h => !h.parents.some(p => p.name === 'form'))
      .map(h => `h2 tag (${h.text.join(' ')}) is not contained in a section or a form`),
    // altless images
    data => data.img.filter(i => !i.attribs.alt)
      .map(i => 'img tags should always have alt text.'),
    // extra spell-ignores
    data => data.div
      .filter(d => d.attribs.class === 'spell-ignore')
      .slice(1)
      .map(s => 'extra <spell-ignore> component.'),
    // known spell-ignore words
    async data => {
      const messages = []
      for (const w of data.ignoreWords) {
        if ((await spellCheck(w)).length) continue
        messages.push(`unecessary word "${w}" in <spell-ignore>`)
      }
      return messages
    },
    // superfluous spell-ignore words
    data => data.ignoreWords
      .filter(w => !data.allText.includes(w))
      .map(w => `word "${w}" in <spell-ignore> not found in document`),
    // unsorted spell-ignore
    data => [data]
      .filter(d => !isSorted(d.ignoreWords))
      .map(d => '<spell-ignore> is unsorted'),
    // spell check
    data => spellCheck(data.allText, data.ignoreWords),
    // lower case am/pm
    data => (data.allText.match(/\b[APap]\.[Mm]\./g) || [])
      .slice(data.timeperiods.length)
      .map(t => 'am/pm should be in a timeperiod span'),
    // incorrect time periods
    data => data.timeperiods
      .filter(t => !t.match(/[ap]m/g))
      .map(t => `.timeperiod ${t} should be either "am" or "pm"`),
    // broken imgs
    data => data.img
      .filter(i => i.path)
      .filter(i => !allPaths.has(i.path))
      .map(i => `non-existent image: ${i.attribs.src}`),
    // misformatted testimonial imgs
    data => data.img
      .filter(i => i.path)
      .filter(i => !i.basename.match(/\.testimonial\.jpg$/))
      .filter(i => i.stack.some(t =>
        t.attribs.class && t.attribs.class.match(/testimonial/)))
      .map(i => `image should be named .testimonial.jpg: ${i.basename}`),
    // broken internal links
    data => data.a
      .filter(a => a.path)
      .filter(a => !allPaths.has(a.path))
      .map(a => `broken link: ${a.attribs.href} ${a.path}`),
    // internal links without slashes
    data => data.a
      .filter(a => a.path)
      // before the hash we should have either:
      // - nothing,
      // - a slash, or
      // - a three or four character file extension.
      .filter(a => !a.attribs.href.match(/(^|\/|\..{3,4})(#.*)?$/))
      .map(a => `internal links should end with a slash ${a.attribs.href}`),
    // unknown mailto links
    data => data.a
      .map(a => a.attribs.href)
      .filter(h => h.match(/^mailto:/))
      .map(h => h.replace('mailto:', ''))
      .filter(e => !emailWhitelist.has(e))
      .map(e => `"${e}" not present in emails.txt`),
    // external links that don't open in a new tab
    data => data.a
      .filter(a => a.attribs.href.match(/^https?:/))
      // FUTURE(v0.3): We should probably remove these sermon filters.
      .filter(a => !a.attribs.href.match(/^https:\/\/s3-us-west-1.amazonaws.com\/pbc-sermons/))
      .filter(a => !a.attribs.href.match(/^https:\/\/storage.googleapis.com\/pbc-ca-sermons/))
      .filter(a => a.attribs.target !== '_blank')
      .map(a => `link does not open in new tab (${a.attribs.href})`),
    // external links that don't use noopener/noreferrer
    data => data.a
      .filter(a => a.attribs.href.match(/^https?:/))
      .filter(a => !['noopener', 'noreferrer'].includes(a.attribs.rel))
      .map(a => 'link does not use rel="noopener" or rel="noreferrer" ' +
        `(${a.attribs.href})`),
    // error-message class
    data => data.div
      .filter(d => (d.attribs.class || '').match(/error-message/))
      .map(d => 'div has error-message class')
  ]
}

async function main () {
  const start = Date.now()

  await emailWhitelist.init()
  await allPaths.init()

  const results = await dist.scan(async data => {
    const checks = CHECKS[data.type]
    if (!checks) return true
    const messages = (await Promise.all(checks.map(c => c(data)))).flat()
    if (messages.length === 0) return true

    console.log(chalk.cyan(data.relpath))

    for (let message of messages) {
      message = message.replace(/(^|\n)/g, '$1  ')
      console.log(chalk.red(`${message}`))
    }

    return false
  })
  const ret = results.every(v => v) ? 0 : 1

  // Finish.
  const seconds = (Date.now() - start) / 1000
  console.log(`lintdist took ${seconds}s`)
  return ret
}

main().then(process.exit, console.error)
