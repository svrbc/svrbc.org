import chalk from 'chalk'
import childProcess from 'child_process'
import { promises as fs } from 'fs'
import gql from 'graphql-tag'
import handlebars from 'handlebars'
import path from 'path'
import tmp from 'tmp-promise'

import cli from './cli'
import graphql from './graphql'

const TEMPLATE_PATH = path.join(
  __dirname, 'templates', 'confessionquiz.tex.handlebars')
const OUTPUT_PATH = path.join(__dirname, '..', 'confession-quiz.pdf')

async function main () {
  const templateText = await fs.readFile(TEMPLATE_PATH, 'utf8')
  const template = handlebars.compile(templateText)

  const res = await graphql.query(gql`{
    allConfessionQuiz(sort: {by: "lbcf", order: ASC}) {
      edges {
        node {
          lbcf {
            id
            chapter {
              id
            }
            paragraph
          }
          question
          options {
            a {
              text
              correct
            }
            b {
              text
              correct
            }
            c {
              text
              correct
            }
            d {
              text
              correct
            }
          }
        }
      }
    }
  }`)

  const nodes = res.data.allConfessionQuiz.edges.map(e => e.node)
  nodes.forEach(n => {
    Object.values(n.options).forEach(o => {
      o.text = o.text.replace('\n', '\\newline{}')
    })
  })
  nodes.sort((a, b) => {
    if (parseInt(a.lbcf.chapter.id) > parseInt(b.lbcf.chapter.id)) return 1
    if (parseInt(a.lbcf.chapter.id) < parseInt(b.lbcf.chapter.id)) return -1
    if (a.lbcf.paragraph > b.lbcf.paragraph) return 1
    if (a.lbcf.paragraph < b.lbcf.paragraph) return -1
    return 0
  })
  const tex = template({ nodes })

  let ret = 0
  await tmp.withDir(async o => {
    process.chdir(o.path)
    await fs.writeFile('main.tex', tex)
    const proc = childProcess.spawn('latexmk', [
      '-pdf',
      '-halt-on-error',
      '-norc',
      '-synctex=1',
      'main'
    ])
    proc.stdout.on('data', data => {
      console.log(data.toString())
    })
    proc.stderr.on('data', data => {
      console.error(data.toString())
    })

    ret = await new Promise(resolve => proc.on('exit', c => resolve(c)))
    if (ret) {
      console.log(chalk.red('Could not create document'))
      return
    }

    await fs.rename('main.pdf', OUTPUT_PATH)
    console.log(chalk.green(`Created ${OUTPUT_PATH}`))
  }, {
    prefix: 'confessionquiz',
    unsafeCleanup: true
  })

  return ret
}

cli.run('genconfessionquiz', main)
