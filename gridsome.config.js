// This is where project configuration and plugin options are located.
// Learn more: https://gridsome.org/docs/config
// Changes here requires a server restart.
// To restart press CTRL + C in terminal and run `gridsome develop`
const remarkAttr = require('remark-attr')

const siteUrl = 'https://svrbc.org'

const remarkAttrConfig = ['remark-attr', {
  // Since remark-bracketed-spans hijacks the linkReference syntax, we
  // can't use remark-attr on references.
  elements: [...remarkAttr.SUPPORTED_ELEMENTS]
    .filter(e => e !== 'reference'),
  extend: { '*': ['nospell', 'expandable'] },
  enableAtxHeaderInline: true
}]

function vueRemarkConfig (name, baseDir, opts = {}) {
  return {
    use: '@gridsome/vue-remark',
    options: Object.assign({
      typeName: name,
      baseDir,
      template: `./src/templates/${name}.vue`,
      plugins: [
        // TODO: our own remark-spans plugin works better with remark attr.
        'remark-bracketed-spans',
        // This makes sure that remark-sectionize does not sectionize the
        // script and style tags.
        // https://github.com/jake-low/remark-sectionize/issues/6
        require('./plugins/remark-vue-remark-touchup'),
        'remark-sectionize',
        remarkAttrConfig,
        ['remark-external-links', { rel: ['noopener'] }]
      ],
      remark: {
        useBuiltIns: false
      },
      ignore: ['**/*.text.md']
    }, opts)
  }
}

module.exports = {
  siteName: 'SVRBC',
  host: '0.0.0.0',
  siteDescription: 'Silicon Valley Reformed Baptist Church in Sunnyvale, ' +
    'California',
  siteUrl, // Required by @microflash/gridsome-plugin-feed
  // Cache busting is not necessary on netlify, and has huge performance
  // implications for deploy.
  // https://github.com/gridsome/gridsome/pull/840
  cacheBusting: false,
  configureWebpack: {
    module: {
      rules: [
        { test: /\.handlebars$/, loader: 'handlebars-loader' }
      ]
    }
  },
  permalinks: {
    slugify: {
      use: require('./src/lib/datautils').slugify
    }
  },
  templates: {
    Article: '/articles/:id/:title/',
    Bulletin: '/bulletin/:id/',
    ConfessionParagraph: '/confession/:id/',
    Preacher: [
      {
        path: '/preachers/:id/',
        component: './src/templates/Sermons.vue'
      },
      {
        name: 'sermonsbypreacher',
        path: '/data/sermons-by-preacher/:id/',
        component: './src/templates/data/SermonsByPreacher.vue'
      }
    ],
    Sermon: '/sermons/:id/:title?'
  },
  transformers: {
    remark: {
      // externalLinks needs to be loaded in after remark-bracketed-spans
      plugins: [
        'remark-bracketed-spans',
        remarkAttrConfig,
        'remark-sectionize',
        'remark-footnotes',
        ['remark-external-links', { rel: ['noopener'] }]
      ]
    }
  },
  plugins: [
    { use: '@gridsome/plugin-critical' },
    require('./src/announcements'),
    require('./src/articles'),
    require('./src/bulletins'),
    require('./src/confession'),
    require('./src/confession/chapters'),
    require('./src/confession/quiz'),
    require('./src/preachers'),
    require('./src/series'),
    require('./src/sermons'),
    require('./src/text'),
    require('./src/urgent'),
    vueRemarkConfig('MarkdownPage', './src/pages'),
    vueRemarkConfig('TestPage', './src/test', {
      template: './src/test/TestPage.vue',
      pathPrefix: '/_test'
    }),
    {
      use: '@microflash/gridsome-plugin-feed',
      options: {
        contentTypes: ['Article'],
        // See https://github.com/jpmonette/feed#example for all options.
        feedOptions: {
          title: 'SVRBC Articles',
          description: 'Articles from Silicon Valley Reformed Baptist Church'
        },
        rss: {
          enabled: true,
          output: '/articles.rss'
        },
        atom: {
          enabled: true,
          output: '/articles.atom'
        },
        maxItems: 500, // This is optional.
        htmlFields: ['content'],
        enforceTrailingSlashes: true,
        // See https://github.com/jpmonette/feed#example for all options.
        nodeToFeedItem: (node) => ({
          title: node.title,
          id: `${siteUrl}${node.path}`,
          link: `${siteUrl}${node.path}`,
          date: node.date,
          content: node.content,
          author: {
            name: node.author
          }
        })
      }
    },
    {
      use: '@microflash/gridsome-plugin-feed',
      options: {
        contentTypes: [
          'Article',
          'Sermon'
        ],
        // See https://github.com/jpmonette/feed#example for all options.
        feedOptions: {
          title: 'SVRBC super feed',
          description: 'All updates from Silicon Valley Reformed Baptist Church'
        },
        rss: {
          enabled: true,
          output: '/feed.rss'
        },
        atom: {
          enabled: true,
          output: '/feed.atom'
        },
        maxItems: 500, // This is optional.
        htmlFields: ['content'],
        enforceTrailingSlashes: true,
        // See https://github.com/jpmonette/feed#example for all options.
        nodeToFeedItem: (node) => ({
          title: node.title,
          id: `${siteUrl}${node.path}`,
          link: `${siteUrl}${node.path}`,
          date: node.date,
          content: node.content,
          author: {
            name: node.author
          }
        })
      }
    }
  ]
}
