# svrbc.org

## Get the code

    git clone https://gitlab.com/svrbc/svrbc.org
    cd svrbc.org

## Install node

Install `nvm`:

    curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.34.0/install.sh | bash

Within the `svrbc.org` directory, run:

    nvm install
    nvm use

## Run the server

Within the `svrbc.org` directory, run:

    npm ci
    npm start

In a browser, visit `http://localhost:8888`.
