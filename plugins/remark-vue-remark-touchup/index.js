const visit = require('unist-util-visit')

function insertExport (parent, index) {
  // It's not clear if `export` means anything in particular, but
  // remark-sectionize will terminate a section if it encounters this.
  parent.children.splice(index, 0, { type: 'export' })
}

module.exports = () => {
  return ast => {
    // Go in reverse so we can insert nodes without infinite looping.
    const reverse = true
    visit(ast, 'vue-remark:block', (node, index, parent) => {
      insertExport(parent, index)
    }, reverse)
    visit(ast, 'html', (node, index, parent) => {
      if (node.value.startsWith('<section')) {
        insertExport(parent, index)
      }
    }, reverse)
  }
}
