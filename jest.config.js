// We can't put this in the globalSetup because jest-puppeteer sets a
// globalSetup.
process.env.TEST = 'true'

module.exports = {
  preset: 'jest-puppeteer',
  transform: {
    '\\.handlebars$': 'jest-handlebars',
    '\\.yml$': 'yaml-jest',
    '^.+\\.(js)$': 'babel-jest'
  },
  transformIgnorePatterns: ['<rootDir>/node_modules/'],
  setupFilesAfterEnv: [
    'expect-puppeteer',
    'jest-expect-message',
    'jest-extended'
  ],
  watchPathIgnorePatterns: [
    '<rootDir>/node_modules/'
  ]
}
