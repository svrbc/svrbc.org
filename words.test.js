/* eslint-env jest */
import { promises as fs } from 'fs'
import path from 'path'

describe('words.txt tests', () => {
  let words

  // The first page load can be especially slow.
  beforeAll(async () => {
    const content = await fs.readFile(path.join(__dirname, 'words.txt'), 'utf8')
    words = content.split(/\s+/).filter(w => w !== '')
  })

  it('words are sorted', async () => {
    const sortedWords = words.slice(0)
    sortedWords.sort()
    expect(words).toEqual(sortedWords)
  })
})
