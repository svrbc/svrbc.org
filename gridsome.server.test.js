/* eslint-env jest */
import { promises as fs } from 'fs'
import path from 'path'
import { xml2js } from 'xml-js'

import { ITUNES_LIMIT } from './lib/podcast'

// BEGIN tests ////////////////////////////////////////////////////////////////

describe('/sermons.rss', () => {
  it('contains max sermons', async () => {
    const text = await fs.readFile(path.join(__dirname, 'dist', 'sermons.rss'))
    const podcast = xml2js(text)
    let element = podcast.elements[0]
    expect(element.name).toEqual('rss')
    element = element.elements[0]
    expect(element.name).toEqual('channel')
    const episodes = element.elements.filter(e => e.name === 'item')
    expect(episodes.length).toEqual(ITUNES_LIMIT)
  }, 30000)
})
